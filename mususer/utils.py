"""
    Various utils for subscription manipulation
"""

from django.utils.timezone import get_default_timezone
from django.template import loader
from django.core.mail import send_mail
from django.contrib.sites.models import Site
from datetime import datetime
from django.conf import settings
import stripe
from mususer.models import MusUser

import logging
log = logging.getLogger('payment')
log.debug('Logging started')


def send_email(user, subject_template, body_template, extra_context={}):
    """
        Email sending routine
    """

    try:
        current_site = Site.objects.get_current()
        site_name = current_site.name
        domain = current_site.domain
        use_https = settings.USE_HTTPS
        ctx = {
            'email': user.email,
            'domain': domain,
            'site_name': site_name,
            'user': user,
            'protocol': use_https and 'https' or 'http',
        }
        ctx.update(extra_context)
        subject = loader.render_to_string(subject_template, ctx)
        subject = ''.join(subject.splitlines())
        email = loader.render_to_string(body_template, ctx)
        send_mail(subject, email, settings.DEFAULT_FROM_EMAIL, [user.email])
    except Exception as exc:
        log.debug('Exception: %s', exc)
        raise


def get_event_data(kwargs):
    """
        Get stripe event data from json
    """
    try:
        full_json = kwargs.pop("full_json", None)
        log.debug('Full JSON: %s', full_json)
        if 'data' in full_json:
            customer_id = full_json.get('data').get('object').get('customer')
        elif 'object' in full_json:
            customer_id = full_json.get('object').get('customer')
        log.debug('Customer ID: %s', customer_id)
        if customer_id is None:
            log.debug('Bad customer_id')
            return full_json, None, None, None
        try:
            user = MusUser.objects.get(stripe_customer_id=customer_id)
        except MusUser.DoesNotExist:
            log.debug('Unknown customer')
            return full_json, None, None, None
        stripe_customer = user.stripe_customer
        log.debug('Subscription: %s', stripe_customer.subscription)
        try:
            timestamp = stripe_customer.subscription.get('current_period_end')
        except AttributeError:
            timestamp = None
        log.debug('Timestamp: %s', timestamp)
        if timestamp is None:
            log.debug('Bad timestamp')
            expire_date = None
        else:
            expire_date = (
                datetime.fromtimestamp(int(timestamp))
                .replace(tzinfo=get_default_timezone())
            )

    except Exception as exc:
        log.debug('Exception: %s', exc)
        raise
    return full_json, user, stripe_customer, expire_date
