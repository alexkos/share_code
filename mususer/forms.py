#-*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext, ugettext_lazy as _
from musopen.models import BlockedDomain, Page
from mususer.models import (
    MusUser, Day, PaymentMethod, TeachLevel, TeachType, Plan,
)
from crispy_forms.helper import FormHelper
from crispy_forms.layout import (
    Layout, Field, MultiField, Fieldset, ButtonHolder, Submit, Div, HTML,
)
from crispy_forms.bootstrap import (
    AppendedText, PrependedText, InlineCheckboxes,
)
from zebra.forms import StripePaymentForm
from zebra.conf import options
from mususer.widgets import (
    NoNameTextInput, NoNameSelect, MyTimeInput, MyImageInput,
)

from django.utils.dates import MONTHS
MONTHS = [
    (m[0], u'%02d - %s' % (m[0], unicode(m[1])))
    for m in sorted(MONTHS.iteritems())
]

from django_localflavor_us import forms as us
from django_localflavor_us.us_states import STATE_CHOICES
STATES = list(STATE_CHOICES)
STATES.insert(0, ('', '---------'))
us.USStateField.choices = STATES


class UserCreationForm(forms.Form):
    """
        Registration form
    """
    error_messages = {
        'duplicate_email': _("A user with that email already exists."),
        'bad_domain': _("This domain name is not allowed for "
                        "registering on this site."),
        'spambot_detected': _(
            "You're probably a spambot "
            "or have turned off javascript in browser"
        ),
    }
    username = forms.CharField(label=_('Name'))
    email = forms.EmailField(label=_("Email"))
    password = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput,
    )
    important = forms.CharField(
        label=_("It's a honeypot, don't fill this if you're not a spambot."),
        required=False,
    )
    confirmation = forms.CharField(
        label=_(
            "This field should be filled by script, "
            "but if you see it just type 'I am not a bot' here."
        ),
    )

    class Meta:
        model = MusUser
        fields = ("email")

    def clean_email(self):
        """
            Check email to be unique.
        """
        email = self.cleaned_data["email"]
        email = MusUser.objects.normalize_email(email)
        if (
            BlockedDomain.objects
            .filter(domain_name=email.split('@')[1])
            .exists()
        ):
            raise forms.ValidationError(self.error_messages['bad_domain'])
        if MusUser.objects.filter(email=email).exists():
            raise forms.ValidationError(self.error_messages['duplicate_email'])
        return email

    def clean_important(self):
        """
            Catch bots in honeypot: should be empty
        """
        value = self.cleaned_data.get("important")
        if value:
            raise forms.ValidationError(
                self.error_messages['spambot_detected'])
        else:
            return value

    def clean_confirmation(self):
        """
            Catch bots in honeypot: should be 'I am not a bot'
        """
        value = self.cleaned_data.get("confirmation")
        if value.lower().strip().replace(' ', '') != 'iamnotabot':
            raise forms.ValidationError(
                self.error_messages['spambot_detected'])
        else:
            return value

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal form-register'
        self.helper.form_id = 'registration-form'
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', _('Sign Up')))
        super(UserCreationForm, self).__init__(*args, **kwargs)


class StateField(forms.ChoiceField, us.USStateField):
    """
        This is a crutch. Default USStateField can't make
        blank choice for some reason.
    """
    def __init__(self, *args, **kwargs):
        self._choices = STATES
        kwargs['choices'] = STATES
        self.widget = us.USStateSelect
        super(StateField, self).__init__(*args, **kwargs)


class ProfileEditForm(forms.ModelForm):
    """
        Form to edit user profile data
    """

    error_messages = {
        'duplicate_email': _("A user with that email already exists."),
        'bad_domain': _("This domain name is not allowed for "
                        "registering on this site."),
        'duplicate_nickname':  _("A user with that nickname already exists."),
        'bad_nickname':  _("Sorry, you cannot use this nickname."),
    }

    nickname = forms.RegexField(
        label=_("Custom URL"), max_length=30,
        regex=r'^[a-zA-Z][\w_-]+$',
        help_text=_("30 characters or fewer. Letters, digits and "
                    "-/_ only. Should start with letter."),
        required=False,
    )
    email = forms.EmailField(label=_("Email"))
    bio = forms.CharField(
        widget=forms.Textarea(attrs={'cols': 50, 'rows': 5, }),
        required=False,
    )
    about_me = forms.CharField(
        widget=forms.Textarea(attrs={'cols': 50, 'rows': 5, }),
        required=False,
    )
    links = forms.CharField(
        widget=forms.Textarea(attrs={'cols': 50, 'rows': 5, }),
        required=False,
    )
    education = forms.CharField(
        widget=forms.Textarea(attrs={'cols': 50, 'rows': 5, }),
        required=False,
    )
    awards = forms.CharField(
        widget=forms.Textarea(attrs={'cols': 50, 'rows': 5, }),
        required=False,
    )
    resume = forms.CharField(
        widget=forms.Textarea(attrs={'cols': 50, 'rows': 5, }),
        required=False,
    )
    past_repertoire = forms.CharField(
        widget=forms.Textarea(attrs={'cols': 50, 'rows': 5, }),
        required=False,
    )
    available_days = forms.ModelMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
        queryset=Day.objects.all(),
        required=False,
    )
    available_from = forms.TimeField(
        widget=MyTimeInput(attrs={'class': 'input-small'}),
        required=False,
    )
    available_to = forms.TimeField(
        widget=MyTimeInput(attrs={'class': 'input-small'}),
        required=False,
    )
    payment_methods = forms.ModelMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
        queryset=PaymentMethod.objects.all(),
        required=False,
    )
    teach_levels = forms.ModelMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
        queryset=TeachLevel.objects.all(),
        required=False,
    )
    teach_types = forms.ModelMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
        queryset=TeachType.objects.all(),
        required=False,
    )
    picture = forms.ImageField(widget=MyImageInput, required=False)
    state = StateField(required=False)

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_id = 'profile-edit-form'
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', _('Save')))
        self.helper.layout = Layout(
            Div(
                HTML("""
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#profile_tab" data-toggle="tab">
                                User Profile
                            </a>
                        </li>
                        <li>
                            <a href="#background_tab" data-toggle="tab">
                                Music Background
                            </a>
                        </li>
                        <li>
                            <a href="#teaching_tab" data-toggle="tab">
                                Teaching Experience
                            </a>
                        </li>
                        <li>
                            <a href="#settings_tab" data-toggle="tab">
                                Settings
                            </a>
                        </li>
                    </ul>
                """),
                Div(
                    Div(
                        Fieldset(
                            "Basic Info",
                            'username', 'email', 'nickname',
                            'homepage', 'picture',
                        ),
                        Fieldset(
                            "",
                            'bio', 'about_me', 'links',
                        ),
                        css_class="tab-pane active",
                        id="profile_tab",
                    ),
                    Div(
                        Fieldset(
                            "Music Background",
                            'instrument', 'genre', 'education', 'awards',
                        ),
                        css_class="tab-pane",
                        id="background_tab",
                    ),
                    Div(
                        Fieldset(
                            "Teaching Info",
                            "resume",
                            InlineCheckboxes('available_days'),
                            'available_from', 'available_to',
                            PrependedText('cost_per_lesson', '$'),
                            InlineCheckboxes('payment_methods'),
                            InlineCheckboxes('teach_levels'),
                            InlineCheckboxes('teach_types'),
                        ),
                        Fieldset(
                            "Performance Info",
                            "distance", 'past_repertoire',
                            PrependedText('cost_per_recording', '$'),
                        ),
                        Fieldset(
                            "Location and Contact Info",
                            'city', 'state', 'zip_code',
                            'country', 'phone', 'contactme',
                        ),
                        css_class="tab-pane",
                        id="teaching_tab",
                    ),
                    Div(
                        Fieldset(
                            "Settings",
                            Div(
                                Div(
                                    HTML("""
                <a class="btn" href="{% url 'mususer-change-password' %}">
                    <i class="icon-lock"></i>Change password
                </a>
                                    """),
                                    css_class="controls",
                                ),
                                css_class="control-group",
                            ),
                            'newsletter',
                        ),
                        css_class="tab-pane",
                        id="settings_tab",
                    ),
                    css_class="tab-content",
                ),
                css_class="tabbable",
            )

        )
        super(ProfileEditForm, self).__init__(*args, **kwargs)

    def clean_email(self):
        """
            Check email to be unique
        """
        email = self.cleaned_data["email"]
        if self.instance and email != self.instance.email:
            if (
                BlockedDomain.objects
                .filter(domain_name=email.split('@')[1])
                .exists()
            ):
                raise forms.ValidationError(self.error_messages['bad_domain'])
            if MusUser.objects.filter(email=email).exists():
                raise forms.ValidationError(
                    self.error_messages['duplicate_email']
                )
        return email

    def clean_nickname(self):
        """
            Check nickname to be unique
        """
        nickname = self.cleaned_data['nickname']
        if nickname == '':
            return None
        if self.instance and nickname != self.instance.nickname:
            if MusUser.objects.filter(nickname=nickname).exists():
                raise forms.ValidationError(
                    self.error_messages['duplicate_nickname']
                )
            if Page.objects.filter(slug=nickname).exists():
                raise forms.ValidationError(
                    self.error_messages['bad_nickname']
                )
        return nickname

    class Meta:
        model = MusUser
        fields = (
            'email', 'nickname', 'username', 'homepage', 'about_me', 'picture',
            'education', 'awards', 'genre',
            'links',
            'country', 'state', 'zip_code', 'city', 'phone', 'contactme',
            'resume', 'bio',
            'newsletter', 'instrument',
            'available_from', 'available_to', 'available_days',
            'cost_per_lesson', 'distance',
            'past_repertoire', 'cost_per_recording',
            'payment_methods', 'teach_levels', 'teach_types',
        )


def plan_title(plan):
    if plan.interval_count != 1:
        result = (
            "{p.name} – ${p.amount} per {p.interval_count} {p.interval}s"
        ).format(p=plan)
    else:
        result = (
            "{p.name} – ${p.amount} per {p.interval}"
        ).format(p=plan)
    return result


class PrettyStripeForm(forms.Form):
    """
        Stripe payment form (not inherited because of problems)
    """

    plan = forms.ChoiceField(
        choices=[
            (plan.plan_id, plan_title(plan))
            for plan in Plan.objects.exclude(plan_id='musopen_pro')
        ],
        widget=forms.RadioSelect,
    )
    card_number = forms.CharField(
        required=False, max_length=20,
        widget=NoNameTextInput(),
    )
    card_name = forms.CharField(
        required=False, max_length=64,
        widget=NoNameTextInput(), label="Cardholder name",
    )
    card_cvv = forms.CharField(
        required=False, max_length=4,
        widget=NoNameTextInput(), label="Card CVC",
        help_text="Card Verification Code; see rear of card.",
    )
    card_expiry_month = forms.ChoiceField(
        required=False, widget=NoNameSelect(),
        choices=MONTHS,
    )
    card_expiry_year = forms.ChoiceField(
        required=False, widget=NoNameSelect(),
        choices=options.ZEBRA_CARD_YEARS_CHOICES,
    )
    last_4_digits = forms.CharField(
        required=True, min_length=4, max_length=4,
        widget=forms.HiddenInput(),
    )
    stripe_token = forms.CharField(required=True, widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        if 'cc_collapsed' in kwargs:
            cc_collapsed = kwargs.pop('cc_collapsed')
        else:
            cc_collapsed = False
        if cc_collapsed:
            fs_class = ' collapse'
            fs_style = 'height: 0px;'
            btn_class = ''
        else:
            fs_class = ' in collapse'
            fs_style = 'height: auto;'
            btn_class = ' hidden'
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_id = 'payment-form'
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('go', _('Submit Payment')))
        self.helper.layout = Layout(
            Fieldset(
                'Select subscription plan',
                'plan',
            ),
            Fieldset(
                'Credit Card Info',
                HTML("""
                    <div class="control-group{}">
                      <div class="controls">
                        <a
                          class="btn"
                          id="id_collapse_cc_btn"
                          data-toggle="collapse"
                          data-target=".form-collapse"
                        >
                          Add new credit card
                        </a>
                        or
                        <a
                          class="btn"
                          href="{{% url 'mususer-update-card' %}}"
                        >
                          Update existing credit card details
                        </a>
                      </div>
                    </div>
                """.format(btn_class)),
                HTML("""
                    <noscript>
                    <h3>Note:&nbsp; this form requires Javascript to use.</h3>
                    </noscript>
                """),
                Div(
                    Field(
                        'card_number',
                        css_class="input-large",
                    ),
                    Field('card_name', css_class="input-xlarge"),
                    Field('card_cvv', css_class="input-mini"),
                    Div(
                        HTML('''
                            <label
                                for="id_card_expiry_month"
                                class="control-label"
                            >
                                    Exp.
                            </label>
                        '''),
                        Div(
                            Field(
                                'card_expiry_month',
                                css_class="input-small",
                                template="custom_crispy/clear-field.html",
                            ),
                            HTML("/"),
                            Field(
                                'card_expiry_year',
                                css_class="input-small",
                                template="custom_crispy/clear-field.html",
                            ),
                            css_class="controls",
                        ), # Div with expiry month/year
                        css_class="control-group",
                        id="div_expire_date",
                    ),  # Div with form fields
                    'last_4_digits',
                    'stripe_token',
                    css_class=("form-collapse" + fs_class),
                    style=("" + fs_style),
                ),  # Collapsing Div
            ),  # Fieldset
        )  # Layout
        super(PrettyStripeForm, self).__init__(*args, **kwargs)
        if cc_collapsed:
            self.fields['last_4_digits'].required = False
            self.fields['stripe_token'].required = False


class UpdateCardForm(forms.Form):
    id = forms.CharField(widget=forms.HiddenInput())
    customer = forms.CharField(widget=forms.HiddenInput())
    name = forms.CharField(label="Cardholder name")
    exp_month = forms.ChoiceField(
        required=False,
        choices=MONTHS,
        label='New expiration month',
    )
    exp_year = forms.ChoiceField(
        required=False,
        choices=options.ZEBRA_CARD_YEARS_CHOICES,
        label='New expiration year',
    )
    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_id = 'update-form'
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', _('Save')))
        super(UpdateCardForm, self).__init__(*args, **kwargs)


class UnsubscribeForm(forms.Form):
    email = forms.EmailField(label=_("Email"))

    def clean_email(self):
        email = self.cleaned_data['email']
        if MusUser.objects.filter(email=email).exists():
            return email
        else:
            raise forms.ValidationError('Email not found')
