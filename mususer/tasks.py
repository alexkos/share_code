"""
    Celery stuff (not used for now)
"""

from celery.task import periodic_task
from celery.schedules import crontab
from django.conf import settings
from musopen.utils import update_blocked_domains

if settings.UPDATE_BLOCKED_DOMAINS:
    @periodic_task(run_every=crontab(hour=23))
    def update_blocked_domains_task():
        """
            Update list of bad email domains
        """
        update_blocked_domains()
