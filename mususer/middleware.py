"""
    Simple middleware to collect users' ip
"""


class IPLogMiddleware(object):
    """
        Simple middleware to collect users' ip
    """

    def process_request(self, request):
        if request.user.is_authenticated():
            ip = request.META['REMOTE_ADDR']
            if not request.user.ip_set.filter(ip=ip).exists():
                request.user.ip_set.create(ip=ip)
