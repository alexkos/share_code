#-*- coding: utf-8 -*-
"""
    User profile and subscription views
"""
import datetime
from calendar import monthrange
from json import dumps
from django.template import loader
from django.views.decorators.cache import cache_page, never_cache
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.models import Group
from django.contrib.sites.models import get_current_site
from django.http.response import Http404, HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404
from django.contrib.auth.views import password_reset_confirm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth import login, logout as auth_logout
from django.utils.timezone import now, get_default_timezone
from django.utils.http import int_to_base36
from django.conf import settings
from django.contrib.auth.views import logout
from django.core.cache import cache
from django.core.urlresolvers import reverse

import stripe
from mususer.models import MusUser, IP, Subscription, Plan
from mususer.forms import (
    UserCreationForm, ProfileEditForm,
    PrettyStripeForm, UnsubscribeForm, UpdateCardForm,
)
from mususer.utils import send_email
from mususer.decorators import admin_required
from musopen.utils import get_client_ip
from musopen.models import MultipleTimesLogin
from musmusic.models import (
    Piece, SheetMusic, PiecePartBookmark, SheetPartBookmark,
)
from athumb.exceptions import UploadedImageIsUnreadableError
from mususer.templatetags.user_tags import subscription_data
import logging
payment_log = logging.getLogger('payment')


@never_cache
def signup(request, profile='LITE'):
    """
        Register user. Can be rendered to two templates
        (for modal and full window view)
    """
    ip = request.META['REMOTE_ADDR']
    if IP.objects.filter(user__is_blocked=True, ip=ip).exists():
        msg = "<h3>Sorry, someone with your ip address was banned on our site.</h3>"
        if request.is_ajax():
            return HttpResponse(msg)
        else:
            return HttpResponse(msg, status=403)
    if request.is_ajax():
        template_name = 'mususer/registration-ajax.html'
    else:
        template_name = 'mususer/registration.html'
    if not request.user.is_authenticated():
        if request.method == 'POST':
            form = UserCreationForm(request.POST)
            if form.is_valid():
                user = MusUser.objects.create_user(
                    email=form.cleaned_data['email'],
                    password=form.cleaned_data['password'])
                user.username = form.cleaned_data['username']
                user.save()
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                login(request, user)
                if 'level' in request.GET and request.GET.get('level') == 'benefactor':
                    benefactor = True
                else:
                    benefactor = False
                if profile == 'PRO':
                    qs = '?pro-registration=1'
                    if benefactor:
                        qs += '&plan=musopen_benefactor'
                    if request.is_ajax():
                        return HttpResponse(
                            dumps(
                                {
                                    'redirect': reverse(
                                        'mususer-upgrade-profile'
                                    ) + qs
                                }
                            )
                        )
                    else:
                        return redirect(
                            reverse(
                                'mususer-upgrade-profile'
                            ) + qs
                        )
                else:
                    user.group = Group.objects.get(id=1)
                    user.save()
                    if request.is_ajax():
                        return HttpResponse(
                            dumps(
                                {
                                    'redirect': reverse(
                                        'mususer-edit-profile'
                                    ) + '?after-registration=1'
                                }
                            )
                        )
                    else:
                        return redirect(
                            reverse(
                                'mususer-edit-profile'
                            ) + '?after-registration=1'
                        )
            else:
                return render(request, template_name, {
                    'form': form,
                    'profile': profile,
                    'hide_ads': True,
                })
        else:
            form = UserCreationForm()
            return render(request, template_name, {
                'form': form,
                'profile': profile,
                'hide_ads': True,
            })
    else:
        if request.is_ajax():
            return HttpResponse(dumps({'redirect': '/'}))
        else:
            return HttpResponseRedirect('/')


def repair_cheater_password(
    request, uidb36=None, token=None,
    template_name='mususer/cheater_password_reset_confirm.html',
    token_generator=default_token_generator,
    set_password_form=SetPasswordForm,
    post_reset_redirect=None,
    current_app=None, extra_context=None,
):
    '''This function resets user's password and unblock theirs account.
    '''
    if uidb36 and token:
        response = password_reset_confirm(
            request, uidb36, token, template_name,
            token_generator, set_password_form,
            post_reset_redirect, current_app, extra_context)
        return response
    return redirect("index")


def timeout(request):
    """
        Simple page for timeouted users
    """
    return render(request, 'mususer/login-timeout.html', {}, status=403)


def block(request):
    """
        Simple page for blocked users
    """
    return render(request, 'mususer/login-block.html', {}, status=403)


@staff_member_required
def ban_user(request, user_id):
    """
        Ajax view for banning users
    """
    user = get_object_or_404(MusUser, id=user_id)
    user.is_active = False
    user.is_blocked = True
    user.save()
    return HttpResponse(dumps({"success": True}))


def prevent_login_twice(func):
    """
        Decorator to detect multiple login errors
    """
    def wrapper(request, *args, **kwargs):
        interval = datetime.timedelta(
            seconds=getattr(settings, 'INTERVAL_BTW_LOGINS_SEC', 60)
        )

        if request.method == 'POST':
            auth_form = AuthenticationForm(data=request.POST)
            if not auth_form.is_valid():
                if (
                    not auth_form.cleaned_data.get('username')
                    or not auth_form.cleaned_data.get('password')
                ):
                    return func(request, *args, **kwargs)
                try:
                    user = (
                        MusUser.objects
                        .get(email=auth_form.cleaned_data['username'])
                    )
                except MusUser.DoesNotExist:
                    return func(request, *args, **kwargs)

                last_time = MultipleTimesLogin.objects.filter(
                    user=user,
                    reason="Login error",
                    date__gte=now()-interval,
                )

                if last_time.exists():
                    return redirect("mususer-login-timeout")

                MultipleTimesLogin.objects.create(
                    ip=get_client_ip(request),
                    user=user,
                    reason="Login error"
                )
                seconds = getattr(
                    settings,
                    "INTERVAL_LOGIN_TO_BLOCK_SEC",
                    10*60,
                )
                mult_times = MultipleTimesLogin.objects.filter(
                    ip=get_client_ip(request),
                    user=user,
                    reason="Login error",
                    date__gte=now()-datetime.timedelta(seconds=seconds)
                )
                need_block = getattr(
                    settings,
                    'MULTIPLE_TIMES_LOGIN_COUNT_BEFORE_BLOCK',
                    5,
                )
                if mult_times.count() >= need_block:
                    user.is_active = False
                    user.save()
                    send_email(
                        user,
                        'mususer/blocked_subject.txt',
                        'mususer/blocked_email.txt',
                        {
                            'uid': int_to_base36(user.pk),
                            'token': default_token_generator.make_token(user),
                        },
                    )
                    return redirect("mususer-login-block")
        return func(request, *args, **kwargs)
    return wrapper


def ajax_login_template(func):
    """
        Decorator for changing login page template (for modal and full window)
    """
    def wrapper(request, *args, **kwargs):
        if request.is_ajax():
            template_name = 'mususer/login-ajax.html'
        else:
            template_name = 'mususer/login.html'
        kwargs['template_name'] = template_name
        return func(request, *args, **kwargs)
    return wrapper


def catch_redirect(func):
    """
        Catch redirect responses from login view and replace them
        with json response for correct modal behavior
    """
    def wrapper(request, *args, **kwargs):
        resp = func(request, *args, **kwargs)
        if request.is_ajax():
            if resp.status_code == 302:
                return HttpResponse(dumps({'redirect': resp['Location']}))
        return resp
    return wrapper


def user_profile(request, user_id=None, user_nickname=None):
    """
    Shows user profile
    """
    if not user_nickname:
        if not user_id:
            if request.user.is_authenticated():
                return redirect('mususer-show-profile', request.user.id)
            else:
                return redirect('login')
        user = get_object_or_404(MusUser, id=user_id, is_active=True)
    else:
        user = get_object_or_404(
            MusUser,
            nickname=user_nickname,
            is_active=True,
        )
    site = get_current_site(request)
    if request.user == user:
        piece_bookmarks = PiecePartBookmark.objects.filter(user=user)
        sheet_bookmarks = SheetPartBookmark.objects.filter(user=user)
        uploaded_pieces = Piece.objects.filter(user=user)
        uploaded_sheets = SheetMusic.objects.filter(user=user)
        hide_ads = True
    else:
        piece_bookmarks = []
        sheet_bookmarks = []
        uploaded_pieces = []
        uploaded_sheets = []
        hide_ads = False
    if request.user.is_authenticated() and request.user.is_paid:
        hide_ads = True


    return render(request, 'mususer/show-profile.html', {
        'user': user,
        'site': site,
        'piece_bookmarks': piece_bookmarks,
        'sheet_bookmarks': sheet_bookmarks,
        'uploaded_pieces': uploaded_pieces,
        'uploaded_sheets': uploaded_sheets,
        'hide_ads': hide_ads,
    })


@login_required
def edit_user_profile(request):
    """
    Edits user profile
    """

    user = request.user
    if request.method == "GET":
        form = ProfileEditForm(instance=user)
    elif request.method == "POST":
        form = ProfileEditForm(instance=user, data=request.POST)
        if form.is_valid():
            if request.FILES and request.FILES.get('picture'):
                picture = request.FILES.get('picture')
                suffix = picture.name.rsplit('.', 1)[-1]
                picture_name = 'profile_picture_{}_{}.{}'.format(
                    user.id, user.email.replace('@', '_at_'), suffix,
                )
                try:
                    form.instance.picture.save(picture_name, picture)
                except UploadedImageIsUnreadableError:
                    if form._errors.get('picture'):
                        form._errors['picture'].append('Invalid image')
                    else:
                        form._errors['picture'] = ['Invalid image']
                    return render(request, 'mususer/edit-profile.html', {
                        'user': user,
                        'form': form,
                        'hide_ads': True,
                    })
            form.save()
            return redirect("mususer-show-profile", user.id,)

    return render(request, 'mususer/edit-profile.html', {
        'user': user,
        'form': form,
        'hide_ads': True,
    })


@login_required
def upgrade_profile(request):
    """
    Setting app subscription with stripe
    """
    error = ""
    user = request.user
    plan = None
    subdata = cache.get('subdata_{}'.format(user.id))

    # TODO: we should use code like this in other places
    # where still there are hardcoded default plan
    plan_types = list(set([x[0] for x in Plan.objects.values_list('simple_name')]))
    plans = dict([
        (plan_type, Plan.objects.filter(simple_name=plan_type).filter(active=True))
        for plan_type in plan_types
    ])
    default_plan = Plan.objects.filter(active=True, default=True)[0]

    if not subdata:
        subdata = subscription_data(user)
        if not subdata:
            subdata = False
    if request.method == "GET":
        if 'plan' in request.GET:
            try:
                plan = Plan.objects.get(plan_id=request.GET.get('plan'))
            except Plan.DoesNotExists:
                plan = default_plan
        else:
            if user.group and user.group.plan_set.exists():
                plan = user.group.plan_set.get()
            else:
                plan = default_plan
        if not plan or not plan.active:
            plan = default_plan
        data = {'plan': plan.plan_id}
        zebra_form = PrettyStripeForm(
            data,
            cc_collapsed=(subdata and subdata.get('status')=='active'),
        )
    elif request.method == "POST":
        zebra_form = PrettyStripeForm(
            request.POST,
            cc_collapsed=(subdata and subdata.get('status')=='active'),
        )
        if zebra_form.is_valid():
            stripe_token = zebra_form.cleaned_data.get('stripe_token')
            if user.stripe_customer_id:
                try:
                    stripe_customer = user.stripe_customer
                except stripe.InvalidRequestError:
                    # Wrong id, probably
                    stripe_customer = (
                        stripe.Customer
                        .create(email=user.email)
                    )
                    user.stripe_customer_id = stripe_customer.id
                    user.save()
            else:
                # FIXME: not good duplication, it's not DRY
                # even though they're just 3 lines and close to each other
                stripe_customer = stripe.Customer.create(email=user.email)
                user.stripe_customer_id = stripe_customer.id
                user.save()
            try:
                if stripe_token:
                    stripe_customer.update_subscription(
                        plan=zebra_form.cleaned_data['plan'],
                        card=zebra_form.cleaned_data['stripe_token'],
                    )
                else:
                    stripe_customer.update_subscription(
                        plan=zebra_form.cleaned_data['plan'],
                    )
            except stripe.CardError as exc:
                body = exc.json_body
                error = body['error']['message']
            except stripe.InvalidRequestError as exc:
                error = "Invalid request."
                payment_log.error(
                    "Invalid request error. Message: '{}', param: {}".format(
                        exc.json_body.get('error').get('message'),
                        exc.json_body.get('error').get('param'),
                    )
                )

            except stripe.AuthenticationError as exc:
                error = "Authentication with payment server failed"
            except stripe.APIConnectionError as exc:
                error = "Network communication with payment server failed"
            except stripe.StripeError as exc:
                error = "Internal payment error"
            except Exception as exc:
                error = "Internal error"
            else:
                cache.delete('subdata_{}'.format(user.id))
                # TODO: maybe we should pass some message in session
                # like "upgraded successfully", though there is a 'PRO'
                # sign which tell just that in most cases
                return redirect("mususer-show-profile", user.id,)
    return render(request, 'mususer/upgrade-profile.html', {
        'plans': plans,
        'default_plan': plan,
        'user': user,
        'error': error,
        'zebra_form': zebra_form,
        'hide_ads': True,
    })


@login_required
def downgrade_profile(request, sure=''):
    """
    Cancels subscription with stripe
    """
    error = ''
    user = request.user
    if not user.stripe_customer_id:
        return redirect('mususer-my-profile')
    if user.profile != 'PRO':
        return redirect('mususer-my-profile')
    try:
        stripe_customer = stripe.Customer.retrieve(user.stripe_customer_id)
        if (
            not stripe_customer.subscription
            or stripe_customer.subscription.get('status') == 'canceled'
        ):
            return redirect('mususer-my-profile')
    except stripe.InvalidRequestError as exc:
        error = "Invalid request"
    except stripe.AuthenticationError as exc:
        error = "Authentication with payment server failed"
    except stripe.APIConnectionError as exc:
        error = "Network communication with payment server failed"
    except stripe.StripeError as exc:
        error = "Internal payment error"
    except Exception as exc:
        error = "Internal error"
    else:
        if sure == 'sure/':
            try:
                stripe_customer.cancel_subscription()
            except stripe.InvalidRequestError as exc:
                error = "Invalid request"
            except stripe.AuthenticationError as exc:
                error = "Authentication with payment server failed"
            except stripe.APIConnectionError as exc:
                error = "Network communication with payment server failed"
            except stripe.StripeError as exc:
                error = "Internal payment error"
            except Exception as exc:
                error = "Internal error"
            else:
                return redirect('mususer-my-profile')

    return render(request, 'mususer/downgrade-profile.html', {
        'error': error,
        'user': user,
        'hide_ads': True,
    })


@login_required
def update_card(request):
    """
        Change some of CC details w/o having to fill card number and all
    """
    if not request.user.stripe_customer_id:
        return redirect('mususer-upgrade-profile')
    stripe_customer = request.user.stripe_customer
    if not stripe_customer.get('default_card'):
        return redirect('mususer-upgrade-profile')
    card_data = stripe_customer.cards.retrieve(
        stripe_customer['default_card']
    )
    if request.method == 'POST':
        form = UpdateCardForm(initial=card_data, data=request.POST)
        if form.is_valid():
            if form.cleaned_data['id'] != card_data['id']:
                form = UpdateCardForm(initial=card_data)
                return render(request, 'mususer/update-card.html',{
                    'form': form,
                    'card_data': card_data,
                    'message': (
                        'Not saved. '
                        'It seems you have changed your default card '
                        'after last save. Please check data and save again.'
                    ),
                })
            else:
                card_data.name = form.cleaned_data['name']
                card_data.exp_month = form.cleaned_data['exp_month']
                card_data.exp_year = form.cleaned_data['exp_year']
                success = False
                try:
                    card_data.save()
                except stripe.CardError as exc:
                    body = exc.json_body
                    error = body['error']['message']
                except stripe.InvalidRequestError as exc:
                    error = "Invalid request."
                    payment_log.error(
                        "Invalid request error. Message: '{}', param: {}".format(
                            exc.json_body.get('error').get('message'),
                            exc.json_body.get('error').get('param'),
                        )
                    )

                except stripe.AuthenticationError as exc:
                    error = "Authentication with payment server failed"
                except stripe.APIConnectionError as exc:
                    error = "Network communication with payment server failed"
                except stripe.StripeError as exc:
                    error = "Internal payment error"
                except Exception as exc:
                    error = "Internal error"
                else:
                    error = 'Saved'
                    success = True
                return render(request, 'mususer/update-card.html',{
                    'form': form,
                    'card_data': card_data,
                    'success': success,
                    'message': error,
                    'hide_ads': True,
                })

        else:
            return render(request, 'mususer/update-card.html',{
                'form': form,
                'card_data': card_data,
                'message': (
                    'Not saved. Please check data for errors and save again.'
                ),
                'hide_ads': True,
            })
    else:
        form = UpdateCardForm(initial=card_data)
        return render(request, 'mususer/update-card.html',{
            'form': form,
            'card_data': card_data,
            'hide_ads': True,
        })

@admin_required
def downgrade_statistics_years(request):

    length_sum = 0
    length_count = 0
    for s in Subscription.objects.filter(start__isnull=False):
        start = s.start
        end = s.end
        if not end:
            end = s.user.premium_expire
        if not end:
            end = (
                    datetime.datetime
                    .fromtimestamp(
                        int(s.user.stripe_subscription.get(
                            'current_period_end'
                        ))
                    )
                    .replace(tzinfo=get_default_timezone())
                )
        length = (end - start).days
        print start, end, length
        length_sum += length
        length_count += 1
    avg_length = float(length_sum)/float(length_count)

    return render(request, 'mususer/downgrade_statistics_years.html', {
        'years': range(now().year - 5, now().year + 1),
        'avg_length': avg_length,
    })


@admin_required
def downgrade_statistics(request, year, month=None):
    year = int(year)
    if month:
        month = int(month)
    dt = lambda y, m, d: datetime.datetime(y, m, d).replace(
        tzinfo=get_default_timezone()
    )
    year_count = MusUser.objects.filter(
        premium_expire__gte=dt(year, 1, 1),
        premium_expire__lte=dt(year, 12, 31),
    ).count()
    month_counts = [MusUser.objects.filter(
        premium_expire__gte=dt(year, m, 1),
        premium_expire__lte=dt(year, m, monthrange(year, m)[1]),
    ).count() for m in range(1, 13)]
    month_avg = float(sum(month_counts)) / 12.0
    month_extended_counts = []
    for i, num in enumerate(month_counts):
        curr = month_counts[i]
        prev = month_counts[i - 1]
        d = {}
        d['count'] = month_counts[i]
        d['month_num'] = i + 1
        d['month_datetime'] = datetime.datetime(year, i + 1, 1)
        if i != 0:
            d['prev_delta'] = curr - prev
            if prev != 0:
                d['prev_delta_percent'] = float(d['prev_delta']) * 100 / float(prev)
            else:
                d['prev_delta_percent'] = None
        else:
            d['prev_delta'] = 0
            d['prev_delta_percent'] = 0.0
        d['avg_delta'] = curr - month_avg
        if month_avg != 0:
            d['avg_delta_percent'] = float(d['avg_delta']) * 100 / float(month_avg)
        else:
            d['avg_delta_percent'] = None
        month_extended_counts.append(d)
    if month:
        month_count = month_counts[month + 1]
    else:
        month_count = None

    return render(request, 'mususer/downgrade_statistics.html', {
        'year': year,
        'current_or_future': (now().year - year) <= 0,
        'years': range(year - 3, year + 3),
        'month': month,
        'year_count': year_count,
        'month_counts': month_counts,
        'month_count': month_count,
        'month_avg': month_avg,
        'month_extended_counts': month_extended_counts,
    })


def unsubscribe(request):
    if request.user.is_authenticated():
        user = request.user
        user.newsletter = False
        user.save()
        unsubscribed = True
        form = None
    else:
        if request.method == 'POST':
            form = UnsubscribeForm(data=request.POST)
            if form.is_valid():
                user = MusUser.objects.get(email=form.cleaned_data['email'])
                user.newsletter = False
                user.save()
                unsubscribed = True
            else:
                user = None
                unsubscribed = False
        else:
            user = None
            unsubscribed = False
            form = UnsubscribeForm()
    return render(request, 'mususer/unsubscribe.html', {
        'user': user,
        'unsubscribed': unsubscribed,
        'form': form,
        'hide_ads': True,
    })


@admin_required
def revenue_prediction(request):
    dt = lambda y, m, d: datetime.datetime(y, m, d).replace(
        tzinfo=get_default_timezone()
    )
    year = now().year
    month = now().month
    prediction_list = []
    total = 0

    # Churn average for 3 months
    if month > 3:
        month_counts = [MusUser.objects.filter(
            premium_expire__gte=dt(year, m, 1),
            premium_expire__lte=dt(year, m, monthrange(year, m)[1]),
        ).count() for m in range(month - 3, month)]
    else:
        month_counts = [MusUser.objects.filter(
            premium_expire__gte=dt(year, m, 1),
            premium_expire__lte=dt(year, m, monthrange(year, m)[1]),
        ).count() for m in range(9 + month, 13)] + [MusUser.objects.filter(
            premium_expire__gte=dt(year - 1, m, 1),
            premium_expire__lte=dt(year - 1, m, monthrange(year - 1, m)[1]),
        ).count() for m in range(1, month)]
    churn_rate = sum(month_counts) / float(len(month_counts))
    for m in range(month + 1, 13):
        users = MusUser.objects.filter(
        premium_expire__gte=dt(year, m, 1),
        premium_expire__lte=dt(year, m, monthrange(year, m)[1]),
    ).filter(subscription__active=True)
        summ = 0
        for count, user in enumerate(users):
            if user.group and user.group.plan_set.exists():
                summ += user.group.plan_set.get().amount
                total += user.group.plan_set.get().amount
        loss = - int(float(summ) * churn_rate / count)
        prediction_list.append(
            [m, count, summ, datetime.datetime(year, m, 1), loss]
        )
    for m in range(1, month + 1):
        users = MusUser.objects.filter(
        premium_expire__gte=dt(year + 1, m, 1),
        premium_expire__lte=dt(year + 1, m, monthrange(year + 1, m)[1]),
    ).filter(subscription__active=True)
        summ = 0
        for count, user in enumerate(users):
            if user.group and user.group.plan_set.exists():
                summ += user.group.plan_set.get().amount
                total += user.group.plan_set.get().amount
        if count:
            loss = - int(float(summ) * churn_rate / count)
        else:
            loss = 0
        prediction_list.append(
            [m, count, summ, datetime.datetime(year, m, 1), loss]
        )
    for l in prediction_list:
        l.append(int(l[2] * 100 / total))

    return render(
        request,
        'mususer/revenue-prediction.html',
        {
            'prediction_list': prediction_list,
            'total': total,
        },
    )
