from django.core.management.base import BaseCommand, CommandError
from datetime import datetime
from django.utils.timezone import get_default_timezone, now
from mususer.models import MusUser, Subscription
from mususer.utils import send_email
from django.conf import settings
import stripe

stripe.api_key = settings.STRIPE_SECRET


class Command(BaseCommand):
    def handle(self, *args, **options):
        for user in MusUser.objects.filter(stripe_customer_id__isnull=False):
            #print user.id
            try:
                card_data = user.stripe_customer.active_card
            except AttributeError:
                continue
            if not card_data:
                continue
            if Subscription.objects.filter(user=user).exists():
                sub = Subscription.objects.filter(user=user).get()
            else:
                sub = Subscription()
                sub.user = user
            sub.cc_month = card_data.get('exp_month')
            sub.cc_year = card_data.get('exp_year')
            sub.save()
            year = datetime.now().year
            month = datetime.now().month
            if sub.cc_year == year and sub.cc_month == month:
                send_email(
                    user,
                    'mususer/payment/cc-expiry-subject.txt',
                    'mususer/payment/cc-expiry-email.txt',
                    extra_context={
                        'stripe_customer': user.stripe_customer,
                        'card_data': card_data,
                    },
                )
