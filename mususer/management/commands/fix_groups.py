"""
    Command to detect users' subscriptions and assign groups to them
"""

from django.core.management.base import BaseCommand, CommandError
import datetime
from django.utils.timezone import get_default_timezone, now
from mususer.models import MusUser
from django.contrib.auth.models import Group
from django.conf import settings


class Command(BaseCommand):
    def handle(self, *args, **options):
        benefactors = Group.objects.get(name="Benefactors")
        pay = Group.objects.get(name="Pay")
        free = Group.objects.get(name="Free")
        for user in MusUser.objects.filter(group__isnull=True):
            print user
            if user.profile == 'PRO':
                user.group = pay
            else:
                user.group = free
            user.save()
