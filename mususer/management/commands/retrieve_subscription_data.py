from django.core.management.base import BaseCommand, CommandError
from datetime import datetime
from django.utils.timezone import get_default_timezone, now
from mususer.models import MusUser, Subscription
from django.conf import settings
import stripe

stripe.api_key = settings.STRIPE_SECRET


class Command(BaseCommand):
    def handle(self, *args, **options):
        for user in MusUser.objects.filter(stripe_customer_id__isnull=False):
            print user.id
            #print user.stripe_subscription
            try:
                subscription = user.stripe_subscription
            except AttributeError:
                continue
            if not subscription:
                continue
            start = subscription.get('start')
            if start:
                start = (
                    datetime
                    .fromtimestamp(int(start))
                    .replace(tzinfo=get_default_timezone())
                )
            else:
                start = None
            end = subscription.get('ended_at')
            if end:
                end = (
                    datetime
                    .fromtimestamp(int(end))
                    .replace(tzinfo=get_default_timezone())
                )
            else:
                end = None
            active = subscription.get('status') == 'active'
            if Subscription.objects.filter(user=user).exists():
                sub = Subscription.objects.filter(user=user).get()
            else:
                sub = Subscription()
                sub.user = user
            sub.start = start
            sub.end = end
            sub.active = active
            sub.save()
