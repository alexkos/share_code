from django.conf import settings
from datetime import timedelta
from decimal import Decimal
from django.utils.timezone import now
from django.core.management.base import BaseCommand, CommandError
from mususer.models import MusUser
from mususer.utils import send_email, get_event_data
import stripe
stripe.api_key = settings.STRIPE_SECRET


class Command(BaseCommand):
    def handle(self, *args, **options):
        for user in MusUser.objects.filter(
            premium_expire__gte=now() + timedelta(2),
            premium_expire__lt=now() + timedelta(3),
        ):
            if user.stripe_customer_id:
                stripe_customer = user.stripe.customer
            else:
                stripe_customer = None
            if stripe_customer and stripe_customer.get('subscription'):
                # Get info from stripe since it could be more actual
                plan_id = (
                    stripe_customer.get('subscription').get('plan').get('id')
                )
                amount = Decimal(
                    stripe_customer.get('subscription')
                    .get('plan').get('amount')
                ) / 100
            else:
                # Fall back to local info which is probably good too
                if user.group.plan_set.exists():
                    plan = user.group.plan_set.get()
                else:
                    plan = Plan.objects.filter(default=True, active=True)[0]
                amount = plan.amount
                plan_id = plan.plan_id
            send_email(
                user,
                'mususer/payment/charge-before-subject.txt',
                'mususer/payment/charge-before-email.txt',
                extra_context={
                    'stripe_customer': stripe_customer,
                    'amount': amount,
                },
            )
