"""
    Subscription signals (webhooks) handlers
"""

from django.utils.timezone import now, get_default_timezone
from datetime import datetime
from decimal import Decimal
from django.core.cache import cache
from django.contrib.auth.models import Group
from django.conf import settings
import stripe
from zebra.signals import (
    zebra_webhook_customer_subscription_created,
    zebra_webhook_customer_subscription_updated,
    zebra_webhook_charge_succeeded,
    zebra_webhook_charge_failed,
    zebra_webhook_invoice_payment_succeeded,
)
from mususer.models import MusUser, Subscription
from mususer.utils import send_email, get_event_data
import logging
log = logging.getLogger('payment')
log.debug('Logging started')


def update_expires_date(sender, **kwargs):
    """
        Update premium_expire if charge was successful
    """
    try:
        full_json = kwargs.pop("full_json", None)
        log.debug('Full JSON: %s', full_json)
        customer_id = full_json.get('data').get('object').get('customer')
        log.debug('Customer ID: %s', customer_id)
        if customer_id is None:
            log.debug('Bad customer_id')
            return
        try:
            user = MusUser.objects.get(stripe_customer_id=customer_id)
        except MusUser.DoesNotExist:
            log.debug('Unknown customer')
            return
        stripe_customer = stripe.Customer.retrieve(user.stripe_customer_id)
        log.debug('Subscription: %s', stripe_customer.subscription)
        try:
            timestamp = stripe_customer.subscription.get('current_period_end')
        except AttributeError:
            timestamp = None
        log.debug('Timestamp: %s', timestamp)
        if timestamp is None:
            log.debug('Bad timestamp')
            return
        cache.delete('subdata_{}'.format(user.id))
        try:
            plan_id = stripe_customer.subscription.get('plan').id
            if (
                not user.group
                or not user.group.plan_set.exists()
                or user.group.plan_set.get().plan_id != plan_id
            ):
                if Group.objects.filter(plan__plan_id=plan_id).exists():
                    user.group = (
                        Group.objects
                        .filter(plan__plan_id=plan_id)
                        .get()
                    )
        except AttributeError:
            plan_id = None
        user.premium_expire = (
            datetime
            .fromtimestamp(int(timestamp))
            .replace(tzinfo=get_default_timezone())
        )
        user.save()
        start = stripe_customer.subscription.get('start')
        if start:
            start = (
                datetime
                .fromtimestamp(int(start))
                .replace(tzinfo=get_default_timezone())
            )
        else:
            start = None
        end = stripe_customer.subscription.get('ended_at')
        if end:
            end = (
                datetime
                .fromtimestamp(int(end))
                .replace(tzinfo=get_default_timezone())
            )
        else:
            end = None
        active = stripe_customer.subscription.get('status') == 'active'
        if Subscription.objects.filter(user=user).exists():
            sub = Subscription.objects.filter(user=user).get()
            if not sub.start or sub.start > start:
                sub.start = start
            sub.end = end
            sub.active = active
            sub.save()
        else:
            sub = Subscription.objects.create(
                user=user,
                start=start,
                end=end,
                active=active,
            )
        log.debug('Saved')
    except Exception as exc:
        log.debug('Exception: %s', exc)
        raise

zebra_webhook_customer_subscription_created.connect(update_expires_date)
zebra_webhook_customer_subscription_updated.connect(update_expires_date)
zebra_webhook_charge_succeeded.connect(update_expires_date)


def successful_charge(sender, **kwargs):
    """
        Send email with receipt if charge was successful
    """
    try:
        log.debug('Sending email: successful_charge')
        full_json, user, stripe_customer, expire_date = get_event_data(kwargs)
        if not user:
            return
        if 'data' in full_json:
            full_json = full_json.get('data')
        invoice_id = full_json.get('object').get('id')
        timestamp = full_json.get('object').get('date')
        date = (
            datetime.fromtimestamp(int(timestamp))
            .replace(tzinfo=get_default_timezone())
        )
        total = full_json.get('object').get('total')
        subtotal = full_json.get('object').get('subtotal')
        items = full_json.get('object').get('lines').get('data')
        items_list = []
        for item in items:
            amount = item.get('amount')
            log.debug('item amount: %s', amount)
            amount = Decimal(amount) / 100
            log.debug('item amount: %s', amount)
            if item.get('type') == 'invoiceitem':
                description = item.get('description')
            elif item.get('type') == 'subscription':
                plan_amount = item.get('plan').get('amount')
                plan_amount = Decimal(plan_amount) / 100
                plan_interval = item.get('plan').get('interval')
                plan_interval_count = item.get('plan').get('interval_count')
                plan_name = item.get('plan').get('name')
                description = '{} subscription (${} / {} {})'.format(
                    plan_name,
                    plan_amount,
                    plan_interval_count,
                    plan_interval,
                )
            log.debug('item descr: %s', description)
            items_list.append({'description': description, 'amount': amount})
        log.debug('items: %s', items_list)
        log.debug('subtotal, total: %s', subtotal, total)
        if subtotal:
            subtotal = Decimal(subtotal) / 100
        if total:
            total = Decimal(total) / 100
        log.debug('subtotal, total: %s', subtotal, total)

        send_email(
            user,
            'mususer/payment/charge-success-subject.txt',
            'mususer/payment/charge-success-email.txt',
            extra_context={
                'full_json': full_json,
                'stripe_customer': stripe_customer,
                'date': date,
                'items': items_list,
                'subtotal': subtotal,
                'total': total,
                'invoice_id': invoice_id,
            },
        )

    except Exception as exc:
        log.error('Exception: %s', exc)
        raise


def failed_charge(sender, **kwargs):
    """
        Send email with error if charge was not successful
    """
    try:
        log.debug('Sending email: failed_charge')
        full_json, user, stripe_customer, expire_date = get_event_data(kwargs)
        if not user:
            return
        send_email(
            user,
            'mususer/payment/charge-fail-subject.txt',
            'mususer/payment/charge-fail-email.txt',
            extra_context={
                'full_json': full_json,
                'user': user,
                'stripe_customer': stripe_customer,
                'expire_date': expire_date,
            },
        )
    except Exception as exc:
        log.debug('Exception: %s', exc)
        raise

zebra_webhook_invoice_payment_succeeded.connect(successful_charge)
zebra_webhook_charge_failed.connect(failed_charge)
'''Test stuff:
from json import loads
full_json=loads("""{
    "object":
    {
        "date": 1365095385,
        "id": "in_1aRlYWYJ5kyqRk",
        "period_start": 1365094475,
        "period_end": 1365095385,
        "lines":
        {
            "object": "list",
            "count": 2,
            "url": "/v1/invoices/in_1aRlYWYJ5kyqRk/lines",
            "data":
            [
                {
                    "id": "ii_1aRlihqly3Yg31",
                    "object": "line_item",
                    "type": "invoiceitem",
                    "livemode": false,
                    "amount": -5500,
                    "currency": "usd",
                    "proration": true,
                    "period":
                    {
                        "start": 1365095385,
                        "end": 1365095385
                    },
                    "quantity": null,
                    "plan": null,
                    "description": "Unused time on Member after 04 Apr 2013"
                },
                {
                    "id": "su_1aRlofwPaN0xKU",
                    "object": "line_item",
                    "type": "subscription",
                    "livemode": false,
                    "amount": 2000,
                    "currency": "usd",
                    "proration": false,
                    "period":
                    {
                        "start": 1365095385,
                        "end": 1367687385
                    },
                    "quantity": 1,
                    "plan":
                    {
                        "interval": "month",
                        "name": "Benefactor",
                        "amount": 2000,
                        "currency": "usd",
                        "id": "musopen_benefactor",
                        "object": "plan",
                        "livemode": false,
                        "interval_count": 1,
                        "trial_period_days": null
                    },
                    "description": null
                }
            ]
        },
        "subtotal": -3500,
        "total": -3500,
        "customer": "cus_1aRW4V2sSc4Y1P",
        "object": "invoice",
        "attempted": true,
        "closed": true,
        "paid": true,
        "livemode": false,
        "attempt_count": 0,
        "amount_due": 0,
        "currency": "usd",
        "starting_balance": 0,
        "ending_balance": "-3500",
        "next_payment_attempt": null,
        "charge": null,
        "discount": null
    }
}""")

zebra_webhook_invoice_payment_succeeded.send(
        sender=None,
        full_json=full_json,
)
'''
