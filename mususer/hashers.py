#-*- coding: utf-8 -*-
"""
    Legacy hashers
"""

import hashlib

from django.contrib.auth.hashers import (BasePasswordHasher, mask_hash)
from django.utils.datastructures import SortedDict
from django.utils.encoding import force_bytes
from django.utils.crypto import constant_time_compare
from django.utils.translation import ugettext_noop as _


class MD5MusopenPasswordHasher(BasePasswordHasher):
    """
    Special snowflake algorithm from the second musopen version.
    php code: $hashPass = md5($this->salt . md5($password));
    Database string should be normalize to: md5mus$SALT$PASSSWORDHASH
    """
    algorithm = "md5mus"

    def encode(self, password, salt):
        assert password
        assert salt and '$' not in salt
        hash_digest = hashlib.md5(force_bytes(
            salt + hashlib.md5(force_bytes(password)).hexdigest()
        )).hexdigest()
        return "%s$%s$%s" % (self.algorithm, salt, hash_digest)

    def verify(self, password, encoded):
        algorithm, salt, hash_digest = encoded.split('$', 2)
        assert algorithm == self.algorithm
        encoded_2 = self.encode(password, salt)
        return constant_time_compare(encoded, encoded_2)

    def safe_summary(self, encoded):
        algorithm, salt, hash_digest = encoded.split('$', 2)
        assert algorithm == self.algorithm
        return SortedDict([
            (_('algorithm'), algorithm),
            (_('salt'), mask_hash(salt, show=2)),
            (_('hash'), mask_hash(hash_digest)),
        ])


class UnsaltedSHA1PasswordHasher(BasePasswordHasher):
    """
    Special snowflake algorithm from the first musopen version.
    php code: $pass=substr(sha1(trim($_POST['password'])),0,8);
    Database string should be normalize to:
    unsalted_and_trimmed_sha1$$PASSSWORDHASH
    """
    algorithm = "unsalted_and_trimmed_sha1"  # unsalted and trimmed sha1

    def salt(self):
        return ''

    def encode(self, password, salt):
        assert password
        assert '$' not in salt
        hash_digest = hashlib.sha1(force_bytes(salt + password)).hexdigest()[:8]
        return "%s$%s$%s" % (self.algorithm, salt, hash_digest)

    def verify(self, password, encoded):
        algorithm, salt, hash_digest = encoded.split('$', 2)
        assert algorithm == self.algorithm
        encoded_2 = self.encode(password, salt)
        return constant_time_compare(encoded, encoded_2)

    def safe_summary(self, encoded):
        algorithm, salt, hash_digest = encoded.split('$', 2)
        assert algorithm == self.algorithm
        return SortedDict([
            (_('algorithm'), algorithm),
            (_('salt'), mask_hash(salt, show=2)),
            (_('hash'), mask_hash(hash_digest)),
        ])
