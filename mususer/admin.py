#-*- coding: utf-8 -*-

from django.contrib import admin
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from django.http import HttpResponse
from django.contrib.auth.admin import GroupAdmin
from django.contrib.auth.models import Group
from mususer.models import MusUser, Plan, IP


class ProfileFilter(admin.SimpleListFilter):
    """
        Filter user by profile type
    """
    title = _('Profile')
    parameter_name = 'profile'

    def lookups(self, request, model_admin):
        return (('PRO', 'PRO'), ('LITE', 'LITE'))

    def queryset(self, request, queryset):
        if self.value() == 'PRO':
            return queryset.filter(
                Q(gift_expire__gte=now()) | Q(premium_expire__gte=now())
            )
        elif self.value() == 'LITE':
            return (
                queryset
                .exclude(gift_expire__gte=now())
                .exclude(premium_expire__gte=now())
            )


class DomainFilter(admin.SimpleListFilter):
    """
        Filter users which domains are blocked
    """
    title = 'Blocked domain'
    parameter_name = 'bad_domain'

    def lookups(self, request, model_admin):
        return (('yes', 'Yes'), ('no', 'No'))

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.extra(
                where=(
                    ('''substring_index(`mususer_mususer`.`email`, "@", -1)'''
                     '''IN (SELECT `domain_name` FROM `musopen_blockeddomain`)'''),
                ),
            )
        elif self.value() == 'no':
            return queryset.extra(
                where=(
                    ('''substring_index(`mususer_mususer`.`email`, "@", -1)'''
                     '''NOT IN (SELECT `domain_name` FROM `musopen_blockeddomain`)'''),
                ),
            )


class IPInline(admin.StackedInline):
    model = IP
    readonly_fields = ('ip',)
    extra = 0
    verbose_name_plural = 'IP addresses'
    can_add = False
    can_delete = False

    def has_add_permission(self, request):
        return False


class MusUserAdmin(admin.ModelAdmin):
    """
        Admin class for users (most fields are hidden)
    """
    def ban(modeladmin, request, queryset):
        for user in queryset:
            user.is_active = False
            user.is_blocked = True
            user.save()

    ban.short_description = "Block selected users"

    def export_emails(modeladmin, request, queryset):
        response = HttpResponse(content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename="users.csv"'
        for user in queryset:
            response.write('{0.username},{0.email}\n'.format(user))
        return response

    export_emails.short_description = "Export emails as CSV"

    def unsubscribe(modeladmin, request, queryset):
        for user in queryset:
            user.newsletter = False
            user.save()

    unsubscribe.short_description = "Unsubscribe selected users from email list"

    actions = ('ban', 'export_emails', 'unsubscribe')
    inlines = (IPInline,)
    list_display = (
        'username',
        'email',
        'last_login',
        'gift_expire',
    )
    list_display_links = ('username', 'email')
    list_filter = (
        ProfileFilter,
        'is_active',
        'is_blocked',
        'group',
        DomainFilter,
    )
    ordering = ('-id',)
    # search through all fields of model, excluding fields listed below

    search_exclude_fields = [
        f.name for f in MusUser._meta.fields
        if f.get_internal_type() in (
            'DateField', 'DateTimeField', 'FileField',
        )
    ]
    search_fields = list(set([
        f.name
        for f in MusUser._meta.fields
        if not f.rel
    ]) - set(search_exclude_fields))
    fieldsets = (
        (None, {
            'fields': (
                'email', 'username', 'nickname',
                'is_active', 'is_admin', 'is_manager', 'is_blocked',
            ),
        }),
        (None, {
            'fields': (
                'group', 'premium_expire', 'gift_expire', 'stripe_customer_id',
            ),
        }),
        (None, {
            'fields': ('contactme', 'newsletter'),
        }),
    )


class PlanInline(admin.StackedInline):
    """
        Inline for subscription plan
    """
    model = Plan


class MusGroupAdmin(GroupAdmin):
    """
        Admin class for user group
    """
    inlines = (PlanInline,)


admin.site.unregister(Group)
admin.site.register(Group, MusGroupAdmin)
admin.site.register(MusUser, MusUserAdmin)
