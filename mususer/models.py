#-*- coding: utf-8 -*-
"""
    Custom user model and related stuff
"""

import datetime
from django.utils.timezone import now
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, Group
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify

import stripe

from zebra.mixins import StripeCustomerMixin, StripeSubscriptionMixin
from athumb.fields import ImageWithThumbsField
from musopen.storage_backend import S3PublicStorage

PUBLIC_MEDIA_BUCKET = S3PublicStorage(bucket=settings.AWS_STORAGE_BUCKET_NAME)

import string
import random

CHARS = string.ascii_letters + string.digits

stripe.api_key = settings.STRIPE_SECRET


class MusUserManager(BaseUserManager):
    """
    Custom user manager for musopen project
    """
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email address and password.
        """
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(email=MusUserManager.normalize_email(email),)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None):
        """
        Creates and saves a Superuser with the given email address and
        password.
        """
        user = self.create_user(email, password=password)
        user.is_admin = True
        user.save(using=self._db)
        return user


class Day(models.Model):
    """
        Days for 'available_days' profile field
    """
    name = models.CharField(max_length=32)

    def __unicode__(self):
        return self.name


class PaymentMethod(models.Model):
    """
        Payment methods for 'payment_methods' profile field
    """
    name = models.CharField(max_length=32)

    def __unicode__(self):
        return self.name


class TeachLevel(models.Model):
    """
        Teach levels for 'teach_levels' profile field
    """
    name = models.CharField(max_length=32)

    def __unicode__(self):
        return self.name


class TeachType(models.Model):
    """
        Teach types for 'teach_types' profile field
    """
    name = models.CharField(max_length=32)

    def __unicode__(self):
        return self.name


class Genre(models.Model):
    """
        Genres for 'genre' profile field
    """
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = 'its_u_genres'


class Instrument(models.Model):
    """
        Instruments for 'instrument' profile field (different from musmusic one)
    """
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = 'its_u_instruments'


class MusUser(AbstractBaseUser, StripeCustomerMixin, StripeSubscriptionMixin):
    '''This class represent user profile'''
    PROFILE_TYPE = (
        ('LITE', 'Lite account '),
        ('PRO', 'Pro account'),
    )

    stripe = stripe

    email = models.EmailField(verbose_name=_(u'Email'), max_length=255,
                              unique=True)
    group = models.ForeignKey('auth.Group', null=True, default=1)
    is_active = models.BooleanField(default=True)
    is_blocked = models.BooleanField(default=False)
    nickname = models.CharField(max_length=255, null=True, blank=True,
                                unique=True)
    username = models.CharField(verbose_name=_(u'Full name'), max_length=255,
                                blank=True, null=True)
    about_me = models.TextField(blank=True, null=True)
    education = models.TextField(blank=True, verbose_name=_(u'Education'))
    awards = models.TextField(blank=True, verbose_name=_(u'Awards/Media'))
    picture = ImageWithThumbsField(
        upload_to='images/users',
        thumbs=(
            ('64_82_cropped', {'size': (64, 82), 'crop': True}),
            ('64_82_cropped@2x', {'size': (128, 164), 'crop': True}),
            ('32_32_cropped', {'size': (32, 32), 'crop': True}),
            ('32_32_cropped@2x', {'size': (64, 64), 'crop': True}),
            ('70_70_cropped', {'size': (70, 70), 'crop': True}),
            ('70_70_cropped@2x', {'size': (140, 140), 'crop': True}),
            ('200_200_cropped', {'size': (200, 200), 'crop': True}),
            ('200_200_cropped@2x', {'size': (400, 400), 'crop': True}),
        ),
        blank=True, null=True,
        storage=PUBLIC_MEDIA_BUCKET,
    )
    links = models.TextField(blank=True, verbose_name=_(u'Links'), null=True)
    homepage = models.URLField(blank=True, null=True)
    country = models.CharField(max_length=50, blank=True, null=True)
    state = models.CharField(max_length=50, blank=True, null=True)
    zip_code = models.CharField(max_length=30, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=50, blank=True, null=True)
    contactme = models.BooleanField(default=False,
                                    verbose_name=_(u'Publish contact info'))
    resume = models.TextField(blank=True, null=True)
    bio = models.TextField(blank=True, null=True)
    premium_expire = models.DateTimeField(blank=True, null=True)
    gift_expire = models.DateTimeField(blank=True, null=True)
    newsletter = models.BooleanField(default=True,
                                     verbose_name=_(u'Receive newsletter'))
    instrument = models.ForeignKey(Instrument, blank=True, null=True)
    genre = models.ForeignKey(Genre, blank=True, null=True)
    approved = models.BooleanField(default=True)
    register_date = models.DateField(auto_now_add=True, blank=True, null=True)
    rates = models.CharField(max_length=255, blank=True, null=True)

    is_admin = models.BooleanField(default=False)
    is_manager = models.BooleanField(default=False)

    # Past teacher_info fields
    available_from = models.TimeField(default=datetime.time(9, 0))
    available_to = models.TimeField(default=datetime.time(17, 0))
    available_days = models.ManyToManyField(Day, null=True)
    cost_per_lesson = models.DecimalField(max_digits=5, decimal_places=2,
                                          default='0.00')
    distance = models.CharField(max_length=255, blank=True, null=True)
    past_repertoire = models.TextField(blank=True, null=True)
    cost_per_recording = models.DecimalField(max_digits=5, decimal_places=2,
                                             default='0.00')

    payment_methods = models.ManyToManyField(PaymentMethod, null=True)
    teach_levels = models.ManyToManyField(TeachLevel, null=True)
    teach_types = models.ManyToManyField(TeachType, null=True)

    # For stripe integration
    stripe_customer_id = models.CharField(max_length=255, blank=True, null=True)

    # For unsubscribing
    unsubscribe_token = models.CharField(
            max_length=255,
            blank=True, null=True, unique=True,
    )

    objects = MusUserManager()

    USERNAME_FIELD = 'email'

    def get_absolute_url(self):
        if self.nickname:
            return reverse('custom-profile-link', args=(self.nickname,))
        else:
            url = reverse('mususer-show-profile', args=(self.id,))
            if self.username:
                url += slugify(self.username) + '/'
            return url

    @property
    def profile(self):
        if self.is_admin:
            return 'PRO'
        # Check premium_expire and gift_expire fields
        elif self.premium_expire and self.premium_expire > now():
            return 'PRO'
        elif self.gift_expire and self.gift_expire > now():
            return 'PRO'
        else:
            return 'LITE'

    @property
    def is_benefactor(self):
        if self.group and (
            self.group.name == 'Benefactors'
            or self.group.name == 'Yearly Benefactors'
        ):
            return True
        else:
            return False

    @property
    def is_paid(self):
        # Should be true for users with active subscription
        if self.premium_expire and self.premium_expire > now():
            return True
        else:
            return False

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __unicode__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        # Does the user have a specific permission?
        # Simplest possible answer: Yes, always
        # (Except users-related things which are for admins only)
        if (
                perm.startswith('mususer.')
                or perm.startswith('auth.')
        ):
            if self.is_admin:
                return True
            else:
                return False
        else:
            return True

    def has_module_perms(self, app_label):
        # Does the user have permissions to view the app `app_label`?
        # Simplest possible answer: Yes, always
        print app_label
        return True

    @property
    def is_staff(self):
        # Is the user a member of staff?
        # Simplest possible answer: All admins are staff
        # and all managers are stuff
        return self.is_admin or self.is_manager

    class Meta:
        verbose_name_plural = _(u'Users')
        verbose_name = _(u'User')

    def save(self, *args, **kwargs):
        # Fixing blank values ('' in not unique)
        if self.nickname == '':
            self.nickname = None
        # Setting random unsubscribe token
        if not self.unsubscribe_token:
            token = ''.join(random.choice(CHARS) for x in range(32))
            while MusUser.objects.filter(unsubscribe_token=token).exists():
                token = ''.join(random.choice(CHARS) for x in range(32))
            self.unsubscribe_token = token
        super(MusUser, self).save(*args, **kwargs)


class Subscription(models.Model):
    user = models.ForeignKey(MusUser, unique=True)
    start = models.DateTimeField(null=True, blank=True)
    end = models.DateTimeField(null=True, blank=True)
    active = models.BooleanField(default=True)
    cc_year = models.IntegerField(null=True)
    cc_month = models.IntegerField(null=True)


class Downloads(models.Model):
    """
        Counts user's downloads
    """
    user = models.ForeignKey(MusUser, unique=True)
    count = models.IntegerField(default=0)
    date = models.DateField(auto_now=True)


class Plan(models.Model):
    """
        Stripe subscription plans
    """
    group = models.ForeignKey('auth.Group', unique=True)
    active = models.BooleanField(default=True)
    default = models.BooleanField(default=False)
    name = models.CharField(max_length=32, unique=True)
    simple_name = models.CharField(max_length=32, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    css_class = models.CharField(max_length=32, blank=True, null=True)
    plan_id = models.CharField(max_length=32, unique=True)
    amount = models.DecimalField(
        max_digits=5, decimal_places=2,
        default='0.00',
    )
    interval = models.CharField(
        max_length=32, default="year",
        choices=(("month", "Month"), ("year", "Year")),
    )
    interval_count = models.PositiveIntegerField(
        verbose_name="Billing period (months or years)",
        default=12,
    )
    trial_period_days = models.IntegerField(default=0)

    def __unicode__(self):
        return self.name

    def verbose_amount(self):
        if self.interval_count != 1:
            result = (
                "${p.amount} per {p.interval_count} {p.interval}s"
            ).format(p=self)
        else:
            result = (
                "${p.amount} per {p.interval}"
            ).format(p=self)
        return result


class IP(models.Model):
    ip = models.CharField(max_length=32)
    user = models.ForeignKey(MusUser)

    def __unicode__(self):
        return self.ip
