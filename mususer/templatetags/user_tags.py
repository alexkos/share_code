import stripe
from datetime import datetime
from django.utils.timezone import get_default_timezone
from django.core.cache import cache
from django.contrib.auth.models import Group
from django import template
from mususer.models import Subscription
register = template.Library()


@register.assignment_tag
def subscription_data(user):
    cached_result = cache.get('subdata_{}'.format(user.id))
    if cached_result:
        return cached_result
    if user.stripe_customer_id:
        try:
            customer = stripe.Customer.retrieve(user.stripe_customer_id)
        except:
            customer = None
    else:
        customer = None
    if customer and customer.get('subscription'):
        date = (
            datetime
            .fromtimestamp(int(customer.subscription.get('current_period_end')))
            .replace(tzinfo=get_default_timezone())
        )
        status = customer.subscription.get('status')
        plan_id = customer.subscription.get('plan').id
        if (
            not user.group
            or not user.group.plan_set.exists()
            or user.group.plan_set.get().plan_id != plan_id
        ):
            if Group.objects.filter(plan__plan_id=plan_id):
                user.group = Group.objects.filter(plan__plan_id=plan_id).get()
                user.save()
        if user.premium_expire < date:
            user.premium_expire = date
            user.save()
        start = customer.subscription.get('start')
        if start:
            start = (
                datetime
                .fromtimestamp(int(start))
                .replace(tzinfo=get_default_timezone())
            )
        else:
            start = None
        end = customer.subscription.get('ended_at')
        if end:
            end = (
                datetime
                .fromtimestamp(int(end))
                .replace(tzinfo=get_default_timezone())
            )
        else:
            end = None
        active = customer.subscription.get('status') == 'active'
        if Subscription.objects.filter(user=user).exists():
            sub = Subscription.objects.filter(user=user).get()
        else:
            sub = Subscription()
            sub.user = user
        if not sub.start or sub.start > start:
            sub.start = start
        sub.end = end
        sub.active = active
        sub.save()

    else:
        if user.gift_expire and user.premium_expire:
            date = max(user.gift_expire, user.premium_expire)
        else:
            date = user.gift_expire or user.premium_expire
        status = None
    result = {
        'date': date,
        'status': status,
    }
    cache.set('subdata_{}'.format(user.id), result, 300)
    return result


@register.filter
def is_unsubscribed(user):
    if hasattr(user, 'newsletter'):
        return not user.newsletter
    else:
        return None
