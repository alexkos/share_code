from django import forms
from django.utils.translation import ugettext, ugettext_lazy
from django.utils.html import conditional_escape, format_html, format_html_join
from django.utils.encoding import force_text, python_2_unicode_compatible
from django.utils.safestring import mark_safe


class NoNameWidget(object):
    """
        Abstract widget with 'name' attribute stripping
    """

    def _strip_name_attr(self, widget_string, name):
        return widget_string.replace("name=\"%s\"" % (name,), "")


class NoNameTextInput(forms.TextInput, NoNameWidget):
    """
        Text input w/o name attribute
    """

    def render(self, name, *args, **kwargs):
        return mark_safe(
            self._strip_name_attr(
                super(NoNameTextInput, self).render(name, *args, **kwargs),
                name,
            )
        )


class NoNameSelect(forms.Select, NoNameWidget):
    """
        Select w/o name attribute
    """

    def render(self, name, *args, **kwargs):
        return mark_safe(
            self._strip_name_attr(
                super(NoNameSelect, self).render(name, *args, **kwargs),
                name,
            )
        )


class MyTimeInput(forms.TimeInput):
    """
        Simple widget rendered to HTML5 <input type="time">
    """
    input_type = 'time'


class MyImageInput(forms.ClearableFileInput):
    """
        Pretty image input (with customized button and preview area)
    """

    template_with_initial = (
        '<div class="span2">%(initial)s</div>'
        '<div class="span2">'
        '   <div class="row">%(input)s</div>'
        '   <div class="divider">&nbsp;</div>'
        '   <div class="row"> %(clear_template)s</div>'
        '</div>'
    )
    template_with_clear = (
        '<label for="%(clear_checkbox_id)s" class="checkbox">'
        '   %(clear)s %(clear_checkbox_label)s'
        '</label>'
    )

    def render(self, name, value, attrs=None):
        substitutions = {
            'initial_text': self.initial_text,
            'input_text': self.input_text,
            'clear_template': '',
            'clear_checkbox_label': self.clear_checkbox_label,
        }
        template = '%(input)s'
        style = (
            "position: absolute; top: 0; right: 0; margin: 0; "
            "opacity: 0; filter: alpha(opacity=0); "
            "transform: translate(-300px, 0) scale(4); "
            "font-size: 600px; direction: ltr; cursor: pointer;"
        )
        if attrs.get('style'):
            attrs['style'].append(' ' + style)
        else:
            attrs['style'] = style
        substitutions['input'] = format_html(
            '<span style="'
            'position: relative; overflow: hidden; '
            'float: left; margin-right: 4px;'
            '" class="btn btn-success">'
            '<i class="icon-plus icon-white"></i> {} {}</span>',
            ugettext('Upload'),
            super(forms.FileInput, self).render(name, value, attrs)
        )

        if value and hasattr(value, "url"):
            template = self.template_with_initial
            substitutions['initial'] = format_html(
                (
                    '<a href="{0}">'
                    '<img class="img-polaroid" src="{1}" alt="{2}" />'
                    '</a>'
                ),
                value.url,
                value.generate_url("64_82_cropped"),
                force_text(value),
            )
            if not self.is_required:
                checkbox_name = self.clear_checkbox_name(name)
                checkbox_id = self.clear_checkbox_id(checkbox_name)
                substitutions['clear_checkbox_name'] = (
                    conditional_escape(checkbox_name)
                )
                substitutions['clear_checkbox_id'] = (
                    conditional_escape(checkbox_id)
                )
                substitutions['clear'] = (
                    forms.CheckboxInput()
                    .render(checkbox_name, False, attrs={'id': checkbox_id})
                )
                substitutions['clear_template'] = (
                    self.template_with_clear % substitutions
                )

        return mark_safe(template % substitutions)
