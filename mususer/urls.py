#-*- coding: utf-8 -*-
"""
    Mususer urls: accounts, profiles, subscriptions
"""

from django.conf.urls import patterns, include, url
from django.contrib.auth.views import login
from musopen.settings import LOGOUT_REDIRECT_URL
from views import prevent_login_twice, ajax_login_template, catch_redirect


urlpatterns = patterns(
    'django.contrib.auth.views',
    url(
        r'^login/$',
        catch_redirect(prevent_login_twice(ajax_login_template(login))),
        name='login',
    ),
    url(r'^logout/$', 'logout',
        {'next_page': LOGOUT_REDIRECT_URL}, name='logout'),

    url(
        r'^password/reset/$', 'password_reset',
        {
            'template_name': 'mususer/password_reset.html',
            'email_template_name': 'mususer/password_reset_email.html',
        }, name="mususer-reset-password",
    ),
    url(
        r'^password/reset/done/$', 'password_reset_done',
        {'template_name': 'mususer/password_reset_done.html'},
    ),

    url(
        r'^password/reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
        'password_reset_confirm',
        {'template_name': 'mususer/password_reset_confirm.html'},
    ),
    url(
        r'password/reset/complete/$', 'password_reset_complete',
        {
            'template_name': 'mususer/password_reset_complete.html'
        }, name="mususer-set-password",
    ),
    url(
        r'password/change/$', 'password_change',
        {
            'template_name': 'mususer/password_change.html',
            'post_change_redirect': '/account/profile/',
        },
        name="mususer-change-password",
    ),
) + patterns(
    'mususer.views',
    # Reserved for /signup page
    #    url(r'^signup/$', 'signup'),
    url(r'^signup/free/$', 'signup',
        {'profile': 'LITE'}, name="signup_free"),
    url(r'^signup/pro/$', 'signup',
        {'profile': 'PRO'}, name="signup_pro"),
    url(r'^login/timeout/$', 'timeout', name="mususer-login-timeout"),
    url(r'^login/block/$', 'block', name="mususer-login-block"),
    url(r'^profile/edit/$', 'edit_user_profile', name="mususer-edit-profile"),
    url(
        r'^profile/upgrade/$',
        'upgrade_profile',
        name="mususer-upgrade-profile",
    ),
    url(
        r'^profile/update_card/$',
        'update_card',
        name="mususer-update-card",
    ),
    url(
        r'^profile/downgrade/(?P<sure>sure/?)?$',
        'downgrade_profile',
        name="mususer-downgrade-profile",
    ),
    url(r'^profile/$', 'user_profile', name="mususer-my-profile"),
    url(
        r'^profile/unsubscribe/$', 'unsubscribe',
        name="mususer-unsubscribe"
    ),
    url(
        r'^ajax/ban-user/(?P<user_id>\d+)/$',
        'ban_user',
        name="mususer-ban-user",
    ),
    url(
        r'^downgrade-statistics/$',
        'downgrade_statistics_years',
        name='mususer-downgrade-statistics-years',
    ),
    url(
        r'^downgrade-statistics/(?P<year>\d{4})/(?:(?P<month>\d{1,2})/)?$',
        'downgrade_statistics',
        name='mususer-downgrade-statistics',
    ),
    url(
        r'^revenue-prediction/$',
        'revenue_prediction',
        name='mususer-revenue-prediction',
    ),
)

# Activate zebra's signals handling
import mususer.signals
