# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'TeacherInfo'
        db.delete_table(u'mususer_teacherinfo')

        # Deleting field 'MusUser.ref_link'
        db.delete_column(u'mususer_mususer', 'ref_link')

        # Deleting field 'MusUser.confirm_code'
        db.delete_column(u'mususer_mususer', 'confirm_code')

        # Deleting field 'MusUser.last_login_time'
        db.delete_column(u'mususer_mususer', 'last_login_time')

        # Deleting field 'MusUser.is_invited'
        db.delete_column(u'mususer_mususer', 'is_invited')

        # Deleting field 'MusUser.teacher_info'
        db.delete_column(u'mususer_mususer', 'teacher_info_id')

        # Deleting field 'MusUser.show_profile_completion'
        db.delete_column(u'mususer_mususer', 'show_profile_completion')


    def backwards(self, orm):
        # Adding model 'TeacherInfo'
        db.create_table(u'mususer_teacherinfo', (
            ('cost_per_lesson', self.gf('django.db.models.fields.DecimalField')(default='0.00', max_digits=5, decimal_places=2)),
            ('available_from', self.gf('django.db.models.fields.TimeField')(default=datetime.time(9, 0), null=True)),
            ('cost_per_recording', self.gf('django.db.models.fields.DecimalField')(default='0.00', max_digits=5, decimal_places=2)),
            ('distance', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('available_to', self.gf('django.db.models.fields.TimeField')(default=datetime.time(17, 0), null=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('resume', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('past_repertoire', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'mususer', ['TeacherInfo'])

        # Adding field 'MusUser.ref_link'
        db.add_column(u'mususer_mususer', 'ref_link',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.confirm_code'
        db.add_column(u'mususer_mususer', 'confirm_code',
                      self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'MusUser.last_login_time'
        raise RuntimeError("Cannot reverse this migration. 'MusUser.last_login_time' and its values cannot be restored.")
        # Adding field 'MusUser.is_invited'
        db.add_column(u'mususer_mususer', 'is_invited',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'MusUser.teacher_info'
        db.add_column(u'mususer_mususer', 'teacher_info',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mususer.TeacherInfo'], null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.show_profile_completion'
        db.add_column(u'mususer_mususer', 'show_profile_completion',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'mususer.day': {
            'Meta': {'object_name': 'Day'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'mususer.downloads': {
            'Meta': {'object_name': 'Downloads'},
            'count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mususer.MusUser']", 'unique': 'True'})
        },
        u'mususer.genre': {
            'Meta': {'object_name': 'Genre', 'db_table': "'its_u_genres'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'mususer.instrument': {
            'Meta': {'object_name': 'Instrument', 'db_table': "'its_u_instruments'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'mususer.mususer': {
            'Meta': {'object_name': 'MusUser'},
            'about_me': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'available_days': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['mususer.Day']", 'null': 'True', 'symmetrical': 'False'}),
            'available_from': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'available_to': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(17, 0)'}),
            'awards': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'bio': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'contactme': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cost_per_lesson': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'cost_per_recording': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'distance': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'education': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'genre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mususer.Genre']", 'null': 'True', 'blank': 'True'}),
            'gift_expire': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Group']", 'null': 'True'}),
            'homepage': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instrument': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mususer.Instrument']", 'null': 'True', 'blank': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'links': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'newsletter': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'nickname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'past_repertoire': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'payment_methods': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['mususer.PaymentMethod']", 'null': 'True', 'symmetrical': 'False'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'picture': ('athumb.fields.ImageWithThumbsField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'premium_expire': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'rates': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'register_date': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'resume': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'stripe_customer': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'teach_levels': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['mususer.TeachLevel']", 'null': 'True', 'symmetrical': 'False'}),
            'teach_types': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['mususer.TeachType']", 'null': 'True', 'symmetrical': 'False'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'})
        },
        u'mususer.paymentmethod': {
            'Meta': {'object_name': 'PaymentMethod'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'mususer.plan': {
            'Meta': {'object_name': 'Plan'},
            'amount': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Group']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interval': ('django.db.models.fields.CharField', [], {'default': "'year'", 'max_length': '32'}),
            'interval_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '12'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'plan_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'trial_period_days': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'mususer.teachlevel': {
            'Meta': {'object_name': 'TeachLevel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'mususer.teachtype': {
            'Meta': {'object_name': 'TeachType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        }
    }

    complete_apps = ['mususer']