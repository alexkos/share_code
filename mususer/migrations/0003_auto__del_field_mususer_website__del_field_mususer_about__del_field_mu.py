# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'MusUser.website'
        db.delete_column(u'mususer_mususer', 'website')

        # Deleting field 'MusUser.about'
        db.delete_column(u'mususer_mususer', 'about')

        # Deleting field 'MusUser.name'
        db.delete_column(u'mususer_mususer', 'name')

        # Deleting field 'MusUser.custom_url'
        db.delete_column(u'mususer_mususer', 'custom_url')

        # Adding field 'MusUser.group'
        db.add_column(u'mususer_mususer', 'group',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['auth.Group']),
                      keep_default=False)

        # Adding field 'MusUser.salt'
        db.add_column(u'mususer_mususer', 'salt',
                      self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.nickname'
        db.add_column(u'mususer_mususer', 'nickname',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.username'
        db.add_column(u'mususer_mususer', 'username',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.about_me'
        db.add_column(u'mususer_mususer', 'about_me',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.picture'
        db.add_column(u'mususer_mususer', 'picture',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.homepage'
        db.add_column(u'mususer_mususer', 'homepage',
                      self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.country'
        db.add_column(u'mususer_mususer', 'country',
                      self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.state'
        db.add_column(u'mususer_mususer', 'state',
                      self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.zip_code'
        db.add_column(u'mususer_mususer', 'zip_code',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.city'
        db.add_column(u'mususer_mususer', 'city',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.phone'
        db.add_column(u'mususer_mususer', 'phone',
                      self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.contactme'
        db.add_column(u'mususer_mususer', 'contactme',
                      self.gf('django.db.models.fields.IntegerField')(default=1, null=True),
                      keep_default=False)

        # Adding field 'MusUser.resume'
        db.add_column(u'mususer_mususer', 'resume',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.bio'
        db.add_column(u'mususer_mususer', 'bio',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.premium_expire'
        db.add_column(u'mususer_mususer', 'premium_expire',
                      self.gf('django.db.models.fields.CharField')(default=0, max_length=20, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.gift_expire'
        db.add_column(u'mususer_mususer', 'gift_expire',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'MusUser.approved'
        db.add_column(u'mususer_mususer', 'approved',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)

        # Adding field 'MusUser.register_date'
        db.add_column(u'mususer_mususer', 'register_date',
                      self.gf('django.db.models.fields.DateField')(auto_now_add=True, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.rates'
        db.add_column(u'mususer_mususer', 'rates',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.confirm_code'
        db.add_column(u'mususer_mususer', 'confirm_code',
                      self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.show_profile_completion'
        db.add_column(u'mususer_mususer', 'show_profile_completion',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)

        # Adding field 'MusUser.last_login_time'
        db.add_column(u'mususer_mususer', 'last_login_time',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.datetime(2013, 2, 8, 0, 0), blank=True),
                      keep_default=False)

        # Adding field 'MusUser.ref_link'
        db.add_column(u'mususer_mususer', 'ref_link',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.is_invited'
        db.add_column(u'mususer_mususer', 'is_invited',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


        # Changing field 'MusUser.links'
        db.alter_column(u'mususer_mususer', 'links', self.gf('django.db.models.fields.TextField')(null=True))

    def backwards(self, orm):
        # Adding field 'MusUser.website'
        db.add_column(u'mususer_mususer', 'website',
                      self.gf('django.db.models.fields.URLField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.about'
        db.add_column(u'mususer_mususer', 'about',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)

        # Adding field 'MusUser.name'
        db.add_column(u'mususer_mususer', 'name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128),
                      keep_default=False)

        # Adding field 'MusUser.custom_url'
        db.add_column(u'mususer_mususer', 'custom_url',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True),
                      keep_default=False)

        # Deleting field 'MusUser.group'
        db.delete_column(u'mususer_mususer', 'group_id')

        # Deleting field 'MusUser.salt'
        db.delete_column(u'mususer_mususer', 'salt')

        # Deleting field 'MusUser.nickname'
        db.delete_column(u'mususer_mususer', 'nickname')

        # Deleting field 'MusUser.username'
        db.delete_column(u'mususer_mususer', 'username')

        # Deleting field 'MusUser.about_me'
        db.delete_column(u'mususer_mususer', 'about_me')

        # Deleting field 'MusUser.picture'
        db.delete_column(u'mususer_mususer', 'picture')

        # Deleting field 'MusUser.homepage'
        db.delete_column(u'mususer_mususer', 'homepage')

        # Deleting field 'MusUser.country'
        db.delete_column(u'mususer_mususer', 'country')

        # Deleting field 'MusUser.state'
        db.delete_column(u'mususer_mususer', 'state')

        # Deleting field 'MusUser.zip_code'
        db.delete_column(u'mususer_mususer', 'zip_code')

        # Deleting field 'MusUser.city'
        db.delete_column(u'mususer_mususer', 'city')

        # Deleting field 'MusUser.phone'
        db.delete_column(u'mususer_mususer', 'phone')

        # Deleting field 'MusUser.contactme'
        db.delete_column(u'mususer_mususer', 'contactme')

        # Deleting field 'MusUser.resume'
        db.delete_column(u'mususer_mususer', 'resume')

        # Deleting field 'MusUser.bio'
        db.delete_column(u'mususer_mususer', 'bio')

        # Deleting field 'MusUser.premium_expire'
        db.delete_column(u'mususer_mususer', 'premium_expire')

        # Deleting field 'MusUser.gift_expire'
        db.delete_column(u'mususer_mususer', 'gift_expire')

        # Deleting field 'MusUser.approved'
        db.delete_column(u'mususer_mususer', 'approved')

        # Deleting field 'MusUser.register_date'
        db.delete_column(u'mususer_mususer', 'register_date')

        # Deleting field 'MusUser.rates'
        db.delete_column(u'mususer_mususer', 'rates')

        # Deleting field 'MusUser.confirm_code'
        db.delete_column(u'mususer_mususer', 'confirm_code')

        # Deleting field 'MusUser.show_profile_completion'
        db.delete_column(u'mususer_mususer', 'show_profile_completion')

        # Deleting field 'MusUser.last_login_time'
        db.delete_column(u'mususer_mususer', 'last_login_time')

        # Deleting field 'MusUser.ref_link'
        db.delete_column(u'mususer_mususer', 'ref_link')

        # Deleting field 'MusUser.is_invited'
        db.delete_column(u'mususer_mususer', 'is_invited')


        # Changing field 'MusUser.links'
        db.alter_column(u'mususer_mususer', 'links', self.gf('django.db.models.fields.TextField')(default=''))

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'musmusic.genre': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Genre'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'musmusic.instrument': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Instrument'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'learn_more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'mususer.mususer': {
            'Meta': {'object_name': 'MusUser'},
            'about_me': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'awards': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'bio': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'confirm_code': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'contactme': ('django.db.models.fields.IntegerField', [], {'default': '1', 'null': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'education': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'genre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Genre']", 'null': 'True', 'blank': 'True'}),
            'gift_expire': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Group']"}),
            'homepage': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instrument': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Instrument']", 'null': 'True', 'blank': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_invited': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_login_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'links': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'newsletter': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'nickname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'premium_expire': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'profile': ('django.db.models.fields.CharField', [], {'default': "'PRO'", 'max_length': '10'}),
            'rates': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'ref_link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'register_date': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'resume': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'salt': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'show_profile_completion': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'zip_code': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['mususer']