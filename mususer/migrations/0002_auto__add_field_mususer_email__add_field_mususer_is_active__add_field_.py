# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'MusUser.email'
        db.add_column(u'mususer_mususer', 'email',
                      self.gf('django.db.models.fields.EmailField')(default='', unique=True, max_length=255),
                      keep_default=False)

        # Adding field 'MusUser.is_active'
        db.add_column(u'mususer_mususer', 'is_active',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)

        # Adding field 'MusUser.is_admin'
        db.add_column(u'mususer_mususer', 'is_admin',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'MusUser.profile'
        db.add_column(u'mususer_mususer', 'profile',
                      self.gf('django.db.models.fields.CharField')(default='PRO', max_length=10),
                      keep_default=False)

        # Adding field 'MusUser.name'
        db.add_column(u'mususer_mususer', 'name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128),
                      keep_default=False)

        # Adding field 'MusUser.custom_url'
        db.add_column(u'mususer_mususer', 'custom_url',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.website'
        db.add_column(u'mususer_mususer', 'website',
                      self.gf('django.db.models.fields.URLField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.about'
        db.add_column(u'mususer_mususer', 'about',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)

        # Adding field 'MusUser.links'
        db.add_column(u'mususer_mususer', 'links',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)

        # Adding field 'MusUser.instrument'
        db.add_column(u'mususer_mususer', 'instrument',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['musmusic.Instrument'], null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.genre'
        db.add_column(u'mususer_mususer', 'genre',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['musmusic.Genre'], null=True, blank=True),
                      keep_default=False)

        # Adding field 'MusUser.education'
        db.add_column(u'mususer_mususer', 'education',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)

        # Adding field 'MusUser.awards'
        db.add_column(u'mususer_mususer', 'awards',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)

        # Adding field 'MusUser.newsletter'
        db.add_column(u'mususer_mususer', 'newsletter',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'MusUser.email'
        db.delete_column(u'mususer_mususer', 'email')

        # Deleting field 'MusUser.is_active'
        db.delete_column(u'mususer_mususer', 'is_active')

        # Deleting field 'MusUser.is_admin'
        db.delete_column(u'mususer_mususer', 'is_admin')

        # Deleting field 'MusUser.profile'
        db.delete_column(u'mususer_mususer', 'profile')

        # Deleting field 'MusUser.name'
        db.delete_column(u'mususer_mususer', 'name')

        # Deleting field 'MusUser.custom_url'
        db.delete_column(u'mususer_mususer', 'custom_url')

        # Deleting field 'MusUser.website'
        db.delete_column(u'mususer_mususer', 'website')

        # Deleting field 'MusUser.about'
        db.delete_column(u'mususer_mususer', 'about')

        # Deleting field 'MusUser.links'
        db.delete_column(u'mususer_mususer', 'links')

        # Deleting field 'MusUser.instrument'
        db.delete_column(u'mususer_mususer', 'instrument_id')

        # Deleting field 'MusUser.genre'
        db.delete_column(u'mususer_mususer', 'genre_id')

        # Deleting field 'MusUser.education'
        db.delete_column(u'mususer_mususer', 'education')

        # Deleting field 'MusUser.awards'
        db.delete_column(u'mususer_mususer', 'awards')

        # Deleting field 'MusUser.newsletter'
        db.delete_column(u'mususer_mususer', 'newsletter')


    models = {
        u'musmusic.genre': {
            'Meta': {'object_name': 'Genre'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'musmusic.instrument': {
            'Meta': {'object_name': 'Instrument'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'mususer.mususer': {
            'Meta': {'object_name': 'MusUser'},
            'about': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'awards': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'custom_url': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'education': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'genre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Genre']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instrument': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Instrument']", 'null': 'True', 'blank': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'links': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'newsletter': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'profile': ('django.db.models.fields.CharField', [], {'default': "'PRO'", 'max_length': '10'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        }
    }

    complete_apps = ['mususer']