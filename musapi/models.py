import hmac
import time
import uuid
from hashlib import sha1
from django.conf import settings
from django.db import models
from tastypie.utils import now
from tastypie.models import ApiAccess, ApiKey as TastyApiKey
from mususer.models import MusUser


class ApiKey(models.Model):
    user = models.OneToOneField(MusUser, related_name='api_key')
    key = models.CharField(
        max_length=256, blank=True, default='', db_index=True,
    )
    created = models.DateTimeField(default=now)

    def __unicode__(self):
        return u"%s for %s" % (self.key, self.user)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()

        return super(ApiKey, self).save(*args, **kwargs)

    def generate_key(self):
        # Get a random UUID.
        new_uuid = uuid.uuid4()
        # Hmac that beast.
        return hmac.new(str(new_uuid), digestmod=sha1).hexdigest()


def create_api_key(sender, **kwargs):
    """
    A signal for hooking up automatic ``ApiKey`` creation.
    """
    if kwargs.get('created') is True:
        ApiKey.objects.create(user=kwargs.get('instance'))
