from django.conf import settings
from django.contrib import admin

from musapi.models import ApiKey


class ApiKeyInline(admin.StackedInline):
    model = ApiKey
    extra = 0

# Also.
admin.site.register(ApiKey)
