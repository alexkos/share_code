from django.conf.urls import url
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash
from haystack.query import SearchQuerySet


class SearchableResource(ModelResource):

    musictype = 'music'

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/search%s$" % (
                    self._meta.resource_name, trailing_slash(),
                ),
                self.wrap_view('get_search'),
                name="api_get_search"
            ),
        ]

    def get_search(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        #self.is_authenticated(request)
        self.throttle_check(request)
        # Do the query.
        query = request.GET.get('q', u'')
        sqs = (
            SearchQuerySet()
            .filter(musictype=self.musictype)
            .filter(text=query)
        )

        paginator = self._meta.paginator_class(
            request.GET, sqs, resource_uri=self.get_resource_uri(),
            limit=self._meta.limit, max_limit=self._meta.max_limit,
            collection_name=self._meta.collection_name,
        )
        bundles = []
        to_be_serialized = paginator.page()
        for obj in to_be_serialized[self._meta.collection_name]:
            if obj and obj.object.__class__ is self._meta.object_class:
                obj.id = obj.pk
                bundle = self.build_bundle(obj=obj, request=request)
                bundles.append(self.full_dehydrate(bundle))

        to_be_serialized[self._meta.collection_name] = bundles
        to_be_serialized['meta']['total_count'] = len(bundles)
        #to_be_serialized = self.alter_list_data_to_serialize(request, to_be_serialized)
        #elf.log_throttled_access(request)
        return self.create_response(request, to_be_serialized)

    def apply_filters(self, request, applicable_filters):
        startswith_filters = {}

        semi_filtered = super(SearchableResource, self).apply_filters(
            request, applicable_filters,
        )

        if startswith_filters:
            for key, val in startswith_filters.items():
                semi_filtered = semi_filtered.filter(
                    Q(**{key: val[0]}) | Q(**{key: val[1]})
                )

        print unicode(semi_filtered.query)
        return semi_filtered
