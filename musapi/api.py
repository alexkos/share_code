from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie import fields
from musapi.resources import SearchableResource
from tastypie.authorization import Authorization, ReadOnlyAuthorization
from tastypie.http import HttpUnauthorized, HttpForbidden
from django.conf.urls import url
from django.core.exceptions import ObjectDoesNotExist
from tastypie.utils import trailing_slash
from django.contrib.auth import authenticate
from musapi.authentication import ApiKeyAuthentication
from musapi.authorization import (
    CustomAuthorization, UserObjectsOnlyAuthorization,
)
from tastypie.validation import Validation
from musapi.validation import UniqueBmarkValidation, UniqueSBmarkValidation
from musapi.models import ApiKey
from tastypie.api import Api

from musmusic.models import (
    Piece, SheetMusic, Part, SheetPart,
    Composer, Form, Instrument, Performer, Period, License,
    PiecePartBookmark, SheetPartBookmark,
)
from mususer.models import MusUser
from mususer.forms import UserCreationForm
from box_client.models import UploadedFile

v1_api = Api(api_name='v1')


class ComposerResource(ModelResource):
    class Meta:
        queryset = Composer.objects.all()
        resource_name = 'composer'
        excludes = ['approved', 'box_folder', 'box_folder_sheet']
        filtering = {
            'name': ALL,
            'created_date': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'id': ['exact'],
        }
        ordering = ['name', 'id']
        authentication = ApiKeyAuthentication()


class FormResource(ModelResource):
    class Meta:
        queryset = Form.objects.all()
        resource_name = 'form'
        excludes = ['approved', ]
        filtering = {
            'name': ALL,
            'created_date': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'id': ['exact'],
        }
        ordering = ['name', 'id']
        authentication = ApiKeyAuthentication()


class InstrumentResource(ModelResource):
    class Meta:
        queryset = Instrument.objects.all()
        resource_name = 'instrument'
        excludes = ['approved', ]
        filtering = {
            'name': ALL,
            'created_date': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'id': ['exact'],
        }
        ordering = ['name', 'id']
        authentication = ApiKeyAuthentication()


class PerformerResource(ModelResource):
    class Meta:
        queryset = Performer.objects.all()
        resource_name = 'performer'
        excludes = ['approved', ]
        filtering = {
            'name': ALL,
            'created_date': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'id': ['exact'],
        }
        ordering = ['name', 'id']
        authentication = ApiKeyAuthentication()


class PeriodResource(ModelResource):
    class Meta:
        queryset = Period.objects.all()
        resource_name = 'period'
        excludes = ['approved', ]
        filtering = {
            'name': ALL,
            'created_date': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
        }
        ordering = ['name', 'id']
        authentication = ApiKeyAuthentication()


class LicenseResource(ModelResource):
    class Meta:
        queryset = License.objects.all()
        resource_name = 'license'
        excludes = ['approved', 'created_date']
        filtering = {
            'name': ALL,
            'id': ['exact'],
        }
        ordering = ['name', 'id']
        authentication = ApiKeyAuthentication()


class FileResource(ModelResource):
    class Meta:
        queryset = UploadedFile.objects.all()
        resource_name = 'file'
        authentication = ApiKeyAuthentication()


class PartResource(ModelResource):
    piece = fields.ToOneField('musapi.api.PieceResource', 'piece', null=True)
    box_file = fields.ToOneField(
        'musapi.api.FileResource', 'box_file',
        null=True, full=True, full_list=True,
    )
    hq_box_file = fields.ToOneField(
        'musapi.api.FileResource', 'hq_box_file',
        null=True, full=True, full_list=True,
    )

    class Meta:
        queryset = Part.objects.all()
        resource_name = 'part'
        excludes = ['approved', 'filename', 'hq_filename', 'created_date']
        authentication = ApiKeyAuthentication()


class SheetPartResource(ModelResource):
    sheet = fields.ToOneField(
        'musapi.api.SheetMusicResource', 'sheet', null=True,
    )
    box_file = fields.ToOneField(
        'musapi.api.FileResource', 'box_file',
        null=True, full=True, full_list=True,
    )

    class Meta:
        queryset = SheetPart.objects.all()
        resource_name = 'sheetpart'
        excludes = ['approved', 'filename', 'hq_filename', 'created_date']
        authentication = ApiKeyAuthentication()


class PieceResource(SearchableResource):
    composer = fields.ToOneField(ComposerResource, 'composer', null=True)
    form = fields.ToOneField(FormResource, 'form', null=True)
    instrument = fields.ToOneField(InstrumentResource, 'instrument', null=True)
    performer = fields.ToOneField(PerformerResource, 'performer', null=True)
    period = fields.ToOneField(PeriodResource, 'period', null=True)
    license = fields.ToOneField(
        LicenseResource, 'license',
        null=True, full=True,
    )
    parts = fields.ToManyField(
        'musapi.api.PartResource', 'part_set',
        null=True, full=True,
    )

    class Meta:
        queryset = Piece.objects.all()
        resource_name = 'music'
        excludes = ['approved', ]
        allowed_methods = ['get']
        filtering = {
            'title': ALL,
            'upload_date': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'composer': ALL_WITH_RELATIONS,
            'form': ALL_WITH_RELATIONS,
            'instrument': ALL_WITH_RELATIONS,
            'performer': ALL_WITH_RELATIONS,
            'period': ALL_WITH_RELATIONS,
            'license': ALL_WITH_RELATIONS,
        }
        ordering = [
            'composer', 'form', 'instrument', 'performer', 'period', 'license',
            'title', 'upload_date', 'id',
        ]

        authentication = ApiKeyAuthentication()
        #authorization = CustomAuthorization()

    def get_list(self, request, **kwargs):
        self.fields.pop("comment", None)
        return super(PieceResource, self).get_list(request, **kwargs)

    def get_detail(self, request, **kwargs):
        self.fields["comment"] = fields.CharField(
            attribute='comment', null=True,
        )
        return super(PieceResource, self).get_detail(request, **kwargs)


class SheetMusicResource(SearchableResource):
    composer = fields.ToOneField(ComposerResource, 'composer', null=True)
    form = fields.ToOneField(FormResource, 'form', null=True)
    instrument = fields.ToOneField(InstrumentResource, 'instrument', null=True)
    period = fields.ToOneField(PeriodResource, 'period', null=True)
    parts = fields.ToManyField(
        'musapi.api.SheetPartResource', 'sheetpart_set',
        null=True, full=True,
    )

    class Meta:
        queryset = SheetMusic.objects.all()
        resource_name = 'sheetmusic'
        excludes = ['approved', ]
        allowed_methods = ['get']
        filtering = {
            'title': ALL,
            'upload_date': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'composer': ALL_WITH_RELATIONS,
            'form': ALL_WITH_RELATIONS,
            'instrument': ALL_WITH_RELATIONS,
            'period': ALL_WITH_RELATIONS,
        }
        ordering = [
            'composer', 'form', 'instrument', 'period',
            'title', 'upload_date', 'id',
        ]
        authentication = ApiKeyAuthentication()
        #authorization = CustomAuthorization()

    def get_list(self, request, **kwargs):
        self.fields.pop("comment", None)
        return super(SheetMusicResource, self).get_list(request, **kwargs)

    def get_detail(self, request, **kwargs):
        self.fields["comment"] = fields.CharField(
            attribute='comment', null=True,
        )
        return super(SheetMusicResource, self).get_detail(request, **kwargs)


class UserResource(ModelResource):
    class Meta:
        queryset = MusUser.objects.all()
        resource_name = 'user'
        fields = ['username', 'email', 'id']
        allowed_methods = ['get']
        authentication = ApiKeyAuthentication()

    def authorized_read_list(self, object_list, bundle):
        return object_list.filter(id=bundle.request.user.id)

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/login%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('login'), name="api_login"),
            url(r'^(?P<resource_name>%s)/registration%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('registration'), name='api_registration'),
        ]

    def login(self, request, **kwargs):
        self.method_check(request, allowed=['post'])

        data = self.deserialize(
            request, request.raw_post_data,
            format=request.META.get('CONTENT_TYPE', 'application/json'),
        )
        email = data.get('email', '')
        password = data.get('password', '')
        user = authenticate(username=email, password=password)
        if user:
            if user.is_active:
                try:
                    apikey = user.api_key
                except ObjectDoesNotExist:
                    apikey = ApiKey(user=user)
                    apikey.save()
                return self.create_response(request, {
                    'success': True,
                    'apikey': apikey.key,
                })
            else:
                return self.create_response(
                    request,
                    {
                        'success': False,
                        'reason': 'disabled',
                    },
                    HttpUnauthorized,
                )
        else:
            return self.create_response(
                request,
                {
                    'success': False,
                    'reason': 'incorrect',
                },
                HttpUnauthorized,
            )

    def registration(self, request, **kwargs):
        self.method_check(request, allowed=['post'])

        data = self.deserialize(
            request, request.raw_post_data,
            format=request.META.get('CONTENT_TYPE', 'application/json'),
        )

        form = UserCreationForm(data)
        if form.is_valid():
            user = MusUser.objects.create_user(
                email=form.cleaned_data['email'],
                password=form.cleaned_data['password1'])
            user.username = form.cleaned_data['username']
            user.save()
            try:
                apikey = user.api_key
            except ObjectDoesNotExist:
                apikey = ApiKey(user=user)
                apikey.save()
            return self.create_response(request, {
                'success': True,
                'apikey': apikey.key,
            })
        else:
            return self.create_response(request, {
                'success': False,
                'reason': form.errors,
            }, HttpForbidden)


class MusicBookmarkResource(ModelResource):
    part = fields.ToOneField(PartResource, 'part', full=True, full_list=True)
    user = fields.ToOneField(UserResource, 'user')

    class Meta:
        queryset = PiecePartBookmark.objects.all()
        resource_name = 'musicbmark'
        allowed_methods = ['get', 'post', 'delete']
        authentication = ApiKeyAuthentication()
        authorization = UserObjectsOnlyAuthorization()
        validation = UniqueBmarkValidation()


class SheetMusicBookmarkResource(ModelResource):
    part = fields.ToOneField(
        SheetPartResource, 'part',
        full=True, full_list=True,
    )
    user = fields.ToOneField(UserResource, 'user')

    class Meta:
        queryset = SheetPartBookmark.objects.all()
        resource_name = 'sheetmusicbmark'
        allowed_methods = ['get', 'post', 'delete']
        authentication = ApiKeyAuthentication()
        authorization = UserObjectsOnlyAuthorization()
        validation = UniqueSBmarkValidation()


v1_api.register(ComposerResource())
v1_api.register(FormResource())
v1_api.register(InstrumentResource())
v1_api.register(PerformerResource())
v1_api.register(PeriodResource())
v1_api.register(LicenseResource())

v1_api.register(FileResource())
v1_api.register(PartResource())
v1_api.register(SheetPartResource())

v1_api.register(PieceResource())
v1_api.register(SheetMusicResource())
v1_api.register(UserResource())

v1_api.register(MusicBookmarkResource())
v1_api.register(SheetMusicBookmarkResource())
