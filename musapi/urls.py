from django.conf.urls import patterns, include, url
from django.conf import settings
from musapi.api import v1_api

urlpatterns = patterns(
    '',
    (r'^', include(v1_api.urls)),
)
