from tastypie.validation import Validation
from musmusic.models import PiecePartBookmark, SheetPartBookmark
import musapi.api


class UniqueBmarkValidation(Validation):
    def is_valid(self, bundle, request=None):
        if not bundle.data:
            return {'__all__': 'Data not found.'}

        if 'user' in bundle.data:
            user = bundle.data['user']
        else:
            return {'__all__': 'User not found.'}

        if 'part' in bundle.data:
            part = bundle.data['part']
        else:
            return {'__all__': 'Part not found.'}

        user_resource = musapi.api.UserResource()
        user = user_resource.get_via_uri(user)
        if not user:
            return {'user': ['Invalid user']}
        part_resource = musapi.api.PartResource()
        part = part_resource.get_via_uri(part)
        if not part:
            return {'part': ['Invalid part']}
        if PiecePartBookmark.objects.filter(user=user, part=part).exists():
            return {'part': ['Bookmark already created.']}
        else:
            return {}


class UniqueSBmarkValidation(Validation):
    def is_valid(self, bundle, request=None):
        if not bundle.data:
            return {'__all__': 'Data not found.'}

        if 'user' in bundle.data:
            user = bundle.data['user']
        else:
            return {'__all__': 'User not found.'}

        if 'part' in bundle.data:
            part = bundle.data['part']
        else:
            return {'__all__': 'Part not found.'}

        user_resource = musapi.api.UserResource()
        user = user_resource.get_via_uri(user)
        if not user:
            return {'user': ['Invalid user']}
        part_resource = musapi.api.PartResource()
        part = part_resource.get_via_uri(part)
        if not part:
            return {'part': ['Invalid part']}
        if SheetPartBookmark.objects.filter(user=user, part=part).exists():
            return {'part': ['Bookmark already created.']}
        else:
            return {}
