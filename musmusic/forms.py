#-*- coding: utf-8 -*-

from django.conf import settings
from django import forms
from django.utils.translation import ugettext, ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify
from django.db.models import Q

import requests

from tinymce.widgets import TinyMCE

from crispy_forms.helper import FormHelper
from crispy_forms.layout import (
    Layout, Field, Fieldset, Submit, Div, HTML,
)
from musmusic.layout import Table, Tr, Td, Thead, Tbody, CustomSubmit
from crispy_forms.bootstrap import AppendedText

from musmusic.models import SheetMusic, Piece, SheetPart, Part
from musmusic.models import Composer, Performer, Instrument, Period, Form
from mususer.models import MusUser

from box_client.client import BoxClient
from box_client.models import Token, UploadedFile, Folders
from box_client.exceptions import AccessError

LINK_FMT = '<a href="{}" class="ajax-add"><i class="icon-plus"></i>{}</a>'
LINK_INTRO_FMT = (
    '<a href="{}" class="ajax-add" '
    'data-step="{}" data-intro="{}"><i class="icon-plus"></i>{}</a>'
)


class BoxIdField(forms.CharField):
    def validate(self, value):
        "Check if file id is not already used in DB"
        if value and UploadedFile.objects.filter(box_id=value).exists():
            raise forms.ValidationError("Seems like file is already used in DB")
        super(BoxIdField, self).validate(value)


class PieceEditForm(forms.ModelForm):
    """
        User's piece edit and add form
    """

    title = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'input-xlarge'})
    )
    comment = forms.CharField(
        widget=TinyMCE(
            attrs={'class': 'input-xxlarge', 'rows': 8,},
            mce_attrs=settings.TINYMCE_SIMPLE_CONFIG,
        ),
#        widget=forms.Textarea(attrs={'class': 'input-xxlarge', 'rows': 8,}),
        required=False,
    )

    def music_or_none(self, arg):
        if self.musictype == 'music':
            return arg
        else:
            return None

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        self.musictype = kwargs.pop('musictype')
        if self.musictype == 'music':
            self._meta.fields += ('performer',)
        else:
            self._meta.model = SheetMusic
        posted_data = kwargs.get('data')

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_id = 'piece-edit-form'
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            Fieldset(
                'Meta data',
                Div('title', data_step=2, data_intro='Edit title here'),
                Div(
                    AppendedText(
                        'composer',
                        LINK_INTRO_FMT.format(
                            reverse('new_object', args=('composer',)),
                            4, "You can add new composer by clicking on + button",
                            unicode(_("New Composer")),
                        ),
                    ),
                    data_step=3,
                    data_intro='Select composer from dropdown',
                ),
                self.music_or_none(AppendedText(
                    'performer',
                    LINK_FMT.format(
                        reverse('new_object', args=('performer',)),
                        unicode(_("New Performer"))
                    )
                )),
                AppendedText(
                    'instrument',
                    LINK_FMT.format(
                        reverse('new_object', args=('instrument',)),
                        unicode(_("New Instrument"))
                    )
                ),
                AppendedText(
                    'period',
                    LINK_FMT.format(
                        reverse('new_object', args=('period',)),
                        unicode(_("New Period"))
                    )
                ),
                AppendedText(
                    'form',
                    LINK_FMT.format(
                        reverse('new_object', args=('form',)),
                        unicode(_("New Form"))
                    )
                ),
                Div(
                    'license',
                    data_step=5,
                    data_intro='Select right license',
                ),
                Div(
                    'comment',
                    data_step=6,
                    data_intro='Write good description with in WYSIWYG editor',
                ),
            ),

        )
        self.helper.add_input(Submit(
            'submit', _('Save'),
            data_step=7,
            data_intro='Do not forget to save changes',
        ))
        self.helper.add_input(
            CustomSubmit(
                'submit_and_continue',
                _('Save and continue'),
            )
        )
        super(PieceEditForm, self).__init__(*args, **kwargs)
        if self.musictype == 'music':
            self.fields['performer'] = forms.ModelChoiceField(
                label="Performer",
                queryset=Performer.objects.all(),
                required=False,
            )
        part_count = 0
        divs = []
        rows = []
        if self.instance:
            if self.musictype == 'music':
                manager = self.instance.part_set
            else:
                manager = self.instance.sheetpart_set
            instance = self.instance
            initial_part_count = manager.count()
            if posted_data and int(posted_data.get('part_count')) > initial_part_count:
                posted_part_count = int(posted_data.get('part_count'))
            else:
                posted_part_count = initial_part_count

            parts = manager.all()
            for part in list(parts) + [None for x in range(initial_part_count, posted_part_count)] + [False]:
                if part != False:
                    # Hidden row as a template for new rows
                    i = part_count
                    part_count += 1
                    row_style = "";
                else:
                    i = '__INSERT_ROW_NUMBER__'
                    row_style = "display: none;";
                if part:
                    part_number = part.number
                    part_id = part.id
                    part_title = part.title
                    part_file_id = part.box_file_id
                    if part_file_id:
                        part_filename = part.filename
                    else:
                        part_filename = ''
                    if self.musictype == 'music':
                        part_hq_file_id = part.hq_box_file_id
                        if part_hq_file_id:
                            part_hq_filename = part.hq_filename
                        else:
                            part_hq_filename = ''
                    if self.user.is_staff:
                        delete_style = ''
                    else:
                        delete_style = 'display:none;'
                else:
                    part_number = 0
                    part_id = None
                    part_title = ''
                    part_filename = ''
                    part_file_id = None
                    if self.musictype == 'music':
                        part_hq_filename = ''
                        part_hq_file_id = None
                    delete_style = ''

                self.fields['part_number_{}'.format(i)] = (
                    forms.IntegerField(
                        initial=part_number,
                        widget=forms.HiddenInput,
                    )
                )
                self.fields['part_id_{}'.format(i)] = (
                    forms.IntegerField(
                        required=False, initial=part_id,
                        widget=forms.HiddenInput,
                    )
                )
                self.fields['part_title_{}'.format(i)] = (
                    forms.CharField(
                        required=(part != False),
                        initial=part_title,
                    )
                )
                self.fields['part_filename_{}'.format(i)] = (
                    forms.CharField(
                        required=False,
                        initial=part_filename,
                        widget=forms.HiddenInput,
                    )
                )
                self.fields['part_file_id_{}'.format(i)] = (
                    forms.IntegerField(
                        required=False,
                        initial=part_file_id,
                        widget=forms.HiddenInput,
                    )
                )
                self.fields['part_file_boxid_{}'.format(i)] = (
                    BoxIdField(required=False, widget=forms.HiddenInput)
                )

                if self.musictype == 'music':
                    self.fields['part_hq_filename_{}'.format(i)] = (
                        forms.CharField(
                            required=False,
                            initial=part_hq_filename,
                            widget=forms.HiddenInput,
                        )
                    )
                    self.fields['part_hq_file_id_{}'.format(i)] = (
                        forms.IntegerField(
                            required=False,
                            initial=part_hq_file_id,
                            widget=forms.HiddenInput,
                        )
                    )
                    self.fields['part_hq_file_boxid_{}'.format(i)] = (
                        BoxIdField(required=False, widget=forms.HiddenInput)
                    )

                self.fields['part_delete_file_{}'.format(i)] = (
                    forms.BooleanField(
                        initial=False,
                        required=False,
                        widget=forms.HiddenInput,
                    )
                )
                if self.musictype == 'music':
                    self.fields['part_delete_hq_file_{}'.format(i)] = (
                        forms.BooleanField(
                            initial=False,
                            required=False,
                            widget=forms.HiddenInput,
                        )
                    )
                self.fields['delete_part_{}'.format(i)] = (
                    forms.BooleanField(
                        initial=False, required=False,
                        widget=forms.HiddenInput,
                    )
                )

                rows.append(
                    Tr(
                        Td(
                            HTML('<div class="drag" style="/*opacity: 0;*/">&nbsp;</div>'),
                            Field(
                                'part_id_{}'.format(i),
                                template='custom_crispy/clear-field.html',
                                readonly=True,
                                css_class="part_id",
                            ),
                            Field(
                                'part_number_{}'.format(i),
                                template='custom_crispy/clear-field.html',
                            ),
                            Field(
                                'delete_part_{}'.format(i),
                                css_class="delete-part-input",
                                data_row_number=i,
                            ),
                        ),
                        Td(
                            Field(
                                'part_title_{}'.format(i),
                                css_class="input-xxlarge",
                                template='custom_crispy/clear-field.html',
                            ),
                            Field(
                                'part_filename_{}'.format(i),
                                template='custom_crispy/clear-field.html',
                            ),
                            Field(
                                'part_file_id_{}'.format(i),
                                template='custom_crispy/clear-field.html',
                            ),
                            Field(
                                'part_file_boxid_{}'.format(i),
                                template='custom_crispy/clear-field.html',
                            ),
                            Field(
                                'part_delete_file_{}'.format(i),
                                template='custom_crispy/clear-field.html',
                            ),
                            Field(
                                'part_hq_filename_{}'.format(i),
                                template='custom_crispy/clear-field.html',
                            ),
                            Field(
                                'part_hq_file_id_{}'.format(i),
                                template='custom_crispy/clear-field.html',
                            ),
                            Field(
                                'part_hq_file_boxid_{}'.format(i),
                                template='custom_crispy/clear-field.html',
                            ),
                            Field(
                                'part_delete_hq_file_{}'.format(i),
                                template='custom_crispy/clear-field.html',
                            ),
                        ),
                        Td(
                            HTML('''<a
        class="delete delete-part" href="#"
        data-scope="part" data-id="part-table-row-{0}"
        data-row-number="{0}" style="{1}">
                                    <i class="icon icon-trash"></i>
                                </a>'''.format(i, delete_style)
                            ),
                            colspan="2"
                        ),  # Td

                        data_row_number=i,
                        css_class="part-row",
                        css_id="part-table-row-{}".format(i),
                        style=row_style,
                    ),  # Tr
                )

            self.fields['part_count'] = forms.IntegerField(
                initial=part_count,
                widget=forms.HiddenInput,
            )
            divs.append('part_count')
            thead = Thead(
                Tr(
                    HTML('<th></th>'),
                    HTML('<th>Title</th>'),
                    HTML('<th>Delete</th>'),
                    css_class="part-table-headrow",
                )
            )
            rows.append(
                Tr(
                    Td(
                        HTML(
                            '''<a class="add-part pull-right" href="#">
                                    Upload additional part
                                    <i class="icon-plus"></i>
                                </a>'''
                        ),
                        colspan="3",
                    ),
                ),
            )

            if posted_part_count:
                table_style = ''
                box_style = 'display: none;'
            else:
                table_style = 'display: none;'
                box_style = ''

            self.helper.layout = Layout(
                self.helper.layout,
                Fieldset(
                    'Files',
                    Div(
                        HTML('''Great!<br /> Now please make sure
                             the names are correct for everything,
                             and they are in the order you like
                             (drag to re-arrange).'''),
                        css_id="second-upload-note",
                        style="display:none;",
                    ),
                    Div(
                        HTML('<h2>Drag music files here</h2>'),
                        Div(
                            HTML(
                                '''<small>Or, if you prefer...</small><br />
                                <input type="file" id="id_file_select" multiple="multiple" />
                                <a href="#" id="file-select-link" class="btn btn-success">choose files to upload</a>'''
                            ),
                            Div(HTML('&nbsp;'), css_id="uploadMessage"),
                            css_id='fileInput',
                        ),
                        HTML('<progress value="0" max="100" style="opacity:0;" class="upload"></progress>'),
                        css_id='dropZone',
                        style=box_style,
                    ),
                    Div(
                        HTML('''<a class="hide_dropzone" href="#" style="display: none;">Show already uploaded files</a>'''),
                    ),
                    Table(
                        thead, Tbody(*rows),
                        css_class="table table-hover", css_id="parts-table", style=table_style,
                    ),
                    *divs
                )
            )

    def save(self, *args, **kwargs):
        if self.instance and not self.instance.user_id:
            self.instance.user = self.user
        super(PieceEditForm, self).save(*args, **kwargs)
        piece = self.instance
        data = self.cleaned_data
        created_parts = {}
        if self.musictype == 'music':
            manager = self.instance.part_set
        else:
            manager = self.instance.sheetpart_set
        for i in range(data['part_count']):  # Saving cycle
            part_id = data.get('part_id_{}'.format(i))
            part_deleted = data.get('delete_part_{}'.format(i))

            file_id = data.get('part_file_id_{}'.format(i))
            file_boxid = data.get('part_file_boxid_{}'.format(i))
            file_deleted = data.get('part_delete_file_{}'.format(i))
            if self.musictype == 'music':
                hq_file_id = data.get('part_hq_file_id_{}'.format(i))
                hq_file_boxid = data.get('part_hq_file_boxid_{}'.format(i))
                hq_file_deleted = data.get('part_delete_hq_file_{}'.format(i))

            if part_id:
                part = manager.get(id=part_id)
                part.title = data.get('part_title_{}'.format(i))
                part.number = data.get('part_number_{}'.format(i))
                if file_id or file_boxid:
                    part.filename = data.get('part_filename_{}'.format(i))
                if self.musictype == 'music' and (hq_file_id or hq_file_boxid):
                    part.hq_filename = data.get('part_hq_filename_{}'.format(i))
            else:
                part = manager.create(
                    title = data.get('part_title_{}'.format(i)),
                    number = data.get('part_number_{}'.format(i)),
                    created_by = self.user,
                )
                if file_id or file_boxid:
                    part.filename = data.get('part_filename_{}'.format(i))
                if self.musictype == 'music' and (hq_file_id or hq_file_boxid):
                    part.hq_filename = data.get('part_hq_filename_{}'.format(i))
                created_parts[i] = part

            if file_id and not file_boxid:  # Situation 1
                # File was moved or remains here
                # NOTE: We don't check for deleted and just save data
                # to simplify deletion in the next cycle
                if not part.box_file_id or (
                    file_id != part.box_file_id and self.user.is_staff
                ):
                    part.box_file = UploadedFile.objects.get(id=file_id)
            elif not file_id and file_boxid:  # Situation 2
                # File was uploaded here
                if not part.box_file_id or self.user.is_staff:
                    if not part_deleted and not file_deleted:
                        # Do not upload if file or part was deleted
                        part.box_file = self.finish_uploading(
                            file_boxid, part.filename, part,
                        )
                    elif self.user.is_staff:
                        # TODO: delete file from upload bucket?
                        part.box_file = None
            elif file_id and file_boxid and self.user.is_staff:  # Situation 3
                # File was uploaded but there is old file's id in another field
                # Strange situation, actually, it should not happen at all,
                # but it's here just in case. And we don't do anything with
                # saved files for non-staff user
                if not part_deleted and not file_deleted:
                    UploadedFile.objects.get(id=file_id).safe_delete(part)
                    part.box_file = self.finish_uploading(
                        file_boxid, part.filename, part,
                    )
                else:
                    # TODO: delete file from upload bucket?
                    # Do not set None because we need to delete this file too
                    pass
            elif not file_id and not file_boxid:  # Situation 4
                # File was moved elsewhere or never was here
                if part.box_file_id and self.user.is_staff:
                    part.box_file = None

            if self.musictype == 'music':  # Same stuff for HQ files
                # FIXME: I don't know how to get rid of code duplicity easily
                if hq_file_id and not hq_file_boxid:  # Situation 1 (HQ)
                    if not part.hq_box_file_id or (
                        hq_file_id != part.hq_box_file_id and self.user.is_staff
                    ):
                        part.hq_box_file = UploadedFile.objects.get(id=hq_file_id)
                elif not hq_file_id and hq_file_boxid:  # Situation 2 (HQ)
                    if not part.hq_box_file_id or self.user.is_staff:
                        if not part_deleted and not hq_file_deleted:
                            part.hq_box_file = self.finish_uploading(
                                hq_file_boxid, part.hq_filename, part,
                            )
                        elif self.user.is_staff:
                            # TODO: delete file from upload bucket?
                            part.hq_box_file = None
                elif hq_file_id and hq_file_boxid and self.user.is_staff:
                    # Situation 3 (HQ)
                    if not part_deleted and not hq_file_deleted:
                        UploadedFile.objects.get(id=hq_file_id).safe_delete(part)
                        part.hq_box_file = self.finish_uploading(
                            hq_file_boxid, part.hq_filename, part,
                        )
                    else:
                        pass # TODO: remove from box?
                elif not hq_file_id and not hq_file_boxid:  # Situation 4 (HQ)
                    if part.hq_box_file_id and self.user.is_staff:
                        part.hq_box_file = None
            part.save()  # Postponed saving

        for i in range(data['part_count']):  # Deletion cycle
            part_id = data.get('part_id_{}'.format(i))
            part_deleted = data.get('delete_part_{}'.format(i))
            if part_id:
                part = manager.get(id=part_id)
            else:
                part = created_parts[i]
            file_id = data.get('part_file_id_{}'.format(i))
            file_boxid = data.get('part_file_boxid_{}'.format(i))
            file_deleted = data.get('part_delete_file_{}'.format(i))
            if self.musictype == 'music':
                hq_file_boxid = data.get('part_hq_file_boxid_{}'.format(i))
                hq_file_id = data.get('part_hq_file_id_{}'.format(i))
                hq_file_deleted = data.get('part_delete_hq_file_{}'.format(i))

            if part_deleted:
                if not part_id or self.user.is_staff:
                    if part.box_file_id:
                        part.box_file.safe_delete(part)  # TODO: we need safe delete
                    if self.musictype == 'music':
                        if part.hq_box_file_id:
                            part.hq_box_file.safe_delete(part)
                    part.delete()
            elif self.user.is_staff:
                # Delete existing files only for staff
                # not saved files just don't need deletion
                if file_deleted and file_id:
                    UploadedFile.objects.get(id=file_id).safe_delete(part)
                if self.musictype == 'music':
                    if hq_file_deleted and hq_file_id:
                        UploadedFile.objects.get(id=hq_file_id).safe_delete(part)

            if self.musictype == 'music' and part and part.id:
                # Encode HQ to mp3 if needed
                # We do it in the delete cycle in order not to encode anything deleted
                if part.hq_box_file and not part.box_file:
                    if self.instance.composer:
                        box_folder = self.instance.composer.get_folder(self.musictype)
                    else:
                        box_folder = Folders.objects.get(
                            description='no-composer-{}'.format(self.musictype),
                        )
                    token = Token.objects.filter(name='download').latest('id')
                    requests.get(
                        settings.ENCODER_BASE_URL.format(part.id),
                        params={
                            'url': part.hq_box_file.shared_url,
                            'filename': part.hq_box_file.name,
                            'auth_code': settings.ENCODER_AUTH_CODE,
                            'parent_id': box_folder.box_id,
                            'box_token': token.access_token,
                        }
                    )

    def finish_uploading(self, box_id, filename, part):
        musictype = self.musictype
        upload_filename = '-'.join(
            (
                self.instance.title, str(part.number), part.title,
                str(self.instance.id), str(part.id),
            )
        )
        if '.' in filename:
            suffix = filename.rsplit(".", 1)[-1].lower()
            upload_filename = upload_filename + '.' + suffix
        if self.instance.composer:
            box_folder = self.instance.composer.get_folder(musictype)
        else:
            box_folder = Folders.objects.get(
                description='no-composer-{}'.format(musictype),
            )

        box = BoxClient(token_name="download")
        # XXX: May cause name conflict error here, need to think about it
        box.move_file(box_id, box_folder.box_id, upload_filename)
        file_obj = box.share_file(box_id)
        if file_obj.path_collection:
            description = ''.join(
                [
                    '/' + entry.get('name', '')
                    for entry in file_obj.path_collection.get('entries')
                ]
            ) + '/' + file_obj.name
        else:
            description = upload_filename
        box_file = UploadedFile(
            box_id=box_id,
            name=upload_filename,
            description=description,
            shared_url=file_obj.shared_link.get('download_url'),
        )
        box_file.save()
        return box_file

    class Meta:
        model = Piece
        fields = (
            'title', 'composer', 'instrument',
            'period', 'form', 'license', 'comment',
        )


class AjaxForm(forms.ModelForm):
    """
        Abstract form class for ajaz objects adding
    """

    objecttype = 'abstractobject'
    comment = forms.CharField(
        widget=TinyMCE(
            attrs={'class': 'input-xxlarge', 'rows': 8,},
            mce_attrs=settings.TINYMCE_SIMPLE_CONFIG,
        ),
#        widget=forms.Textarea(attrs={'class': 'input-xxlarge', 'rows': 8,}),
        required=False,
    )

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal ajax-form'
        self.helper.form_id = self.objecttype + '-form'
        self.helper.attrs = {'data-select': self.objecttype}
        self.helper.form_method = 'post'
        if 'instance' in kwargs:
            instance = kwargs['instance']
            self.helper.form_action = reverse(
                'edit_object',
                args=(self.objecttype, instance.id),
            )
        else:
            self.helper.form_action = reverse(
                'new_object',
                args=(self.objecttype,),
            )
        self.helper.add_input(Submit('submit', _('Done')))
        super(AjaxForm, self).__init__(*args, **kwargs)

    def clean_name(self):
        if not 'name' in self.Meta.fields:
            return
        name = self.cleaned_data['name']
        slug = slugify(name)
        if self.instance:
            instance_id = self.instance.id
        else:
            instance_id = -1
        if (
                self.Meta.model.objects
                .filter(name=name).exclude(id=instance_id).exists()
        ):
            raise forms.ValidationError(
                "Object with this name already exists in database"
            )
        elif hasattr(self.Meta.model, 'slug') and (
                self.Meta.model.objects
                .filter(slug=slug).exclude(id=instance_id).exists()
        ):
            raise forms.ValidationError(
                "Object with this name already exists in database"
            )
        else:
            return name


class ComposerForm(AjaxForm):

    objecttype = 'composer'

    class Meta:
        model = Composer
        fields = ('first_name', 'last_name', 'learn_more', 'comment')

    def clean_last_name(self):
        first_name = self.cleaned_data.get('first_name')
        last_name = self.cleaned_data.get('last_name')
        if last_name:
            slug = slugify(first_name + '-' + last_name)
            name = first_name + ' ' + last_name
        else:
            slug = slugify(first_name)
            name = first_name
        if self.instance:
            instance_id = self.instance.id
        else:
            instance_id = -1
        if (
                self.Meta.model.objects
                .filter(slug=slug).exclude(id=instance_id).exists()
        ) or (
                self.Meta.model.objects
                .filter(name=name).exclude(id=instance_id).exists()
        ):
            raise forms.ValidationError(
                "Composer with this name already exists in database"
            )
        else:
            return last_name


class PerformerForm(AjaxForm):

    objecttype = 'performer'

    class Meta:
        model = Performer
        fields = ('name', 'learn_more', 'comment')


class FormForm(AjaxForm):

    objecttype = 'form'

    class Meta:
        model = Form
        fields = ('name', 'learn_more', 'comment')


class InstrumentForm(AjaxForm):

    objecttype = 'instrument'

    class Meta:
        model = Instrument
        fields = ('name', 'learn_more', 'comment')


class PeriodForm(AjaxForm):

    objecttype = 'period'

    class Meta:
        model = Period
        fields = ('name', 'more', 'comment')


class BoxForm(forms.Form):
    box_file = forms.ChoiceField(
        label="File",
        widget=forms.Select(attrs={'class': 'input-xxlarge'}),
    )

    def __init__(self, *args, **kwargs):
        files = kwargs.pop('files')
        action = kwargs.pop('action')
        self.helper = FormHelper()
        self.helper.form_action = action
        self.helper.form_class = 'form-horizontal ajax-set-file-form'
        self.helper.form_id = 'set-file-form'
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', _('Done')))
        super(BoxForm, self).__init__(*args, **kwargs)
        if 'box_file' in self.fields:
            self.fields['box_file'].widget.choices = [
                (f.id, f.name) for f in files
            ]
            widget = self.fields['box_file'].widget
            if 'class' in widget.attrs:
                widget.attrs['class'] += ' input-xxlarge'
            else:
                widget.attrs['class'] = 'input-xxlarge'


class PieceForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(PieceForm, self).__init__(*args, **kwargs)
        # FIXME: This hack does not work, I don't know why yet
        if self.fields['instrument'].widget.attrs.get('class'):
            self.fields['instrument'].widget.attrs["class"] += ' input-xlarge'
        else:
            self.fields['instrument'].widget.attrs["class"] = 'input-xlarge'

    class Meta:
        model = Piece
        exclude = ('approved',)


class MergeForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)

    def __init__(self, *args, **kwargs):
        queryset = kwargs.pop('queryset')
        super(MergeForm, self).__init__(*args, **kwargs)
        self.fields['target'] = forms.ModelChoiceField(
            label=u'Merge target',
            queryset=queryset,
        )


class PieceMassForm(forms.Form):
    composer = forms.ModelChoiceField(
        label="Composer",
        queryset=Composer.objects.all().defer(
            'comment', 'learn_more', 'image',
            'box_folder', 'box_folder_sheet', 'created_date'
        ),
        required=False,
    )
    performer = forms.ModelChoiceField(
        label="Performer",
        queryset=Performer.objects.all(),
        required=False,
    )
    instrument = forms.ModelChoiceField(
        label="Instrument",
        queryset=Instrument.objects.all(),
        required=False,
    )
    period = forms.ModelChoiceField(
        label="Period",
        queryset=Period.objects.all(),
        required=False,
    )
    form = forms.ModelChoiceField(
        label="Form",
        queryset=Form.objects.all(),
        required=False,
    )

    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)

    def __init__(self, *args, **kwargs):
        queryset = kwargs.pop('queryset')
        super(PieceMassForm, self).__init__(*args, **kwargs)
        if queryset.model._meta.module_name == 'sheetmusic':
            del self.fields['performer']
        initial_data = {}
        for field in self.fields:
            if field in (
                'composer', 'performer', 'instrument', 'period', 'form',
            ):
                initial_data[field] = []
                for item in queryset:
                    if getattr(item, field):
                        if getattr(item, field).id not in initial_data[field]:
                            initial_data[field].append(getattr(item, field).id)
                    else:
                        if 0 not in initial_data[field]:
                            initial_data[field].append(0)
        for field, lst in initial_data.items():
            if lst and len(lst) == 1:
                self.fields[field].initial = lst[0]


class PieceConnectForm(forms.Form):

    def __init__(self, *args, **kwargs):
        action = kwargs.pop('action')
        self.helper = FormHelper()
        self.helper.form_action = action
        self.helper.form_class = 'form-vertical ajax-connect-file-form'
        self.helper.form_id = 'connect-file-form'
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            Fieldset(
                'Select piece you want to connect file to',
                Field('piece', css_class="input-xxlarge"),
            )
        )
        self.helper.add_input(Submit('submit', _('Connect')))
        super(PieceConnectForm, self).__init__(*args, **kwargs)
        self.fields['piece'] = forms.ModelChoiceField(
            label=u'Piece',
            queryset=Piece.objects.all(),
        )


class SheetConnectForm(forms.Form):

    def __init__(self, *args, **kwargs):
        action = kwargs.pop('action')
        self.helper = FormHelper()
        self.helper.form_action = action
        self.helper.form_class = 'form-vertical ajax-connect-file-form'
        self.helper.form_id = 'connect-file-form'
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            Fieldset(
                'Select sheet you want to connect file to',
                Field('piece', css_class="input-xxlarge"),
            )
        )

        self.helper.add_input(Submit('submit', _('Connect')))
        super(SheetConnectForm, self).__init__(*args, **kwargs)
        self.fields['piece'] = forms.ModelChoiceField(
            label=u'Sheet',
            queryset=SheetMusic.objects.all(),
        )

class PieceCreateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        action = kwargs.pop('action')
        self.helper = FormHelper()
        self.helper.form_action = action
        self.helper.form_class = 'form-horizontal ajax-connect-file-form'
        self.helper.form_id = 'connect-file-form'
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', _('Create')))
        super(PieceCreateForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Piece
        fields = (
            'title', 'composer', 'performer', 'instrument',
            'period', 'form', 'license', 'comment',
        )


class SheetCreateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        action = kwargs.pop('action')
        self.helper = FormHelper()
        self.helper.form_action = action
        self.helper.form_class = 'form-horizontal ajax-connect-file-form'
        self.helper.form_id = 'connect-file-form'
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', _('Create')))
        super(SheetCreateForm, self).__init__(*args, **kwargs)

    class Meta:
        model = SheetMusic
        fields = (
            'title', 'composer', 'instrument',
            'period', 'form', 'comment',
        )


class MassConnectForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)

    def __init__(self, *args, **kwargs):
        super(MassConnectForm, self).__init__(*args, **kwargs)
        self.fields['piece'] = forms.ModelChoiceField(
            label=u'Target',
            queryset=Piece.objects.all(),
        )


class CreateAndConnectForm(forms.ModelForm):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)

    def __init__(self, *args, **kwargs):
        super(CreateAndConnectForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Piece
        fields = (
            'title', 'composer', 'performer', 'instrument',
            'period', 'form', 'license', 'comment',
        )


class IMSLPUrlForm(forms.Form):
    url = forms.URLField()

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_method = 'get'
        self.helper.add_input(Submit('submit', _('Submit')))
        super(IMSLPUrlForm, self).__init__(*args, **kwargs)

    def clean_url(self):
        url = self.cleaned_data.get('url')
        if not url:
            raise forms.ValidationError('Enter IMSLP url')
        elif not url.startswith('http://imslp.org/wiki/'):
            raise forms.ValidationError('Enter valid IMSLP url')
        else:
            return url


class IMSLPMassUrlForm(IMSLPUrlForm):
    def clean_url(self):
        url = self.cleaned_data.get('url')
        if not url:
            raise forms.ValidationError('Enter IMSLP url')
        elif not url.startswith('http://imslp.org/'):
            raise forms.ValidationError('Enter valid IMSLP category url')
        else:
            return url


class IMSLPMetadataForm(forms.ModelForm):
    """
        Form for editing metadata for importing
    """

    title = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'input-xlarge'})
    )
    comment = forms.CharField(
        widget=TinyMCE(
            attrs={'class': 'input-xlarge', 'rows': 5,},
            mce_attrs=settings.TINYMCE_SIMPLE_CONFIG,
        ),
        required=False,
    )
    part_number = forms.IntegerField(widget=forms.HiddenInput)
    user = forms.ModelChoiceField(
        queryset=(MusUser.objects
                  .filter(is_active=True)
                  .filter(Q(is_admin=True) | Q(is_manager=True))),
        widget=forms.HiddenInput,
    )
    imported_from = forms.CharField(widget=forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        initial = kwargs.get('initial')
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_id = 'piece-edit-form'
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            Fieldset(
                'Meta data',
                'title',
                AppendedText(
                    'composer',
                    LINK_FMT.format(
                        reverse('new_object', args=('composer',)),
                        unicode(_("New Composer")),
                    )
                ),
                AppendedText(
                    'instrument',
                    LINK_FMT.format(
                        reverse('new_object', args=('instrument',)),
                        unicode(_("New Instrument"))
                    )
                ),
                AppendedText(
                    'period',
                    LINK_FMT.format(
                        reverse('new_object', args=('period',)),
                        unicode(_("New Period"))
                    )
                ),
                AppendedText(
                    'form',
                    LINK_FMT.format(
                        reverse('new_object', args=('form',)),
                        unicode(_("New Form"))
                    )
                ),
                'comment',
                'part_number',
                'user',
                'imported_from',
            ),
        )
        self.helper.add_input(Submit('submit_form', _('Done')))
        super(IMSLPMetadataForm, self).__init__(*args, **kwargs)
        if initial.get('part_number'):
            divs = []
            for i in range(initial.get('part_number')):
                self.fields['part_enabled_{}'.format(i)] = (
                    forms.BooleanField(required=False)
                )
                self.fields['part_title_{}'.format(i)] = (
                    forms.CharField()
                )
                self.fields['part_filename_{}'.format(i)] = (
                    forms.CharField()
                )
                f = self.fields['part_filename_{}'.format(i)]
                f.widget.attrs['readonly'] = True
                f.widget.attrs['class'] = 'input-xxlarge'
                self.fields['part_url_{}'.format(i)] = (
                    forms.URLField(widget=forms.HiddenInput())
                )
                divs.append(
                    Div(
                        Div(
                            Field(
                                'part_enabled_{}'.format(i),
                                template='custom_crispy/clear-field.html',
                            ),
                            Field(
                                'part_title_{}'.format(i),
                                template='custom_crispy/clear-field.html',
                            ),
                            Field(
                                'part_filename_{}'.format(i),
                                template='custom_crispy/clear-field.html',
                            ),
                            Field(
                                'part_url_{}'.format(i),
                                template='custom_crispy/clear-field.html',
                            ),
                            css_class="controls"
                        ),
                        ccs_class="control-group"
                    )
                )
            self.helper.layout = Layout(
                self.helper.layout,
                Fieldset(
                    'Parts',
                    *divs
                )
            )

    class Meta:
        model = SheetMusic
        fields = (
            'title', 'composer', 'instrument',
            'period', 'form', 'comment',
            'user', 'imported_from',
        )
