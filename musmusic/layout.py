import warnings
from django.conf import settings
from django.template import Context, Template
from django.template.loader import render_to_string
from django.utils.html import conditional_escape

from crispy_forms.utils import render_field, flatatt
from crispy_forms.exceptions import DynamicError
from crispy_forms.layout import LayoutObject, TEMPLATE_PACK, Submit

class Table(LayoutObject):

    template = "custom_crispy/layout/table.html"
    tag_name = "table"

    def __init__(self, *fields, **kwargs):
        self.fields = list(fields)

        if hasattr(self, 'css_class') and kwargs.has_key('css_class'):
            self.css_class += ' %s' % kwargs.pop('css_class')
        if not hasattr(self, 'css_class'):
            self.css_class = kwargs.pop('css_class', None)

        self.css_id = kwargs.pop('css_id', '')
        self.template = kwargs.pop('template', self.template)
        self.flat_attrs = flatatt(kwargs)

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK):
        fields = ''
        for field in self.fields:
            fields += render_field(
                field,
                form,
                form_style,
                context,
                template_pack=template_pack,
            )
        return render_to_string(
            self.template,
            Context({
                'obj': self,
                'fields': fields,
                'tag_name': self.tag_name,
            }))


class Tr(Table):
    template = "custom_crispy/layout/table.html"
    tag_name = "tr"


class Td(Table):
    template = "custom_crispy/layout/table.html"
    tag_name = "td"

class Tbody(Table):
    template = "custom_crispy/layout/table.html"
    tag_name = "tbody"


class Thead(Table):
    template = "custom_crispy/layout/table.html"
    tag_name = "thead"


class CustomSubmit(Submit):
    field_classes = 'btn'
