# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'SheetRate'
        db.delete_table(u'musmusic_sheetrate')

        # Deleting field 'SheetRating.rating_sum'
        db.delete_column(u'musmusic_sheetrating', 'rating_sum')

        # Deleting field 'SheetRating.rating_count'
        db.delete_column(u'musmusic_sheetrating', 'rating_count')

        # Adding field 'PieceRate.rate_recoding'
        db.add_column(u'musmusic_piecerate', 'rate_recoding',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'PieceRate.rate_playing'
        db.add_column(u'musmusic_piecerate', 'rate_playing',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'PieceRating.rating_recoding'
        db.add_column(u'musmusic_piecerating', 'rating_recoding',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'PieceRating.rating_playing'
        db.add_column(u'musmusic_piecerating', 'rating_playing',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Adding model 'SheetRate'
        db.create_table(u'musmusic_sheetrate', (
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mususer.MusUser'])),
            ('rate', self.gf('django.db.models.fields.IntegerField')(default=0)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('piece', self.gf('django.db.models.fields.related.ForeignKey')(related_name='rates', to=orm['musmusic.SheetMusic'])),
        ))
        db.send_create_signal(u'musmusic', ['SheetRate'])

        # Adding field 'SheetRating.rating_sum'
        db.add_column(u'musmusic_sheetrating', 'rating_sum',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'SheetRating.rating_count'
        db.add_column(u'musmusic_sheetrating', 'rating_count',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Deleting field 'PieceRate.rate_recoding'
        db.delete_column(u'musmusic_piecerate', 'rate_recoding')

        # Deleting field 'PieceRate.rate_playing'
        db.delete_column(u'musmusic_piecerate', 'rate_playing')

        # Deleting field 'PieceRating.rating_recoding'
        db.delete_column(u'musmusic_piecerating', 'rating_recoding')

        # Deleting field 'PieceRating.rating_playing'
        db.delete_column(u'musmusic_piecerating', 'rating_playing')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'box_client.folders': {
            'Meta': {'object_name': 'Folders'},
            'box_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'owned_by': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'shared_url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'box_client.uploadedfile': {
            'Meta': {'object_name': 'UploadedFile'},
            'box_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'hash': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'owned_by': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'shared_url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'musmusic.composer': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Composer'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'box_folder': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'composer'", 'null': 'True', 'to': u"orm['box_client.Folders']"}),
            'box_folder_sheet': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sheet_composer'", 'null': 'True', 'to': u"orm['box_client.Folders']"}),
            'comment': ('tinymce.models.HTMLField', [], {}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('athumb.fields.ImageWithThumbsField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'learn_more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        u'musmusic.form': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Form'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'comment': ('tinymce.models.HTMLField', [], {}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'learn_more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        u'musmusic.genre': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Genre'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'musmusic.instrument': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Instrument'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'comment': ('tinymce.models.HTMLField', [], {}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'learn_more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        u'musmusic.license': {
            'Meta': {'ordering': "('name',)", 'object_name': 'License'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'cc_license': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'musmusic.part': {
            'Meta': {'object_name': 'Part', 'db_table': "'its_parts'"},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'box_file': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'lossypart'", 'null': 'True', 'to': u"orm['box_client.UploadedFile']"}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mususer.MusUser']", 'null': 'True', 'db_column': "'created_by'"}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'hq_box_file': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'losslesspart'", 'null': 'True', 'to': u"orm['box_client.UploadedFile']"}),
            'hq_filename': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'piece': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Piece']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'musmusic.performer': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Performer'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'comment': ('tinymce.models.HTMLField', [], {}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'learn_more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        u'musmusic.period': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Period'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'comment': ('tinymce.models.HTMLField', [], {}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        u'musmusic.piece': {
            'Meta': {'object_name': 'Piece', 'db_table': "'its_pieces'"},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'comment': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'composer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Composer']", 'null': 'True', 'blank': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Form']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instrument': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Instrument']", 'null': 'True', 'blank': 'True'}),
            'learn_more': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'license': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.License']", 'null': 'True', 'blank': 'True'}),
            'performer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Performer']", 'null': 'True', 'blank': 'True'}),
            'period': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Period']", 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'upload_date': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mususer.MusUser']", 'null': 'True', 'blank': 'True'})
        },
        u'musmusic.piecepartbookmark': {
            'Meta': {'object_name': 'PiecePartBookmark'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'part': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Part']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mususer.MusUser']"})
        },
        u'musmusic.piecerate': {
            'Meta': {'object_name': 'PieceRate'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'piece': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rates'", 'to': u"orm['musmusic.Piece']"}),
            'rate': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'rate_playing': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'rate_recoding': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mususer.MusUser']"})
        },
        u'musmusic.piecerating': {
            'Meta': {'object_name': 'PieceRating'},
            'hits': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'piece': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rating'", 'unique': 'True', 'to': u"orm['musmusic.Piece']"}),
            'rating_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'rating_playing': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'rating_recoding': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'rating_sum': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'musmusic.sheetmusic': {
            'Meta': {'object_name': 'SheetMusic', 'db_table': "'its_sheets'"},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'comment': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'composer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Composer']", 'null': 'True', 'blank': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Form']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instrument': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Instrument']", 'null': 'True', 'blank': 'True'}),
            'learn_more': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'period': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Period']", 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'upload_date': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mususer.MusUser']", 'null': 'True', 'blank': 'True'})
        },
        u'musmusic.sheetpart': {
            'Meta': {'ordering': "('sheet',)", 'object_name': 'SheetPart', 'db_table': "'its_sparts'"},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'box_file': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['box_client.UploadedFile']", 'null': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mususer.MusUser']", 'null': 'True', 'db_column': "'created_by'"}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'sheet': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.SheetMusic']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'musmusic.sheetpartbookmark': {
            'Meta': {'object_name': 'SheetPartBookmark'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'part': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.SheetPart']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mususer.MusUser']"})
        },
        u'musmusic.sheetrating': {
            'Meta': {'object_name': 'SheetRating'},
            'hits': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'piece': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rating'", 'unique': 'True', 'to': u"orm['musmusic.SheetMusic']"})
        },
        u'mususer.day': {
            'Meta': {'object_name': 'Day'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'mususer.genre': {
            'Meta': {'object_name': 'Genre', 'db_table': "'its_u_genres'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'mususer.instrument': {
            'Meta': {'object_name': 'Instrument', 'db_table': "'its_u_instruments'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'mususer.mususer': {
            'Meta': {'object_name': 'MusUser'},
            'about_me': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'available_days': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['mususer.Day']", 'null': 'True', 'symmetrical': 'False'}),
            'available_from': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'available_to': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(17, 0)'}),
            'awards': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'bio': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'confirm_code': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'contactme': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cost_per_lesson': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'cost_per_recording': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'distance': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'education': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'genre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mususer.Genre']", 'null': 'True', 'blank': 'True'}),
            'gift_expire': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Group']", 'null': 'True'}),
            'homepage': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instrument': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mususer.Instrument']", 'null': 'True', 'blank': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_invited': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_login_time': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'links': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'newsletter': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'nickname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'past_repertoire': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'payment_methods': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['mususer.PaymentMethod']", 'null': 'True', 'symmetrical': 'False'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'picture': ('athumb.fields.ImageWithThumbsField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'premium_expire': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'rates': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'ref_link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'register_date': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'resume': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'show_profile_completion': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'stripe_customer': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'teach_levels': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['mususer.TeachLevel']", 'null': 'True', 'symmetrical': 'False'}),
            'teach_types': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['mususer.TeachType']", 'null': 'True', 'symmetrical': 'False'}),
            'teacher_info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mususer.TeacherInfo']", 'null': 'True', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'})
        },
        u'mususer.paymentmethod': {
            'Meta': {'object_name': 'PaymentMethod'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'mususer.teacherinfo': {
            'Meta': {'object_name': 'TeacherInfo'},
            'available_from': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)', 'null': 'True'}),
            'available_to': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(17, 0)', 'null': 'True'}),
            'cost_per_lesson': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'cost_per_recording': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'distance': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'past_repertoire': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'resume': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'mususer.teachlevel': {
            'Meta': {'object_name': 'TeachLevel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'mususer.teachtype': {
            'Meta': {'object_name': 'TeachType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        }
    }

    complete_apps = ['musmusic']