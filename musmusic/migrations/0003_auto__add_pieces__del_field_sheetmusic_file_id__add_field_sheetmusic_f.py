# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Pieces'
        db.create_table(u'musmusic_pieces', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.TextField')()),
            ('composer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['musmusic.Composer'])),
            ('performer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['musmusic.Performer'])),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['musmusic.Form'])),
            ('instrument', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['musmusic.Instrument'])),
            ('license', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['musmusic.License'])),
            ('period', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['musmusic.Period'])),
            ('approved', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('upload_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('learn_more', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('text', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'musmusic', ['Pieces'])

        # Deleting field 'SheetMusic.file_id'
        db.delete_column(u'musmusic_sheetmusic', 'file_id')

        # Adding field 'SheetMusic.file'
        db.add_column(u'musmusic_sheetmusic', 'file',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['musmusic.Pieces'], null=True, blank=True),
                      keep_default=False)

        # Adding field 'SheetMusic.learn_more'
        db.add_column(u'musmusic_sheetmusic', 'learn_more',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'SheetMusic.approved'
        db.add_column(u'musmusic_sheetmusic', 'approved',
                      self.gf('django.db.models.fields.SmallIntegerField')(default=1),
                      keep_default=False)

        # Adding field 'SheetMusic.upload_date'
        db.add_column(u'musmusic_sheetmusic', 'upload_date',
                      self.gf('django.db.models.fields.DateField')(null=True, blank=True),
                      keep_default=False)


        # Changing field 'TokenBoxCom.update'
        db.alter_column(u'musmusic_tokenboxcom', 'update', self.gf('django.db.models.fields.DateTimeField')())
        # Deleting field 'Composer.more'
        db.delete_column(u'musmusic_composer', 'more')

        # Adding field 'Composer.learn_more'
        db.add_column(u'musmusic_composer', 'learn_more',
                      self.gf('django.db.models.fields.URLField')(default='', max_length=200),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'Pieces'
        db.delete_table(u'musmusic_pieces')


        # User chose to not deal with backwards NULL issues for 'SheetMusic.file_id'
        raise RuntimeError("Cannot reverse this migration. 'SheetMusic.file_id' and its values cannot be restored.")
        # Deleting field 'SheetMusic.file'
        db.delete_column(u'musmusic_sheetmusic', 'file_id')

        # Deleting field 'SheetMusic.learn_more'
        db.delete_column(u'musmusic_sheetmusic', 'learn_more')

        # Deleting field 'SheetMusic.approved'
        db.delete_column(u'musmusic_sheetmusic', 'approved')

        # Deleting field 'SheetMusic.upload_date'
        db.delete_column(u'musmusic_sheetmusic', 'upload_date')


        # Changing field 'TokenBoxCom.update'
        db.alter_column(u'musmusic_tokenboxcom', 'update', self.gf('django.db.models.fields.DateTimeField')(auto_now=True))
        # Adding field 'Composer.more'
        db.add_column(u'musmusic_composer', 'more',
                      self.gf('django.db.models.fields.URLField')(default='', max_length=200),
                      keep_default=False)

        # Deleting field 'Composer.learn_more'
        db.delete_column(u'musmusic_composer', 'learn_more')


    models = {
        u'musmusic.composer': {
            'Meta': {'object_name': 'Composer'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'bio': ('django.db.models.fields.TextField', [], {}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'learn_more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'musmusic.form': {
            'Meta': {'object_name': 'Form'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'musmusic.genre': {
            'Meta': {'object_name': 'Genre'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'musmusic.instrument': {
            'Meta': {'object_name': 'Instrument'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'musmusic.license': {
            'Meta': {'object_name': 'License'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cc_license': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "u'Public Domain'", 'max_length': '128'})
        },
        u'musmusic.performer': {
            'Meta': {'object_name': 'Performer'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'musmusic.period': {
            'Meta': {'object_name': 'Period'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'musmusic.pieces': {
            'Meta': {'object_name': 'Pieces'},
            'approved': ('django.db.models.fields.SmallIntegerField', [], {}),
            'composer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Composer']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Form']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instrument': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Instrument']"}),
            'learn_more': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'license': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.License']"}),
            'performer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Performer']"}),
            'period': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Period']"}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.TextField', [], {}),
            'upload_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        u'musmusic.sheetmusic': {
            'Meta': {'object_name': 'SheetMusic'},
            'approved': ('django.db.models.fields.SmallIntegerField', [], {}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            'composer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Composer']"}),
            'file': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Pieces']", 'null': 'True', 'blank': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Form']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instrument': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Instrument']"}),
            'learn_more': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'period': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Period']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'upload_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        u'musmusic.tokenboxcom': {
            'Meta': {'object_name': 'TokenBoxCom'},
            'access_token': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'refresh_token': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'ttl': ('django.db.models.fields.IntegerField', [], {'default': '3600'}),
            'update': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['musmusic']