# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TokenBoxCom'
        db.create_table(u'musmusic_tokenboxcom', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('access_token', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('refresh_token', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('update', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'musmusic', ['TokenBoxCom'])

        # Adding model 'Composer'
        db.create_table(u'musmusic_composer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('approved', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('bio', self.gf('django.db.models.fields.TextField')()),
            ('more', self.gf('django.db.models.fields.URLField')(max_length=200)),
        ))
        db.send_create_signal(u'musmusic', ['Composer'])

        # Adding model 'Form'
        db.create_table(u'musmusic_form', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('approved', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('more', self.gf('django.db.models.fields.URLField')(max_length=200)),
        ))
        db.send_create_signal(u'musmusic', ['Form'])

        # Adding model 'Instrument'
        db.create_table(u'musmusic_instrument', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('approved', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('more', self.gf('django.db.models.fields.URLField')(max_length=200)),
        ))
        db.send_create_signal(u'musmusic', ['Instrument'])

        # Adding model 'Performer'
        db.create_table(u'musmusic_performer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('approved', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('more', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('comment', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'musmusic', ['Performer'])

        # Adding model 'Period'
        db.create_table(u'musmusic_period', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('approved', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('more', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('comment', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'musmusic', ['Period'])

        # Adding model 'License'
        db.create_table(u'musmusic_license', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('approved', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('name', self.gf('django.db.models.fields.CharField')(default=u'Public Domain', max_length=128)),
            ('cc_license', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'musmusic', ['License'])

        # Adding model 'Genre'
        db.create_table(u'musmusic_genre', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('approved', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'musmusic', ['Genre'])

        # Adding model 'SheetMusic'
        db.create_table(u'musmusic_sheetmusic', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('composer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['musmusic.Composer'])),
            ('form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['musmusic.Form'])),
            ('period', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['musmusic.Period'])),
            ('instrument', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['musmusic.Instrument'])),
            ('file_id', self.gf('django.db.models.fields.TextField')()),
            ('comment', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'musmusic', ['SheetMusic'])


    def backwards(self, orm):
        # Deleting model 'TokenBoxCom'
        db.delete_table(u'musmusic_tokenboxcom')

        # Deleting model 'Composer'
        db.delete_table(u'musmusic_composer')

        # Deleting model 'Form'
        db.delete_table(u'musmusic_form')

        # Deleting model 'Instrument'
        db.delete_table(u'musmusic_instrument')

        # Deleting model 'Performer'
        db.delete_table(u'musmusic_performer')

        # Deleting model 'Period'
        db.delete_table(u'musmusic_period')

        # Deleting model 'License'
        db.delete_table(u'musmusic_license')

        # Deleting model 'Genre'
        db.delete_table(u'musmusic_genre')

        # Deleting model 'SheetMusic'
        db.delete_table(u'musmusic_sheetmusic')


    models = {
        u'musmusic.composer': {
            'Meta': {'object_name': 'Composer'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'bio': ('django.db.models.fields.TextField', [], {}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'musmusic.form': {
            'Meta': {'object_name': 'Form'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'musmusic.genre': {
            'Meta': {'object_name': 'Genre'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'musmusic.instrument': {
            'Meta': {'object_name': 'Instrument'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'musmusic.license': {
            'Meta': {'object_name': 'License'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cc_license': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "u'Public Domain'", 'max_length': '128'})
        },
        u'musmusic.performer': {
            'Meta': {'object_name': 'Performer'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'musmusic.period': {
            'Meta': {'object_name': 'Period'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'musmusic.sheetmusic': {
            'Meta': {'object_name': 'SheetMusic'},
            'comment': ('django.db.models.fields.TextField', [], {}),
            'composer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Composer']"}),
            'file_id': ('django.db.models.fields.TextField', [], {}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Form']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instrument': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Instrument']"}),
            'period': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Period']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'musmusic.tokenboxcom': {
            'Meta': {'object_name': 'TokenBoxCom'},
            'access_token': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'refresh_token': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'update': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['musmusic']
