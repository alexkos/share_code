# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Form.comment'
        db.add_column(u'musmusic_form', 'comment',
                      self.gf('django.db.models.fields.TextField')(default=''),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Form.comment'
        db.delete_column(u'musmusic_form', 'comment')


    models = {
        u'musmusic.composer': {
            'Meta': {'object_name': 'Composer'},
            'approved': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'learn_more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'musmusic.form': {
            'Meta': {'object_name': 'Form'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'musmusic.genre': {
            'Meta': {'object_name': 'Genre'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'musmusic.instrument': {
            'Meta': {'object_name': 'Instrument'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'learn_more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'musmusic.license': {
            'Meta': {'object_name': 'License'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cc_license': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "u'Public Domain'", 'max_length': '128'})
        },
        u'musmusic.performer': {
            'Meta': {'object_name': 'Performer'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'musmusic.period': {
            'Meta': {'object_name': 'Period'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'more': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'musmusic.pieces': {
            'Meta': {'object_name': 'Pieces'},
            'approved': ('django.db.models.fields.SmallIntegerField', [], {}),
            'composer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Composer']"}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Form']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instrument': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Instrument']"}),
            'learn_more': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'license': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.License']"}),
            'performer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Performer']"}),
            'period': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Period']"}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.TextField', [], {}),
            'upload_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        u'musmusic.sheetmusic': {
            'Meta': {'object_name': 'SheetMusic'},
            'approved': ('django.db.models.fields.SmallIntegerField', [], {}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            'composer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Composer']"}),
            'file': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Pieces']", 'null': 'True', 'blank': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Form']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instrument': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Instrument']"}),
            'learn_more': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'period': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['musmusic.Period']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'upload_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        u'musmusic.tokenboxcom': {
            'Meta': {'object_name': 'TokenBoxCom'},
            'access_token': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'refresh_token': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'ttl': ('django.db.models.fields.IntegerField', [], {'default': '3600'}),
            'update': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['musmusic']