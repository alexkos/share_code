from django import template

register = template.Library()

@register.filter
def control_label(value):
    if 'class="' in value:
        return value.replace('class="', 'class="control-label ')
    else:
        return value.replace('<label', '<label class="control-label"')


@register.filter
def checkbox_label(value):
    if 'class="' in value:
        return (
            value
            .replace('class="', 'class="checkbox ')
            .replace('</label>', '')
        )
    else:
        return (
            value
            .replace('<label', '<label class="checkbox"')
            .replace('</label>', '')
        )


@register.filter
def unquery_string(string):
    try:
        return string.rsplit('=', 1)[1]
    except IndexError:
        return ''
