from random import randint, seed
from django import template
from musmusic.models import Composer

register = template.Library()
seed()


@register.assignment_tag
def get_featured_composers():
    qs = (
        Composer.objects
        .filter(featured=True)
        .filter(approved=True)
        .order_by('id')
        .defer(
            'comment', 'learn_more', 'created_date',
            'box_folder', 'box_folder_sheet'
        )
    )
    count = qs.count()
    nums = []
    while len(nums) < 4:
        num = randint(0, count-1)
        if num not in nums:
            nums.append(num)
    result = [
        qs[nums[0]],
        qs[nums[1]],
        qs[nums[2]],
        qs[nums[3]],
    ]
    return result
