from django import template

register = template.Library()


@register.simple_tag
def filter_url(filter_obj, musictype):
    return filter_obj.get_filter_url(musictype)


@register.simple_tag(takes_context=True)
def add_query(context, key, value):
    result = context.get('request').GET.copy()
    if result.getlist(key):
        lst = result.getlist(key)
        if str(value) not in lst:
            lst.append(str(value))
        result.setlist(key, lst)
    else:
        result.setlist(key, [value])
    if 'page' in result:
        del result['page']
    return result.urlencode()


@register.simple_tag(takes_context=True)
def rm_query(context, key, value):
    result = context.get('request').GET.copy()
    if result.getlist(key):
        lst = result.getlist(key)
        if str(value) in lst:
            lst.remove(str(value))
        result.setlist(key, lst)
    if 'page' in result:
        del result['page']
    return result.urlencode()


@register.simple_tag(takes_context=True)
def order_query(context, field_name):
    result = context.get('request').GET.copy()
    if result.get('order_by') == field_name:
        result.setlist('order_by', ['-' + field_name])
    else:
        result.setlist('order_by', [field_name])
    if 'page' in result:
        del result['page']
    return result.urlencode()


@register.assignment_tag
def rm_s(string):
    if string.endswith('s'):
        return string[:-1]


@register.assignment_tag(takes_context=True)
def in_query(context, key, value):
    return str(value) in context.get('request').GET.getlist(key)
