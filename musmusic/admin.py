#-*- coding: utf-8 -*-

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Count
import reversion

from musmusic.actions import (
    delete_and_ban, merge_metadata, mass_edit, unapprove,
)
from musmusic.models import (
    Composer, Form, Instrument, Performer, Period,
    License, Genre, SheetMusic, SheetPart, Piece, Part,
)
from musmusic.forms import PieceForm

class SheetPartFilter(admin.SimpleListFilter):
    title = _('Parts')
    parameter_name = 'parts'

    def lookups(self, request, model_admin):
        return (('absent', 'Absent'), ('not_absent', 'Not Absent'))

    def queryset(self, request, queryset):
        if self.value() == 'absent':
            return queryset.filter(
                sheetpart__isnull=True,
            )
        elif self.value() == 'not_absent':
            return queryset.filter(
                sheetpart__isnull=False,
            )


class SheetFilesFilter(admin.SimpleListFilter):
    title = _('Files')
    parameter_name = 'files'

    def lookups(self, request, model_admin):
        return (('absent', 'Absent'), ('not_absent', 'Not Absent'))

    def queryset(self, request, queryset):
        queryset = queryset.annotate(num_files=Count('sheetpart__box_file'))
        if self.value() == 'absent':
            return queryset.filter(num_files__lte=0)
        elif self.value() == 'not_absent':
            return queryset.filter(num_files__gt=0)

class SheetPartInline(admin.StackedInline):
    model = SheetPart
    # FIXME: SELECTs for user/created_by and box_file fields are very slow
    # Need some hack to improve it. Field are made read only nor now.
    list_display = (
        'number', 'title', 'approved',
        'filename', 'box_file', 'created_by'
    )
    readonly_fields = (
        'filename', 'created_by', 'box_file',
    )
    extra = 1

class SheetMusicAdmin(reversion.VersionAdmin):
    list_filter = (
        'approved',
        SheetPartFilter,
        SheetFilesFilter,
    )
    list_display = (
        'title', 'composer', 'period', 'instrument',  'form'
    )
    search_fields = ['title', ]
    #filter_vertical = (
        #'title',
    #)
    readonly_fields = ('user',)
    history_latest_first = True
    template_path = "reversion/musmusic/sheetmusic/"
    object_history_template = template_path + "object_history.html"
    revision_form_template = template_path + "revision_form.html"
    actions = (delete_and_ban, mass_edit, unapprove)
    inlines = (SheetPartInline, )

    class Media:
        js = [
            '/static/tiny_mce/tiny_mce.js',
        ]


class PartInline(admin.StackedInline):
    model = Part
    # FIXME: SELECTs for user/created_by and box_file fields are very slow
    # Need some hack to improve it. Field are made read only nor now.
    list_display = (
        'number', 'title', 'approved',
        'filename', 'box_file', 'hq_filename', 'hq_box_file',
        'created_by'
    )
    readonly_fields = (
        'filename', 'hq_filename', 'created_by',
        'box_file', 'hq_box_file',
    )
    extra = 1


class PartFilter(admin.SimpleListFilter):
    title = _('Parts')
    parameter_name = 'parts'

    def lookups(self, request, model_admin):
        return (('absent', 'Absent'), ('not_absent', 'Not Absent'))

    def queryset(self, request, queryset):
        if self.value() == 'absent':
            return queryset.filter(
                part__isnull=True,
            )
        elif self.value() == 'not_absent':
            return queryset.filter(
                part__isnull=False,
            )


class FilesFilter(admin.SimpleListFilter):
    title = _('Files')
    parameter_name = 'files'

    def lookups(self, request, model_admin):
        return (('absent', 'Absent'), ('not_absent', 'Not Absent'))

    def queryset(self, request, queryset):
        queryset = queryset.annotate(num_files=Count('part__box_file'))
        if self.value() == 'absent':
            return queryset.filter(num_files__lte=0)
        elif self.value() == 'not_absent':
            return queryset.filter(num_files__gt=0)


class IsComposerFilter(admin.SimpleListFilter):
    title = _('Composer')
    parameter_name = 'composer'

    def lookups(self, request, model_admin):
        return (('absent', 'Absent'), ('not_absent', 'Not Absent'))

    def queryset(self, request, queryset):
        if self.value() == 'absent':
            return queryset.filter(
                composer__isnull=True,
            )
        elif self.value() == 'not_absent':
            return queryset.filter(
                composer__isnull=False,
            )


class ComposerFilter(admin.SimpleListFilter):
    title = _('Composer')
    template = "musmusic/admin-by-composer-filter.html"
    parameter_name = 'composer_id_exact'

    def lookups(self, request, model_admin):
        return [(composer.id, composer.name) for composer in Composer.objects.all()]

    def queryset(self, request, queryset):
        return queryset

class PieceAdmin(reversion.VersionAdmin):
    form = PieceForm
    search_fields = [
        'composer__name',
        'composer__first_name',
        'composer__last_name',
        'user__email',
    ]

    def get_performer(self, obj):
        if obj.performer:
            return obj.performer.name
        else:
            return ''

    get_performer.short_description = "Performer"

    def get_user(self, obj):
        if obj.user:
            return u'<a href="/admin/mususer/mususer/{}/">{}</a>'.format(
                obj.user.id,
                unicode(obj.user.username),
            )
        else:
            return ''

    get_user.short_description = "User"
    get_user.allow_tags = True
    list_display = (
        'title',
        'composer',
        'get_user',
        'get_performer',
    )
    list_filter = (
        ComposerFilter,
        'approved',
        FilesFilter,
        PartFilter,
        IsComposerFilter,
    )
    actions = (delete_and_ban, mass_edit, unapprove)
    inlines = (PartInline,)
    readonly_fields = ('user',)
    history_latest_first = True
    object_history_template = "reversion/musmusic/piece/object_history.html"
    revision_form_template = "reversion/musmusic/piece/revision_form.html"

    class Media:
        js = [
            '/static/tiny_mce/tiny_mce.js',
        ]


class ComposerAdmin(reversion.VersionAdmin):
    exclude = ('slug', 'box_folder', 'box_folder_sheet')
    search_fields = ('name', 'first_name', 'last_name', )
    actions = (merge_metadata, )

    class Media:
        js = [
            '/static/tiny_mce/tiny_mce.js',
        ]


class FormAdmin(reversion.VersionAdmin):
    actions = (merge_metadata, )

    class Media:
        js = [
            '/static/tiny_mce/tiny_mce.js',
        ]


class InstrumentAdmin(reversion.VersionAdmin):
    actions = (merge_metadata, )

    class Media:
        js = [
            '/static/tiny_mce/tiny_mce.js',
        ]


class PerformerAdmin(reversion.VersionAdmin):
    actions = (merge_metadata, )

    class Media:
        js = [
            '/static/tiny_mce/tiny_mce.js',
        ]


class PeriodAdmin(reversion.VersionAdmin):
    actions = (merge_metadata, )

    class Media:
        js = [
            '/static/tiny_mce/tiny_mce.js',
        ]


class LicenseAdmin(reversion.VersionAdmin):
    actions = (merge_metadata, )

    class Media:
        js = [
            '/static/tiny_mce/tiny_mce.js',
        ]


admin.site.register(Composer, ComposerAdmin)
admin.site.register(Form, FormAdmin)
admin.site.register(Instrument, InstrumentAdmin)
admin.site.register(Performer, PerformerAdmin)
admin.site.register(Period, PeriodAdmin)
admin.site.register(License, LicenseAdmin)
admin.site.register(Genre, reversion.VersionAdmin)
admin.site.register(Piece, PieceAdmin)
admin.site.register(SheetMusic, SheetMusicAdmin)
