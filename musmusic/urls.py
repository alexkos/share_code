#-*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url

from django.conf import settings
from django.views.generic import TemplateView

from musmusic.views import MusicSearchView

urlpatterns = patterns(
    'musmusic.views',

    # Special for composers
    url(
        (
            r'^(?P<mtype>music|sheetmusic)/'
            r'by/composer/(?P<name>[^/]+)/$'
        ),
        'smart_composer_redirect', name='music_by_composer_legacy',
    ),
    # old urls:
    url(
        (
            r'^(?P<mtype>music|sheetmusic)/'
            r'by/(?P<filter_by>\w+)/(?P<name>[^/]+)/$'
        ),
        'show_items_redirect', name='music_by_legacy',
    ),
    url(
        r'^(?P<musictype>music|sheetmusic)/(?P<object_id>\d+)/$',
        'show_piece_redirect', name='music_view_notsoold',
    ),
    url(
        r'^(?P<musictype>music|sheetmusic)/(?P<object_id>\d+)(?:-[\w_-]*)?/$',
        'show_piece_redirect', name='music_view_notold',
    ),
    url(
        (
            r'^(?P<musictype>music|sheetmusic)/'
            r'(?:sheet|piece)/(?P<object_id>\d+)/$'
        ),
        'show_piece_redirect', name='music_view_old',
    ),
    url(
        r'download/(?P<musictype>music)/id/(?P<object_id>\d+)',
        'show_piece_redirect',
        name='download_piece_old',
    ),
    url(
        r'download/sheet/id/(?P<object_id>\d+)',
        'show_piece_redirect',
        {'musictype': 'sheetmusic'},
        name='download_sheet_old',
    ),
    url(r'^get_piece/', 'get_piece_by_slug', name='musmusic_by_slug'),
    url(r'^list_pieces/', 'list_slugs', name='musmusic_slugs'),


    url(
        r'^info/(?P<object_type>\w+)/(?P<name>[^/]+)/',
        'show_info', name="musmusic_info_page"
    ),
    # Would be useful, but we need to many reverses to use this
    #url(
    #    (
    #        r'^(?P<mtype>^sheetmusic|^music)/'
    #        r'(?P<filter_by>[^/]+)/(?P<name>[^/]+)/?$'
    #    ),
    #    'show_items', name='show_items'
    #),

    # Edit/add urls
    url(
        r'^(?P<musictype>music|sheetmusic)/edit/(?P<object_id>\d+)/$',
        'edit_piece',
        name='music_edit',
    ),
    url(
        r'^(?P<musictype>music|sheetmusic)/edit/$',
        'edit_piece',
        name='music_add',
    ),
    url(
        r'^(?P<musictype>music|sheetmusic)/delete/(?P<object_id>\d+)/$',
        'delete_piece',
        name='music_delete',
    ),
    # Search
    url(
        r'^(?P<musictype>music|sheetmusic)/search/$',
        MusicSearchView(), name='music_search',
    ),
    # View piece/sheet
    url(
        (
            r'^(?P<musictype>music|sheetmusic)/'
            r'(?P<object_id>\d+)'
            r'(?:/[\w_-]+)?(?:/[\w_-]+)?/$'
        ),
        'show_piece',
        name='music_view',
    ),

    # Sheetmusic URLs
    url(
        r'^sheetmusic/$',
        'show_catalog', {'mtype': 'sheetmusic'},
        name='sheetmusic_catalog',
    ),

    url(
        r'^embed/sheet/(?P<object_id>\d+)/$',
        'embed_sheet',
        name='sheetmusic_embed',
    ),
    url(
        r'^embed/sheet/(?P<object_id>\d+)/popup/$',
        'embed_sheet_popup',
        name='sheetmusic_embed_popup',
    ),
    url(
        r'^sheetmusic/(?P<filter_by>\w+)/(?P<name>[^/]+)/$',
        'show_items', {'mtype': 'sheetmusic'},
        name='sheetmusic',
    ),

    # Music URLs
    url(
        r'^music/download/(?P<part_id>\d+)/$',
        'download_part',
        name='musmusic_download_part',
    ),
    url(
        r'^music/$',
        'show_catalog', {'mtype': 'music'},
        name='music_catalog',
    ),

    url(r'^shuffle/$', 'shuffle', name='music_shuffle'),
    url(r'^radio/$', 'radio', name='music_radio'),
    url(r'^radio/next/$', 'next_radio_track', name='music_radio_next'),

    url(
        r'^music/(?P<filter_by>\w+)/(?P<name>[^/]+)/$',
        'show_items', {'mtype': 'music'},
        name='music',
    ),
    url(r'^ajax/new/(?P<object_type>\w+)/$', 'edit_object', name='new_object'),
    url(
        r'^ajax/edit/(?P<object_type>\w+)/(?P<object_id>\d+)/$',
        'edit_object',
        name='edit_object',
    ),
    url(
        r'^ajax/(?P<musictype>music|sheetmusic)/add-bookmark/(?P<part_id>\d+)/',
        'add_bookmark',
        name='musmusic_add_bookmark',
    ),
    url(
        (
            r'^ajax/(?P<musictype>music|sheetmusic)/'
            r'remove-bookmark/(?P<part_id>\d+)/'
        ),
        'remove_bookmark',
        name='musmusic_remove_bookmark',
    ),
    url(
        (
            r'^ajax/(?P<musictype>music)/'
            r'rate-piece/(?P<rate_type>recording|playing)/'
            r'(?P<piece_id>\d+)/(?P<rate_score>\d)/'
        ),
        'rate_piece',
        name='musmusic_rate_piece',
    ),
    url(
        (
            r'^ajax/(?P<contenttype>music|sheet)/'
            r'set-file/(?P<part_id>\d+)/(?P<fieldname>box_file|hq_box_file)/'
            r'(?P<scope>all|orphans)/'
        ),
        'set_file_to_part',
        name='musmusic_set_file_to_part',
    ),
    url(
        r'^ajax/disconnect-file/(?P<part_id>\d+)/(?P<filetype>lq|hq|sheet)/',
        'disconnect_file',
        name='musmusic_disconnect_file',
    ),
    url(
        r'^ajax/connect-file/(?P<task>connect|create)/(?P<file_id>\d+)/',
        'connect_file',
        name='musmusic_connect_file',
    ),
    url(
        r'^(?P<musictype>sheetmusic)/import_imslp/',
        'import_imslp',
        name='musmusic_import_imslp',
    ),
    url(
        r'^(?P<musictype>sheetmusic)/mass_import_imslp/',
        'mass_import_imslp',
        name='musmusic_mass_import_imslp',
    ),
    url(
        r'^imslp_download_status/(?P<random_id>[^/]+)/',
        'async_download_status',
        name='musmusic_import_status',
    ),
    url(
        r'^imslp_import_session_status/(?P<random_id>[^/]+)/',
        'session_status',
        name='musmusic_import_session_status',
    ),
    url(
        r'^imslp_import_threads_count/',
        'threads_count',
        name='musmusic_import_threads_count',
    ),
    url(
        r'finished_encoding/(?P<part_id>\d+)/(?P<box_id>\d+)/(?P<auth_code>[^/]+)/',
        'finished_encoding',
        name='mp3_finished_encoding',
    ),
    # Need to fix reverses
    #url(
    #    r'^(?P<mtype>^sheetmusic|^music)/?',
    #    'show_catalog', name='show_catalog',
    #),
)
