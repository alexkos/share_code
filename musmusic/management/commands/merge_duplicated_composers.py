from django.conf import settings
from musmusic.models import Composer, Genre
from django.core.management.base import BaseCommand, CommandError
import reversion


class Command(BaseCommand):

    def handle(self, *args, **options):

        for com in Composer.objects.all():
            if Composer.objects.filter(slug=com.slug).count() > 1:
                print (
                    com.id, com.slug,
                    com.name, com.first_name, com.last_name,
                    com.piece_set.all().count(),
                )
                main = Composer.objects.filter(slug=com.slug).latest('id')
                for dup in (
                    Composer.objects
                    .filter(slug=com.slug)
                    .exclude(id=main.id)
                ):
                    for piece in dup.sheetmusic_set.all():
                        with reversion.create_revision():
                            piece.composer = main
                            piece.save()
                            reversion.set_comment("Merging duplicates")
                    for piece in dup.piece_set.all():
                        piece.composer = main
                        with reversion.create_revision():
                            piece.composer = main
                            piece.save()
                            reversion.set_comment("Merging duplicates")
                    dup.delete()
        print '==='
        for com in Composer.objects.all():
            if Composer.objects.filter(name=com.name).count() > 1:
                print (
                    com.id, com.slug,
                    com.name, com.first_name, com.last_name,
                    com.piece_set.all().count(),
                )
                main = Composer.objects.filter(name=com.name).latest('id')
                for dup in (
                    Composer.objects
                    .filter(name=com.name).exclude(id=main.id)
                ):
                    for piece in dup.sheetmusic_set.all():
                        piece.composer = main
                        with reversion.create_revision():
                            piece.composer = main
                            piece.save()
                            reversion.set_comment("Merging duplicates")
                    for piece in dup.piece_set.all():
                        piece.composer = main
                        with reversion.create_revision():
                            piece.composer = main
                            piece.save()
                            reversion.set_comment("Merging duplicates")
                    dup.delete()
        print '==='
        for gen in Genre.objects.all():
            if Genre.objects.filter(name=gen.name).count() > 1:
                print gen.id, gen.name
                main = Genre.objects.filter(name=gen.name).latest('id')
                for dup in (
                    Genre.objects
                    .filter(name=gen.name)
                    .exclude(id=main.id)
                ):

                    dup.delete()
