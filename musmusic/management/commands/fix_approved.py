from django.conf import settings
from musmusic.models import *
from django.core.management.base import BaseCommand, CommandError
import reversion


class Command(BaseCommand):

    def handle(self, *args, **options):

        for cls in Composer, Form, Instrument, Performer, Period:
            for obj in cls.objects.all():
                if obj.piece_set.filter(approved=True).exists():
                    obj.approved = True
                    obj.save()
                else:
                    obj.approved = False
                    obj.save()
                if cls != Performer:
                    if obj.sheetmusic_set.filter(approved=True).exists():
                        obj.sheet_approved = True
                        obj.save()
                    else:
                        obj.sheet_approved = False
                        obj.save()
