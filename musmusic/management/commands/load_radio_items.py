from tempfile import NamedTemporaryFile
from urllib2 import urlopen, HTTPError
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.utils.timezone import now, get_default_timezone
from django.core.files import File
from iso8601 import parse_date
from box_client.models import UploadedFile, Folders
from box_client.client import (
    BoxClient, BoxSession, BoxFile, BoxFolder, AccessError,
)
from musmusic.models import Composer, Performer, RadioItem
import id3
import logging
logging.basicConfig(level=logging.INFO)

class Command(BaseCommand):

    def check_token(self):
        logging.debug('Checking access token, update time:{}, now:{}'.format(
            self.s.token.update,
            now()),
        )
        if self.s.token.update <= now():
            logging.info('Updating token...')
            try:
                self.s = BoxSession(token_name="download")
                self.b = BoxClient(token_name="download")
            except ValueError as e:
                logging.error('Got ValueError: %s', e)
                sleep(60)
                self.check_token()


    def dirwalk(self, root):
        """
            Walk a directory tree, using a generator
        """
        for entry in root.entries:
            limit = 100
            offset = 0
            count = limit
            total_count = 0
            if isinstance(entry, BoxFile):
                yield entry
            elif isinstance(entry, BoxFolder):
                while count >= limit:
                    self.check_token()
                    try:
                        items = self.b.get_items(
                            entry.id, limit=limit, offset=offset,
                        )
                        total_count = items.total_count
                    except ValueError as e:
                        logging.error('Got ValueError: %s', e)
                        sleep(60)
                        self.check_token()
                        items = self.b.get_items(
                            entry.id, limit=limit, offset=offset,
                        )
                        total_count = items.total_count
                    for subentry in self.dirwalk(items):
                        yield subentry
                    count = len(items.entries)
                    offset += count
                logging.debug(
                    "End of loop, count: %d, total_count: %d",
                    offset,
                    total_count,
                )
                if offset != total_count:
                    logging.error(
                        "Counter mismatch here, count: %d, total_count: %d",
                        offset,
                        total_count,
                    )

    def handle(self, *args, **options):
        self.s = BoxSession(token_name="download")
        self.b = BoxClient(token_name="download")
        folder = Folders.objects.get(description='benefactors-radio')
        for f in self.dirwalk(self.b.get_items(folder_id=folder.box_id)):
            logging.debug(u'{}, {}'.format(f.id, f.name))
            if not f.shared_link:
                self.check_token()
                try:
                    f = self.b.share_file(f.id)
                except ValueError as e:
                    logging.warn('Got ValueError: %s', e)
                    sleep(60)
                    self.check_token()
                    f = self.b.share_file(f.id)

            data = {}
            data['box_id'] = f.id
            data['name'] = f.name
            if not data['name'].endswith('.mp3'):
                continue
            if f.path_collection:
                data['description'] = ''.join(
                    [
                        '/' + entry.get('name', '')
                        for entry in f.path_collection.get('entries')
                    ]
                ) + '/' + f.name
            else:
                data['description'] = f.name
            data['created_at'] = parse_date(f.created_at)
            data['modified_at'] = parse_date(f.modified_at)
            data['owned_by'] = f.owned_by
            data['hash'] = f.sha1
            if f.shared_link:
                data['shared_url'] = f.shared_link.get('download_url')
            logging.info('Processing %s', data['description'])
            try:
                file_obj = UploadedFile.objects.get(box_id=data['box_id'])
            except UploadedFile.DoesNotExist:
                print 'does not exist yet'
                file_obj = UploadedFile(**data)
                file_obj.save()
            else:
                print 'exists'
                if (
                    file_obj.name == data['name']
                    and file_obj.description == data['description']
                    and file_obj.created_at == data['created_at']
                    and file_obj.modified_at == data['modified_at']
                    and file_obj.hash == data['hash']
                    and file_obj.shared_url == data['shared_url']
                    and file_obj.created_at
                    and file_obj.modified_at
                    and file_obj.hash
                    and file_obj.radioitem_set.exists()
                ):
                    # Seems like nothing changed
                    print 'not changed'
                    continue
                else:
                    # Update data
                    print 'changed, updating'
                    file_obj.shared_url = data['shared_url']
                    file_obj.name = data['name']
                    file_obj.description = data['description']
                    file_obj.created_at = data['created_at']
                    file_obj.modified_at = data['modified_at']
                    file_obj.hash = data['hash']
                    # saving will be at the end of iteration
                    # file_obj.save()

            try:
                url = urlopen(file_obj.shared_url)
            except HTTPError:
                try:
                    sleep(30)
                    url = urlopen(f.shared_url)
                except HTTPError:
                    continue
            temp = File(NamedTemporaryFile(
                dir=settings.TEMP_DOWNLOAD_DIR,
                bufsize=0,
            ))
            eof = False
            while not eof:
                buf = url.read(1024)
                temp.write(buf)
                temp.flush()
                if len(buf) < 1024:
                    eof = True
            #temp.write(url.read())
            temp.seek(0)
            temp.flush()
            # TODO: download, extract metadata, and save item

            id3v2 = id3.ID3v2(temp.name)
            print 'parsing', data['description']

            print '===================='
            tags = dict([(frame.id, unicode(frame))
                    for frame in id3v2.frames
                    if frame.id in ('TCOM', 'TIT2', 'TPE1')])
            print tags
            print '===================='
            if 'TIT2' not in tags:
                print 'No title or not a mp3 file at all'
                file_obj.delete()
                continue

            # XXX: we may and maybe even should do it there, but need to think
            #file_obj.backup.save(f.name, temp)

            # saving at end?
            # XXX: not saving for development reasons
            file_obj.save()
            try:
                composer = Composer.objects.get(name=tags.get('TCOM'))
            except Composer.DoesNotExist:
                composer = None
            try:
                performer = Performer.objects.get(name=tags.get('TPE1'))
            except Performer.DoesNotExist:
                performer = None
            RadioItem.objects.create(
                box_file=file_obj,
                composer=composer,
                performer=performer,
                title=tags.get('TIT2'),
            )

            temp.close()
            url.close()
            logging.debug('Saved')

