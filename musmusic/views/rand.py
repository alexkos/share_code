from django.contrib.sites.models import get_current_site
from django.http.response import HttpResponse, Http404
from django.shortcuts import render

from django.core.urlresolvers import reverse
from musmusic.models import Piece, Part, RadioItem
from json import dumps
import random
random.seed()


def shuffle(request):
    mtype = "music"
    try:
        obj = random.choice(Piece.objects.all())
    except IndexError:
        raise Http404('Something\'s wrong with our library')
    parts = obj.part_set.filter(approved=True).order_by('number')
    site = get_current_site(request)
    ctx = {
        'piece': obj,
        "type_": mtype,
        'parts': parts,
        'site': site,
    }
    return render(request, 'musmusic/shuffle.html', ctx)


def radio(request):
    if 'benefactor' in request.GET:
        model = RadioItem
    else:
        model = Part
    try:
        obj = random.choice(
            (
                model.objects
                .filter(box_file__isnull=False)
                .filter(box_file__shared_url__isnull=False)
            )
        )
    except IndexError:
        raise Http404('Something\'s wrong with our library')
    piece = obj.piece
    ctx = {
        'piece': piece,
        'part': obj,
        'composer': piece.composer,
        'performer': piece.performer,
    }
    return render(request, 'musmusic/radio.html', ctx)


def next_radio_track(request):
    if 'benefactor' in request.GET and (
            request.user.is_benefactor or request.user.is_admin
    ):
        model = RadioItem
    else:
        model = Part
    try:
        obj = random.choice(model.objects
                            .filter(box_file__isnull=False)
                            .filter(box_file__shared_url__isnull=False))
    except IndexError:
        return HttpResponse(
            dumps({
                'success': False,
                'error': 'Something\'s wrong with our library',
            }),
            content_type='application/json',
            status=404,
        )
    piece = obj.piece
    result = {}
    result.update({
        'url': obj.box_file.shared_url,
        'piece': piece.title,
        'piece_url': piece.get_absolute_url(),
    })
    composer = piece.composer
    if composer:
        result.update({
            'composer': composer.name,
            'composer_url': reverse(
                'musmusic_info_page',
                kwargs={
                    'object_type': 'composer',
                    'name': composer.slug,
                }
            ),
            'composer_bio': composer.comment,
        })
        if composer.image:
            result['composer_img'] = composer.image.url
    else:
        result['composer'] = None
    performer = piece.performer
    if performer:
        result.update({
            'performer': performer.name,
            'performer_url': reverse(
                'musmusic_info_page',
                kwargs={
                    'object_type': 'performer',
                    'name': performer.slug,
                }
            ),
        })
    else:
        result['performer'] = None
    return HttpResponse(
        dumps(result),
        content_type='application/json',
        status=200,
    )
