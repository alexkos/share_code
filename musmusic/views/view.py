#-*- coding: utf-8 -*-
from datetime import date
from django.shortcuts import (
    get_object_or_404, render, redirect,
)
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.core.cache import cache
from django.http.response import Http404, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.clickjacking import xframe_options_exempt
from django.conf import settings
from django.db.models.query import QuerySet
from django.db.models import Count, Q
from django.db import IntegrityError
from django.template.defaultfilters import slugify
from musmusic.models import (
    Composer, Instrument, Period, Form, Performer, Piece, Part,
    SheetMusic, SheetPart, PiecePartBookmark, SheetPartBookmark,
)
from json import dumps

TYPES = ('music', 'sheetmusic',)
FILTERS = {i.__name__.lower(): i
           for i in (Composer, Instrument, Period, Form)}


def show_catalog(request, mtype='music'):
    """
    Display index page of catalog
    """
    if mtype not in TYPES:
        raise Http404()
    if mtype == 'music':
        piece = 'piece'
        appr = {'approved': True}
    else:
        piece = 'sheetmusic'
        appr = {'sheet_approved': True}
    composers = (
        Composer.objects
        .filter(**appr)
        .order_by('last_name', 'first_name', 'id')
        .defer(
            'comment', 'learn_more', 'created_date',
            'box_folder', 'box_folder_sheet', 'image',
        )
    )
    instruments = (
        Instrument.objects
        .filter(**appr)
        .defer('comment', 'learn_more')
    )
    periods = (
        Period.objects
        .filter(**appr)
        .defer('comment')
    )
    forms = (
        Form.objects
        .filter(**appr)
        .defer('comment', 'learn_more')
    )
    if mtype == 'music':
        performers = (
            Performer.objects
            .filter(**appr)
            .defer('comment', 'learn_more')
        )
    else:
        performers = []

    return render(
        request, 'musmusic/catalog_v.html',
        {
            'composers': composers,
            'type_': mtype,
            'instruments': instruments,
            'periods': periods,
            'performers': performers,
            'forms': forms,
        },
    )


def create_filters(objects, available_filters=FILTERS):
    create_filter = lambda qs, filtered: (
        list(qs.values_list(filtered+"__id", flat=True).distinct())
    )

    filters = {}
    for flt, kls in available_filters.items():
        filters[flt+'s'] = kls.objects.filter(
            id__in=create_filter(objects, flt)
        ).defer('comment')
        if flt == 'composer':
            filters[flt+'s'] = filters[flt+'s'].defer(
                'created_date', 'learn_more', 'box_folder',
                'box_folder_sheet', 'image',
            )
    return filters


def show_items(request, mtype='music', filter_by=None, name=''):
    filters_ = FILTERS.copy()
    if mtype == 'music':
        filters_['performer'] = Performer
        sort_params = [
            "title", "form", "instrument",
            "period", "performer", "avg_rating",
            "-title", "-form", "-instrument",
            "-period", "-performer", "-avg_rating",
        ]
    else:
        sort_params = [
            "title", "form", "instrument", "period",
            "-title", "-form", "-instrument", "-period",
        ]

    ctx = {}
    if not name or filter_by not in filters_:
        raise Http404()

    model = filters_[filter_by]

    if ' ' in name:
        fname = 'name'
    elif name.isdigit():
        fname = 'id'
    elif 'slug' in model._meta.get_all_field_names():
        fname = 'slug'
    else:
        fname = 'slug'

    query = {fname: name}
    if mtype == 'music':
        query['approved'] = True
    elif mtype == 'sheetmusic':
        query['sheet_approved'] = True

    filtered_by_object = get_object_or_404(model, **query)
    ctx['object'] = filtered_by_object

    if mtype == 'music':
        ctx['objects'] = (
            filtered_by_object.piece_set
            .filter(approved=True)
            .select_related(
                'composer', 'performer', 'form',
                'instrument', 'period', 'rating',
            )
            .extra(
                select={
                    'avg_rating': (
                        'musmusic_piecerating.rating_sum'
                        '/musmusic_piecerating.rating_count'
                    ),
                }
            )
            .defer(
                'learn_more',
                'composer__image', 'composer__created_date',
                'composer__learn_more', 'composer__comment',
                'composer__box_folder', 'composer__box_folder_sheet',
                'form__created_date', 'form__learn_more', 'form__comment',
                'period__created_date', 'period__more', 'period__comment',
                'instrument__created_date', 'instrument__learn_more',
                'instrument__comment',
                'performer__created_date', 'performer__learn_more',
                'performer__comment',
            )
        )
    else:
        ctx['objects'] = (
            filtered_by_object.sheetmusic_set
            .select_related(
                'composer', 'form', 'instrument', 'period', 'rating',
            )
            .filter(approved=True)
            .defer(
                'learn_more',
                'composer__image', 'composer__created_date',
                'composer__learn_more', 'composer__comment',
                'composer__box_folder', 'composer__box_folder_sheet',
                'form__created_date', 'form__learn_more', 'form__comment',
                'period__created_date', 'period__more', 'period__comment',
                'instrument__created_date', 'instrument__learn_more',
                'instrument__comment',
            )
        )

    ctx['most_popular'] = {
        'music': (
            filtered_by_object.piece_set
            .filter(approved=True)
            .select_related('rating')
            .order_by('-rating__hits', 'id')
        )[:6],
    }
    if filter_by != 'performer':
        ctx['most_popular']['sheetmusic'] = (
            filtered_by_object.sheetmusic_set
            .filter(approved=True)
            .select_related('rating')
            .order_by('-rating__hits', 'id')
        )[:6]
    else:
        ctx['most_popular']['sheetmusic'] = []

    ctx['type_'] = mtype
    # Caching filters
    # trying to make it more dynamic, slow and unusable (at least for now)
    #cached_filters = cache.get(
    #    "{}_{}".format(filter_by, filtered_by_object.id),
    #)
    #if cached_filters is None:
    #    cached_filters = create_filters(ctx['objects'], filters_)
    #    cache.set(
    #        "{}_{}".format(filter_by, filtered_by_object.id),
    #        cached_filters,
    #        60 * 10,
    #    )
    #ctx['filter'] = cached_filters
    ctx['filter_by'] = filter_by
    ctx['back_view'] = mtype + "_catalog"
    ctx['forward_view'] = mtype + '_view'
    # Filtering
    for filter_field in filters_:
        if filter_field in request.GET:
            try:
                lst = [int(value)
                       for value in request.GET.getlist(filter_field)]
            except ValueError:
                raise Http404('Bad filter')
            ctx['objects'] = ctx['objects'].filter(**{
                filter_field + '_id__in': lst,
            })
    # Generating filters (slow and bad)
    ctx['filter'] = create_filters(ctx['objects'], filters_)

    # Sorting (NOTE: should be after filtering due to ORM specifics!)
    if request.GET.get('order_by') in sort_params:
        order_by = request.GET.get('order_by')
        if order_by == 'title':
            order_by = 'sort_key'
        elif order_by == '-title':
            order_by = '-sort_key'
        ctx['objects'] = (
            ctx['objects']
            .order_by(order_by, 'sort_key', 'id')
        )
        ctx['order_by'] = request.GET.get('order_by')
    else:
        ctx['objects'] = ctx['objects'].order_by('form', 'sort_key', 'id')
        ctx['order_by'] = 'form'
    return render(request, 'musmusic/show_items.html', ctx)


def show_piece(request, musictype='music', object_id=None):
    if musictype == 'music':
        obj = get_object_or_404(Piece, id=object_id)
        parts = (
            obj.part_set
            .filter(approved=True)
            .select_related('box_file', 'hq_box_file')
            .extra(
                select={
                    'bookmarks_num': (
                        'SELECT COUNT(*) '
                        'FROM {} '
                        'WHERE part_id={}.id AND user_id=%s'
                    ).format(
                        PiecePartBookmark._meta.db_table,
                        Part._meta.db_table,
                    ),
                },
                select_params=(request.user.id,)
            )
            .order_by('number', 'title', 'id')
        )
        template = 'musmusic/music-piece.html'
    elif musictype == 'sheetmusic':
        obj = get_object_or_404(SheetMusic, id=object_id)
        parts = (
            obj.sheetpart_set
            .filter(approved=True)
            .extra(
                select={
                    'bookmarks_num': (
                        'SELECT COUNT(*) '
                        'FROM {} '
                        'WHERE part_id={}.id AND user_id=%s'
                    ).format(
                        SheetPartBookmark._meta.db_table,
                        SheetPart._meta.db_table,
                    ),
                },
                select_params=(request.user.id,)
            )
            .order_by('number', 'title', 'id')
        )
        template = 'musmusic/music-sheet.html'
    if (
        not obj.approved
        and obj.user != request.user
        and (not request.user.is_authenticated() or not request.user.is_admin)
    ):
        raise Http404('Page not found')
    try:
        obj.rating_obj = obj.rating.get()
    except ObjectDoesNotExist:
        obj.rating_obj = obj.rating.create()
    obj.rating_obj.hits += 1
    obj.rating_obj.save()
    ctx = {
        'back_view': musictype+"_catalog",
        'piece': obj,
        "type_": musictype,
        'parts': parts,
    }
    return render(request, template, ctx)

@xframe_options_exempt
def embed_sheet(request, object_id):
    part = get_object_or_404(SheetPart, id=object_id)
    sheet = part.sheet
    ctx = {
        'sheet': sheet,
        'part': part,
    }
    return render(request, 'musmusic/embed.html', ctx)


def embed_sheet_popup(request, object_id):
    part = get_object_or_404(SheetPart, id=object_id)
    ctx = {
        'object_id': object_id,
        'part': part,
    }
    return render(request, 'musmusic/embed-this.html', ctx)


def add_bookmark(request, musictype="music", part_id=0):
    if not request.user.is_authenticated():
        return HttpResponse(
            dumps({
                'success': False,
                'status': 'Should be authenticated'
            }),
            content_type='application/json', status=403,
        )
    if musictype == 'music':
        part_model = Part
        model = PiecePartBookmark
    elif musictype == 'sheetmusic':
        part_model = SheetPart
        model = SheetPartBookmark
    else:
        raise Http404('Unknown music type')
    try:
        part = part_model.objects.get(id=part_id)
    except part_model.DoesNotExist:
        return HttpResponse(
            dumps({
                'success': False,
                'status': 'Part not found'
            }),
            content_type='application/json', status=404,
        )
    if model.objects.filter(user=request.user, part=part).exists():
        return HttpResponse(
            dumps({
                'success': False,
                'status': 'Already exists'
            }),
            content_type='application/json', status=400,
        )
    try:
        bookmark = model(user=request.user, part=part)
        bookmark.save()
    except Exception:
        return HttpResponse(
            dumps({
                'success': False,
                'status': 'Unknown error'
            }),
            content_type='application/json', status=500,
        )
    else:
        return HttpResponse(
            dumps({
                'success': True,
                'status': 'Success',
                'url': reverse(
                    'musmusic_remove_bookmark',
                    kwargs={'musictype': musictype, 'part_id': part.id},
                ),
                'part_id': str(part.id),
            }),
            content_type='application/json', status=200,
        )


def remove_bookmark(request, musictype="music", part_id=0):
    if not request.user.is_authenticated():
        return HttpResponse(
            dumps({
                'success': False,
                'status': 'Should be authenticated'
            }),
            content_type='application/json', status=403,
        )
    if musictype == 'music':
        part_model = Part
        model = PiecePartBookmark
    elif musictype == 'sheetmusic':
        part_model = SheetPart
        model = SheetPartBookmark
    else:
        raise Http404('Unknown music type')
    try:
        part = part_model.objects.get(id=part_id)
    except part_model.DoesNotExist:
        return HttpResponse(
            dumps({
                'success': False,
                'status': 'Part not found'
            }),
            content_type='application/json', status=404,
        )
    if model.objects.filter(user=request.user, part=part).exists():
        try:
            bookmark_qs = model.objects.filter(user=request.user, part=part)
            # Just in rare case there are not-unique bookmark
            for bookmark in bookmark_qs:
                bookmark.delete()
        except Exception:
            return HttpResponse(
                dumps({
                    'success': False,
                    'status': 'Unknown error'
                }),
                content_type='application/json', status=500,
            )
        else:
            return HttpResponse(
                dumps({
                    'success': True,
                    'status': 'Success',
                    'url': reverse(
                        'musmusic_add_bookmark',
                        kwargs={'musictype': musictype, 'part_id': part.id},
                    ),
                    'part_id': str(part.id),
                }),
                content_type='application/json', status=200,
            )
    else:
        return HttpResponse(
            dumps({
                'success': False,
                'status': 'Does not exists'
            }),
            content_type='application/json', status=400,
        )


def rate_piece(
    request, musictype="music",
    rate_type="recording", piece_id=0, rate_score=0
):
    rate_score = int(rate_score)
    if not request.user.is_authenticated():
        return HttpResponse(
            dumps({
                'success': False,
                'status': 'Should be authenticated'
            }),
            content_type='application/json', status=403,
        )
    if musictype == 'music':
        piece_model = Piece
    else:
        raise Http404('Unknown music type')
    if rate_type not in ('playing', 'recording'):
        raise Http404('Unknown rating type')
    try:
        piece = piece_model.objects.get(id=piece_id)
    except piece_model.DoesNotExist:
        return HttpResponse(
            dumps({
                'success': False,
                'status': 'Piece not found'
            }),
            content_type='application/json', status=404,
        )
    if piece.rates.filter(user=request.user).exists():
        rate = piece.rates.filter(user=request.user).get()
        if rate_type == 'recording':
            rate.rate_recording = rate_score
        elif rate_type == 'playing':
            rate.rate_playing = rate_score
        rate.save()
    else:
        if rate_type == 'recording':
            piece.rates.create(user=request.user, rate_recording=rate_score)
        elif rate_type == 'playing':
            piece.rates.create(user=request.user, rate_playing=rate_score)
    piece_model.objects.update()
    piece.rating.update()
    return HttpResponse(
        dumps({
            'success': True,
            'status': 'Success',
            'score': piece.get_avg_rating(),
            'count': piece.rating.get().rating_count,
        }),
        content_type='application/json', status=200,
    )


@login_required
def download_part(request, part_id):
    part = get_object_or_404(Part, id=part_id)
    if not part.box_file or not part.box_file.shared_url:
        raise Http404('There is no file for this part')
    if request.user.profile == 'PRO':
        return redirect(part.box_file.shared_url)
    if not request.user.downloads_set.exists():
        try:
            downloads = request.user.downloads_set.create()
        except IntegrityError:
            # duplicate entry: probably just created in another process
            request.user.downloads_set.update()
            downloads = request.user.downloads_set.get()
    else:
        downloads = request.user.downloads_set.get()
    if (
        downloads.date == date.today()
        and downloads.count > settings.LITE_DOWNLOADS
    ):
        raise Http404(
            'Two many downloads today, '
            'please come back tomorrow, upgrade your account '
            'or enjoy unlimited online streaming.'
        )
    elif downloads.date != date.today():
        downloads.count = 1
        downloads.save()
    else:
        downloads.count += 1
        downloads.save()
    return redirect(part.box_file.shared_url)


def get_piece_by_slug(request):
    if 'keyword' in request.GET:
        keyword = request.GET.get('keyword')
        piece_set = Piece.objects.filter(slug=keyword)
        sheet_set = SheetMusic.objects.filter(slug=keyword)
        if piece_set.exists():
            piece = piece_set[0]
            return redirect(piece)
        elif sheet_set.exists():
            sheet = sheet_set[0]
            return redirect(sheet)
    return redirect('/')


@staff_member_required
def list_slugs(request):
    response = HttpResponse(content_type="text/plain")
    for el in list(
        set(
            [
                x[0] for x in (
                    list(Piece.objects.values_list('slug'))
                    + list(SheetMusic.objects.values_list('slug'))
                )
            ]
        )
    ):
        response.write(el + '\n')
    return response


def show_items_redirect(request, mtype='music', filter_by=None, name=''):
    return redirect(
        mtype, mtype=mtype, filter_by=filter_by, name=name, permanent=True,
    )


def smart_composer_redirect(request, mtype='music', name=''):
    smart_args = dict(map(
        None,
        ('last_name', 'first_name'),
        name.split('-', 1),
    ))
    smart_args_reverse = dict(map(
        None,
        ('first_name', 'last_name'),
        name.split('-', 1),
    ))
    reversed_slug = slugify('-'.join(reversed(name.split('-', 1))))
    if name.isdigit():
        qs = Composer.objects.filter(id=name)
    else:
        qs = Composer.objects.filter(
            Q(name=name) | Q(slug=name) | Q(slug=reversed_slug)
            | Q(**smart_args) | Q(**smart_args_reverse)
        )
    if qs.exists():
        composer = qs[0]
        return redirect(
            composer.get_filter_url(mtype), permanent=True
        )
    else:
        raise Http404('Page not found')


def show_piece_redirect(request, musictype='music', object_id=None):
    if musictype == 'music':
        obj = get_object_or_404(Piece, id=object_id)
    elif musictype == 'sheetmusic':
        obj = get_object_or_404(SheetMusic, id=object_id)
    return redirect(obj)
