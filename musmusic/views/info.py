from django.http.response import Http404
from django.shortcuts import render, get_object_or_404
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from musmusic.models import (
    Composer, Instrument, Period, Form, Performer,
)

TYPES = {
    i.__name__.lower(): i
    for i in (Composer, Instrument, Period, Form, Performer)
}


def show_info(request, object_type, name):
    model = TYPES.get(object_type)
    if not model:
        raise Http404('Unknown type')
    if ' ' in name:
        fname = 'name'
    elif name.isdigit():
        fname = 'id'
    elif 'slug' in model._meta.get_all_field_names():
        fname = 'slug'
    else:
        fname = 'slug'

    qs = model.objects.filter(**{fname: name})
    if object_type != 'performer':
        qs = qs.filter(Q(approved=True) | Q(sheet_approved=True))
    else:
        qs = qs.filter(approved=True)
    if not qs.exists():
        raise Http404('Page not found')
    else:
        obj = qs[0]

    ctx = {
        'obj': obj,
        'object_type': object_type,
        'top_pieces': obj.piece_set.order_by('-rating__hits', 'id')[:9],
    }
    return render(request, 'musmusic/info.html', ctx)
