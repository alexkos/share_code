#-*- coding: utf-8 -*-
from django.conf import settings
from haystack.views import SearchView
from haystack.query import SearchQuerySet
from haystack.forms import ModelSearchForm


class MusicSearchForm(ModelSearchForm):

    def clean_q(self):
        value = self.cleaned_data.get('q')
        if value:
            return value.replace('. ', ' ')
        else:
            return ''


class MusicSearchView(SearchView):

    __name__ = "MusicSearchView"

    def __init__(self, *args, **kwargs):
        self.musictype = 'music'
        kwargs['form_class'] = MusicSearchForm
        super(MusicSearchView, self).__init__(*args, **kwargs)

    def __call__(self, request, musictype='music'):
        """
        Generates the actual response to the search.

        Relies on internal, overridable methods to construct the response.
        """
        self.request = request
        self.musictype = musictype

        self.searchqueryset = SearchQuerySet().filter(musictype=self.musictype)

        self.form = self.build_form()
        self.query = self.get_query()
        self.results = self.get_results()

        return self.create_response()

    def extra_context(self):
        extra = super(MusicSearchView, self).extra_context()
        extra['type_'] = self.musictype
        extra['forward_view'] = 'music_view'
        if self.form.is_valid():
            qs = self.form.cleaned_data['q']
            extra['querystring'] = qs
            extra['suggestion'] = self.searchqueryset.spelling_suggestion(qs)

        return extra
