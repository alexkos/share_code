#-*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render, redirect
from django.core.urlresolvers import reverse
from django.http.response import Http404, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.conf import settings
from django.db.models import Count
from musmusic.models import (
    Composer, Instrument, Period, Form, Performer,
    Piece, Part, SheetMusic, SheetPart
)
from musmusic.forms import (
    PieceEditForm,
    ComposerForm, PerformerForm, InstrumentForm, PeriodForm, FormForm,
    BoxForm, PieceConnectForm, SheetConnectForm,
    PieceCreateForm, SheetCreateForm,
)
from musmusic.decorators import edit_timeout
from box_client.models import Token, UploadedFile, Folders
from box_client.client import BoxClient
from json import dumps, loads

TYPES = ('music', 'sheetmusic',)


@login_required
@edit_timeout
def edit_piece(request, musictype='music', object_id=None):
    user = request.user
    edit_form = PieceEditForm
    if musictype == 'music':
        if object_id:
            piece = get_object_or_404(Piece, id=object_id)
            parts = piece.part_set.order_by('number', 'title', 'id')

        else:
            piece = None
            parts = []
    else:
        if object_id:
            piece = get_object_or_404(SheetMusic, id=object_id)
            parts = piece.sheetpart_set.order_by('number', 'title', 'id')
        else:
            piece = None
            parts = []
    if request.method == 'GET':
        if piece:
            form = edit_form(instance=piece, user=user, musictype=musictype)
        else:
            form = edit_form(user=user, musictype=musictype)
    elif request.method == 'POST':
        if piece:
            form = edit_form(instance=piece, data=request.POST, user=user, musictype=musictype)
        else:
            form = edit_form(data=request.POST, user=user, musictype=musictype)
        if form.is_valid():
            form.save()
            object_id = form.instance.id
            if 'submit_and_continue' in request.POST:
                urlname = 'music_edit'
            else:
                urlname = 'music_view'
            return redirect(reverse(urlname, kwargs={
                'musictype': musictype,
                'object_id': object_id,
            }))

    else:
        raise Http404('Unknown request method')
    try:
        token = (
            Token.objects
            .filter(name='upload')
            .latest('update')
        ).access_token
    except Token.DoesNotExist:
        raise Http404('Cannot find access token')
    ctx = {
        'piece': piece,
        'parts': parts,
        'form': form,
        'type_': musictype,
        'musictype': musictype,
        'token': token,
    }
    return render(request, 'musmusic/edit-piece.html', ctx)


@staff_member_required
def delete_piece(request, musictype="music", object_id=0):
    if musictype == 'music':
        piece = get_object_or_404(Piece, id=object_id)
    elif musictype == 'sheetmusic':
        piece = get_object_or_404(SheetMusic, id=object_id)
    else:
        raise Http404('Unknown object type')
    piece.delete()
    return redirect(musictype+"_catalog")


@login_required
@edit_timeout
def edit_object(request, object_type='', object_id=0):
    if object_type == 'composer':
        edit_form = ComposerForm
        model = Composer
    elif object_type == 'performer':
        edit_form = PerformerForm
        model = Performer
    elif object_type == 'instrument':
        edit_form = InstrumentForm
        model = Instrument
    elif object_type == 'form':
        edit_form = FormForm
        model = Form
    elif object_type == 'period':
        edit_form = PeriodForm
        model = Period
    else:
        raise Http404('Unknown object type')
    if request.method == 'GET':
        if object_id:
            obj = get_object_or_404(model, id=object_id)
            form = edit_form(instance=obj)
        else:
            form = edit_form()
        status = 200
    elif request.method == 'POST':
        if object_id:
            obj = get_object_or_404(model, id=object_id)
            form = edit_form(instance=obj, data=request.POST)
        else:
            form = edit_form(data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse(
                dumps({
                    'success': True,
                    'id': form.instance.id,
                    'name': form.instance.name
                }),
                content_type='application/json', status=200,
            )
        else:
            status = 400
    else:
        raise Http404('Unknown request method')

    ctx = {
        'form': form,
        'objecttype': object_type,
    }
    return render(request, 'musmusic/create_object.html', ctx, status=status)


@staff_member_required
def set_file_to_part(request, contenttype, part_id, fieldname, scope='all'):
    status = 200
    if contenttype == "music":
        model = Part
        if fieldname == 'box_file':
            files = (
                UploadedFile.objects
                .filter(name__endswith="mp3")
            )
        elif fieldname == 'hq_box_file':
            files = (
                UploadedFile.objects
                .exclude(name__endswith="pdf")
                .exclude(name__endswith="mp3")
            )
        else:
            raise Http404("Unknown field")
    elif contenttype == "sheet":
        model = SheetPart
        if fieldname == 'box_file':
            files = (
                UploadedFile.objects
                .filter(name__endswith="pdf")
            )
        else:
            raise Http404("Unknown field")
    else:
        raise Http404("Unknown content type")
    if scope == 'orphans':
        files = files.filter(
            lossypart__isnull=True,
            losslesspart__isnull=True,
            sheetpart__isnull=True,
        ).distinct()
    part = get_object_or_404(model, id=part_id)
    if request.method == 'POST':
        form = BoxForm(files=files, data=request.POST, action=request.path)
        file_id = request.POST.get('box_file')
        file_obj = get_object_or_404(UploadedFile, id=file_id)
        setattr(part, fieldname, file_obj)
        part.save()
        return HttpResponse(dumps({
            'success': True,
            'id': 'id_{}_{}'.format(part.id, fieldname),
            'file_text': unicode(file_obj),
        }))
    else:
        form = BoxForm(files=files, action=request.path)
    return render(
        request, 'musmusic/set_file.html',
        {'part': part, 'files': files, 'form': form},
        status=status,
    )


@staff_member_required
def disconnect_file(request, part_id, filetype):
    if filetype == 'sheet':
        model = SheetPart
        attr = 'box_file'
    elif filetype == 'lq':
        model = Part
        attr = 'box_file'
    elif filetype == 'hq':
        model = Part
        attr = 'hq_box_file'
    else:
        return HttpResponse(
            dumps({'success': False, error: "Bad filetype"}),
            status=400,
        )
    try:
        obj = model.objects.get(id=part_id)
    except model.DoesNotExist:
        return HttpResponse(
            dumps({'success': False, error: "Bad filetype"}),
            status=404,
        )
    setattr(obj, attr, None)
    obj.save()
    return HttpResponse(
        dumps({'success': True}),
        status=200,
    )


@staff_member_required
def connect_file(request, file_id, task='connect'):
    f = get_object_or_404(UploadedFile, id=file_id)
    action=request.path
    if f.name.endswith('.pdf'):
        model = SheetMusic
        part_model = SheetPart
        attr = 'box_file'
        piece_attr = 'sheet'
        connect_form_class = SheetConnectForm
        create_form_class = SheetCreateForm
        parttype = 'sheet'
        musictype = 'sheetmusic'
    elif f.name.endswith('.mp3'):
        model = Piece
        part_model = Part
        attr = 'box_file'
        piece_attr = 'piece'
        connect_form_class = PieceConnectForm
        create_form_class = PieceCreateForm
        parttype = 'lq'
        musictype = 'music'
    else:
        model = Piece
        part_model = Part
        attr = 'hq_box_file'
        piece_attr = 'piece'
        connect_form_class = PieceConnectForm
        create_form_class = PieceCreateForm
        parttype = 'hq'
        musictype = 'music'

    if task == 'connect':
        form_class = connect_form_class
        initial = {}
    elif task == 'create':
        initial = {'title': f.name.rsplit('.', 1)[0]}
        form_class = create_form_class
    else:
        raise Http404('Unknown task')

    if request.method == "POST":
        form = form_class(data=request.POST, action=action)
        if form.is_valid():
            if task == 'connect':
                piece = form.cleaned_data['piece']
            else:
                piece = form.save()
            part = part_model.objects.create(**{
                piece_attr: piece,
                attr: f,
                'title': f.name.rsplit('.', 1)[0],
            })
            return HttpResponse(
                dumps({
                        'success': True,
                        'piece_id': piece.id,
                        'part_id': part.id,
                        'parttype': parttype,
                        'edit_url': reverse(
                            'music_edit',
                            kwargs={
                                'musictype': musictype,
                                'object_id': piece.id,
                            },
                        ),
                        'disconnect_url': reverse(
                            'musmusic_disconnect_file',
                            kwargs={
                                'part_id': part.id,
                                'filetype': parttype,
                            },
                        ),
                }),
                status=200,
            )
        else:
            status = 400
    else:
        form = form_class(action=action, initial=initial)
        status = 200
    return render(
        request,
        'musmusic/connect_file.html',
        {'form': form},
        status=status,
    )


def finished_encoding(request, part_id, box_id, auth_code):

    if auth_code != settings.ENCODER_AUTH_CODE:
        return HttpResponse('Invalid auth code', status=401)

    try:
        part = Part.objects.get(id=part_id)
    except Part.DoesNotExist:
        return HttpResponse('Invalid part id', status=400)

    if part.box_file_id:
        return HttpResponse('File already exists', status=400)

    piece = part.piece
    box = BoxClient(token_name="download")
    file_obj = box.share_file(box_id)
    if file_obj.path_collection:
        description = ''.join(
            [
                '/' + entry.get('name', '')
                for entry in file_obj.path_collection.get('entries')
            ]
        ) + '/' + file_obj.name
    else:
        description = upload_filename
    upload_filename = file_obj.name
    box_file = UploadedFile(
        box_id=box_id,
        name=upload_filename,
        description=description,
        shared_url=file_obj.shared_link.get('download_url'),
    )
    box_file.save()
    part.box_file = box_file
    part.filename = part.hq_filename.rsplit('.', 1)[0] + '.mp3'
    part.save()
    return HttpResponse('OK', status=200)
