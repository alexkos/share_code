import random
import string
from urllib2 import urlopen, unquote, build_opener
from lxml import etree
from lxml.html import parse, fromstring
from lxml.cssselect import CSSSelector
from threading import Thread, active_count
from django.conf import settings
from django.shortcuts import get_object_or_404, render, redirect
from django.http.response import Http404, HttpResponse
from django.http import QueryDict
from django.contrib.admin.views.decorators import staff_member_required
from django.core.cache import cache
from django.core.urlresolvers import reverse
from musmusic.models import (
    Composer, SheetMusic, SheetPart
)
from musmusic.forms import IMSLPUrlForm, IMSLPMassUrlForm, IMSLPMetadataForm
from box_client.client import BoxClient
from box_client.models import Token, UploadedFile, Folders
from box_client.exceptions import AccessError, UnauthorizedError
from json import dumps, loads

random.seed()


@staff_member_required
def import_imslp(request, musictype):
    def parse_url(request, url):
        html = cache.get('imslp_piece_page_' + url)
        if not html:
            try:
                u = urlopen(url)
                html = '\n'.join(u.readlines())
            except IOError:
                raise Http404('Cannot load page from IMSLP')
            cache.set('imslp_piece_page_' + url, html, 3*60*60)
        doc = fromstring(html)
        sel = CSSSelector('#firstHeading')
        title = ''
        if sel(doc):
            title = sel(doc)[0].text
            try:
                title, composer_name = title.rsplit('(', 1)
            except ValueError:
                title = ''
                composer_name = ''
            else:
                title = title.strip()
                composer_name = composer_name.strip(')')
            if ',' in composer_name:
                last, first = composer_name.split(',', 1)
                composer_name = first.strip() + ' ' + last.strip()
            if Composer.objects.filter(name=composer_name).exists():
                composer = Composer.objects.filter(name=composer_name)[0]
            else:
                composer = None
        initial = {
            'title': title,
            'composer': composer,
            'user': request.user,
            'imported_from': url,
        }
        sel = CSSSelector('.we_file_download')
        title_sel = 'a span[title="Download this file"]'
        link_sel = '.we_file_info2 .hidden a.internal'
        count = 0
        for i, el in enumerate(sel(doc)):
            try:
                part_title = el.cssselect(title_sel)[0].text_content().strip()
            except IndexError:
                part_title = 'Parse error: cannot find title for this part'
            try:
                part_url = el.cssselect(link_sel)[0].get('href')
            except IndexError:
                part_url = '/wiki/NOT_FOUND'
            initial['part_title_{}'.format(i)] = part_title
            initial['part_url_{}'.format(i)] = 'http://imslp.org' + part_url
            initial['part_filename_{}'.format(i)] = unquote(
                part_url.rsplit('/', 1)[-1]
            )
            initial['part_enabled_{}'.format(i)] = False
            count = i
        initial['part_number'] = count + 1
        sel = CSSSelector('.wi_head')
        info_dict = []
        if sel(doc):
            root = sel(doc)[0]
            if root.cssselect('table'):
                root = root.cssselect('table')[0]
                if root.cssselect('tbody'):
                    root = root.cssselect('tbody')[0]
                for row in root.cssselect('tr'):
                    key = row.cssselect('th')[0].text_content()
                    value = row.cssselect('td')[0].text_content()
                    info_dict.append((key, value.replace('\n', '<br />')))
        sel = CSSSelector('.wi_body')
        if sel(doc):
            root = sel(doc)[0]
            if root.cssselect('table'):
                root = root.cssselect('table')[0]
                if root.cssselect('tbody'):
                    root = root.cssselect('tbody')[0]
                for row in root.cssselect('tr'):
                    try:
                        key = row.cssselect('th')[0].text_content()
                    except IndexError:
                        continue
                    value = row.cssselect('td')[0].text_content()
                    info_dict.append((key, value.replace('\n', '<br />')))
        return initial, info_dict

    def download_parts(sheet, data, random_id, session_id='', step=None):
        """
            Function for downloading and saving parts.
            Meant to be executed in thread.
        """
        status_dict = {'status': 'running'}
        status_dict['parts'] = dict([
            (num, 'pending') for num in range(data['part_number'])
            if data.get('part_enabled_{}'.format(num))
        ])
        cache.set('async_download_' + random_id, status_dict, 6*60*60)
        if session_id and step is not None:
            results = cache.get('session_results_' + session_id)
            results[step] = 'downloading'
            cache.set(
                'session_results_' + session_id,
                results,
                6*60*60,
            )
        try:
            box = BoxClient(token_name="download")
        except UnauthorizedError as exc:
            status_dict = {'status': 'error', 'exception': exc}
            cache.set('async_download_' + random_id, status_dict, 6*60*60)
            if session_id and step is not None:
                results = cache.get('session_results_' + session_id)
                results[step] = 'error'
                cache.set(
                    'session_results_' + session_id,
                    results,
                    6*60*60,
                )
            return
        opener = build_opener()
        opener.addheaders.append(('Cookie', 'imslpdisclaimeraccepted=yes'))
        if sheet.composer:
            box_folder = sheet.composer.get_folder('sheetmusic')
        else:
            box_folder = Folders.objects.get(
                description='no-composer-sheetmusic',
            )
        for i in range(data['part_number']):
            if data.get('part_enabled_{}'.format(i)):
                status_dict['parts'][i] = 'starting'
                cache.set('async_download_' + random_id, status_dict, 6*60*60)
                try:
                    file_obj = box.upload_file(
                        file=opener.open(data.get('part_url_{}'.format(i))),
                        name=data.get('part_filename_{}'.format(i)),
                        parent_id=box_folder.box_id,
                    )
                    file_id = int(file_obj.get('entries')[0].get('id'))
                    file_obj = box.share_file(file_id=file_id)
                    if file_obj.path_collection:
                        description = ''.join([
                            '/' + entry.get('name', '')
                            for entry in file_obj.path_collection.get(
                                'entries'
                            )
                        ]) + '/' + file_obj.name
                    else:
                        description = upload_filename
                except AccessError, exc:
                    status_dict['parts'][i] = 'error'
                    status_dict['status'] = 'error'
                    status_dict['exception'] = exc
                    cache.set('async_download_' + random_id, status_dict, 6*60*60)
                    return
                status_dict['parts'][i] = 'downloaded'
                cache.set('async_download_' + random_id, status_dict, 6*60*60)

                box_file = UploadedFile.objects.create(
                    box_id=file_obj.id,
                    name=file_obj.name,
                    description=description,
                    shared_url=file_obj.shared_link.get('download_url'),
                )
                status_dict['parts'][i] = 'saved'
                cache.set('async_download_' + random_id, status_dict, 6*60*60)
                part = SheetPart.objects.create(
                    title=data.get('part_title_{}'.format(i)),
                    filename=file_obj.name,
                    box_file=box_file,
                    sheet=sheet,
                )
                status_dict['parts'][i] = 'success'
                cache.set('async_download_' + random_id, status_dict, 6*60*60)
        if session_id and step is not None:
            results = cache.get('session_results_' + session_id)
            results[step] = 'success'
            cache.set(
                'session_results_' + session_id,
                results,
                6*60*60,
            )
    # View function itself starts here
    url = None
    if 'session' in request.GET:
        session_id = request.GET.get('session')
    else:
        session_id = None
    if 'step' in request.GET:
        step = int(request.GET.get('step'))
    else:
        step = None
    if session_id and step:
        links = cache.get('session_links_' + session_id)
        if step + 1 != len(links):
            qd = request.GET.copy()
            qd.update({
                'step': step + 1,
                'url': links[step + 1],
            })
            next_link = '?' + qd.urlencode()
        else:
            next_link = None
    else:
        next_link = None

    if 'url' not in request.GET:
        url_form = IMSLPUrlForm()
    else:
        url_form = IMSLPUrlForm(data=request.GET)
        if url_form.is_valid():
            url = url_form.cleaned_data['url']
    if not url:
        return render(
            request,
            'musmusic/import_imslp.html',
            {'form': url_form, 'session_id': session_id},
        )
    else:
        already_imported = SheetMusic.objects.filter(imported_from=url)
        if request.method == 'GET':
            initial, info_dict = parse_url(request, url)
            form = IMSLPMetadataForm(initial=initial)
            return render(
                request,
                'musmusic/import_imslp.html',
                {
                    'form': form,
                    'info': info_dict,
                    'session_id': session_id,
                    'next_link': next_link,
                    'already_imported': already_imported,
                },
            )
        elif request.method == 'POST':
            initial, info_dict = parse_url(request, url)
            form = IMSLPMetadataForm(
                initial=initial,
                data=request.POST,
            )
            if form.is_valid():
                form.save()
                sheet = form.instance
                data = form.cleaned_data
                random_id = ''.join(
                    random.choice(
                        string.ascii_letters + string.digits
                    ) for x in range(6)
                )
                while cache.get('async_download_' + random_id):
                    random_id = ''.join(
                        random.choice(
                            string.ascii_letters + string.digits
                        ) for x in range(6)
                    )
                cache.set(
                    'async_download_' + random_id,
                    {'status': 'starting'},
                    6*60*60,
                )
                if session_id and step is not None:
                    kwargs = {'session_id': session_id, 'step': step}
                    results = cache.get('session_results_' + session_id)
                    results[step] = 'started'
                    cache.set(
                        'session_results_' + session_id,
                        results,
                        6*60*60,
                    )
                else:
                    kwargs = {}
                thread = Thread(
                    target=download_parts,
                    args=[sheet, data, random_id],
                    kwargs=kwargs,
                )
                thread.start()
                #download_parts(sheet, data)
                if not session_id or step is None:
                    return render(
                        request,
                        'musmusic/import_imslp.html',
                        {
                            'success': True,
                            'sheet': sheet,
                            'random_id': random_id,
                        },
                    )
                else:
                    links = cache.get('session_links_' + session_id)
                    threads = cache.get('session_threads_' + session_id)
                    pieces = cache.get('session_pieces_' + session_id)
                    threads[step] = random_id
                    pieces[step] = sheet
                    cache.set(
                        'session_threads_' + session_id,
                        threads,
                        6*60*60,
                    )
                    cache.set(
                        'session_pieces_' + session_id,
                        pieces,
                        6*60*60,
                    )

                    if step == len(links) - 1:
                        return(HttpResponse('Session ended successfully'))
                    q = QueryDict('', mutable=True)
                    q.update(
                        {
                            'session': session_id,
                            'step': step + 1,
                            'url': links[step + 1],
                        },
                    )
                    url = reverse(
                        'musmusic_import_imslp',
                        kwargs={'musictype': musictype}
                    ) + '?' + q.urlencode()
                    return redirect(url)

            else:
                return render(
                    request,
                    'musmusic/import_imslp.html',
                    {
                        'form': form,
                        'info': info_dict,
                        'session_id': session_id,
                        'next_link': next_link,
                        'already_imported': already_imported,
                    },
                )
        else:  # Neither GET nor POST
            raise Http404('Invalid request method')

@staff_member_required
def async_download_status(request, random_id):
    status_dict = cache.get('async_download_' + random_id)
    if not status_dict:
        raise Http404('Not existent or old id')
    return render(
        request,
        'musmusic/imslp_download_status.html',
        status_dict,
    )


@staff_member_required
def mass_import_imslp(request, musictype):

    def get_links_from_category(request, url):
        try:
            doc = parse(url)
        except IOError:
            raise Http404('Cannot load page from IMSLP')
        sel = CSSSelector('a.categorypagelink')
        urls = []
        titles = []
        for i, el in enumerate(sel(doc)):
            url = el.get('href')
            title = el.get('title')
            if url.startswith('/wiki/'):
                urls.append('http://imslp.org' + url)
                titles.append(title)
            else:
                continue
        return urls, titles

    def precache_pages(links):
        for url in links:
            html = cache.get('imslp_piece_page_' + url)
            if not html:
                try:
                    u = urlopen(url)
                    html = '\n'.join(u.readlines())
                except IOError:
                    continue
                else:
                    cache.set('imslp_piece_page_' + url, html, 3*60*60)

    # Actual view starts here:
    url = None
    if 'url' not in request.GET:
        url_form = IMSLPMassUrlForm()
    else:
        url_form = IMSLPMassUrlForm(data=request.GET)
        if url_form.is_valid():
            url = url_form.cleaned_data['url']
    if not url:
        return render(
            request,
            'musmusic/mass_import_imslp.html',
            {'form': url_form},
        )
    else:
        links, titles = get_links_from_category(request, url)
        if links:
            random_id = ''.join(
                random.choice(
                    string.ascii_letters + string.digits
                ) for x in range(6)
            )
            while cache.get('session_links_' + random_id):
                random_id = ''.join(
                    random.choice(
                        string.ascii_letters + string.digits
                    ) for x in range(6)
                )
            cache.set(
                'session_links_' + random_id,
                links,
                6*60*60,
            )
            cache.set(
                'session_titles_' + random_id,
                titles,
                6*60*60,
            )
            cache.set(
                'session_results_' + random_id,
                ['pending'] * len(links),
                6*60*60
            )
            cache.set(
                'session_threads_' + random_id,
                [''] * len(links),
                6*60*60
            )
            cache.set(
                'session_pieces_' + random_id,
                [None] * len(links),
                6*60*60
            )
            q = QueryDict('', mutable=True)
            q.update(
                {
                    'session': random_id,
                    'step': 0,
                    'url': links[0],
                },
            )
            url = reverse(
                'musmusic_import_imslp',
                kwargs={'musictype': musictype}
            ) + '?' + q.urlencode()
            thread = Thread(
                target=precache_pages,
                args=[links],
            )
            thread.start()
            return redirect(url)
        else:
            return render(request, 'musmusic/mass_import_imslp.html', {
                'form': url_form,
                'error': 'Cannot parse links, please try another page.'
            })


@staff_member_required
def session_status(request, random_id):
    links = cache.get('session_links_' + random_id)
    titles = cache.get('session_titles_' + random_id)
    threads = cache.get('session_threads_' + random_id)
    results = cache.get('session_results_' + random_id)
    pieces = cache.get('session_pieces_' + random_id)
    if not links or not threads or not results or not pieces:
        raise Http404('Not existent or old id')
    return render(
        request,
        'musmusic/imslp_session_status.html',
        {
            'results': map(
                None,
                range(len(links)), links, titles, threads,
                results, pieces,
            ),
            'session_id': random_id,
        },
    )

@staff_member_required
def threads_count(request):
    return HttpResponse(
        dumps({'active_count': active_count()}),
        content_type='application/json',
    )
