from datetime import date
from django.http.response import HttpResponseRedirect


def edit_timeout(view_func):
    def _view(request, *args, **kwargs):
        if request.user.register_date and request.user.register_date >= date.today():
            # There could be some page
            return HttpResponseRedirect('/')
        else:
            return view_func(request, *args, **kwargs)
    _view.__name__ = view_func.__name__
    _view.__dict__ = view_func.__dict__
    _view.__doc__ = view_func.__doc__

    return _view
