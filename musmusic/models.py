#-*- coding: utf-8 -*-
import re

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.core.urlresolvers import reverse
from django.http.response import Http404
from django.template.defaultfilters import slugify
from athumb.fields import ImageWithThumbsField
from tinymce.models import HTMLField

import reversion
from musopen.storage_backend import S3PublicStorage
import box_client.models as box
from box_client.client import get_upload_folder

PUBLIC_MEDIA_BUCKET = S3PublicStorage(bucket=settings.AWS_STORAGE_BUCKET_NAME)
DOWNLOAD_SERVER_URL = 'http://musopen.org'

METADATA = {
    'sheet': ('composer', 'form', 'period', 'instrument'),
    'music': ('composer', 'form', 'period', 'instrument', 'performer'),
}


def digitsrepl(matchobj):
    # Add zeros to digits
    return '0'*(5-len(matchobj.group(0))) + matchobj.group(0)


def has_changed(instance, field):
    if not instance.pk:
        return (False, None, None)
    old_instance = (
        instance.__class__._default_manager
        .filter(pk=instance.pk).get()
    )
    try:
        old_value = getattr(old_instance, field)
    except Exception:
        old_value = None
    value = getattr(instance, field)
    return (old_value != value, old_value, value)


class CatalogManager(models.Manager):
    '''This manager will be used on the catalog page to hide part of content
    whcih will be shown with <a>Show more</a> link which is handled by JS.
    '''
    def catalog_visible_part(self, **kwargs):
        visible_part = settings.HIDDEN_PART_IN_CATALOGUE
        qs = self.get_query_set(**kwargs)
        return qs[: qs.count() / visible_part]

    def catalog_hidden_part(self, **kwargs):
        visible_part = settings.HIDDEN_PART_IN_CATALOGUE
        qs = self.get_query_set(**kwargs)
        return qs[qs.count()/visible_part:]


class CatalogAbstracModel(models.Model):
    allowed_types = ('music', 'sheetmusic',)

    name = models.CharField(max_length=255, unique=True)

    created_date = models.DateTimeField(auto_now_add=True, auto_now=True,
                                        blank=True, null=True)

    def catalog_uri(self, musictype='music', filter_by='', name=''):
        if musictype not in self.allowed_types:
            raise Http404()
        return reverse('show_catalog', musictype, filter_by, name)

    class Meta:
        abstract = True
        ordering = ('name',)


class Composer(CatalogAbstracModel):
    approved = models.BooleanField(default=False)
    sheet_approved = models.BooleanField(default=False)
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128, null=True, blank=True)
    learn_more = models.URLField(null=True, blank=True)
    comment = HTMLField(null=True, blank=True)
    slug = models.SlugField(unique=True)
    image = ImageWithThumbsField(
        upload_to='images/composers',
        thumbs=(
            ('228_109_cropped', {'size': (228, 109), 'crop': '50% 20%'}),
            ('228_109_cropped@2x', {'size': (456, 218), 'crop': '50% 20%'}),
        ),
        blank=True, null=True,
        storage=PUBLIC_MEDIA_BUCKET,
    )
    featured = models.BooleanField(default=False)
    box_folder = models.ForeignKey(
        'box_client.Folders', related_name='composer', null=True,
    )
    box_folder_sheet = models.ForeignKey(
        'box_client.Folders', related_name='sheet_composer', null=True,
    )

    objects = CatalogManager()

    def __unicode__(self):
        return u'{}'.format(self.name)

    def save(self, *args, **kwargs):

        if self.last_name:
            self.slug = slugify(self.first_name + '-' + self.last_name)
            self.name = self.first_name + ' ' + self.last_name
        else:
            self.slug = slugify(self.first_name)
            self.name = self.name = self.first_name

        super(Composer, self).save(*args, **kwargs)

    def get_filter_url(self, musictype):
        return reverse(musictype, args=('composer', self.slug))

    def get_absolute_url(self):
        return reverse('musmusic_info_page', args=('composer', self.slug))

    def get_folder(self, musictype):
        if musictype == 'music':
            if not self.box_folder:
                folder_id = get_upload_folder(
                    name=self.name, musictype=musictype,
                )
                folder = box.Folders(box_id=folder_id, name=self.name)
                folder.save()
                self.box_folder = folder
                self.save()
            return self.box_folder
        elif musictype == 'sheetmusic':
            if not self.box_folder_sheet:
                folder_id = get_upload_folder(
                    name=self.name, musictype=musictype,
                )
                folder = box.Folders(box_id=folder_id, name=self.name)
                folder.save()
                self.box_folder_sheet = folder
                self.save()
            return self.box_folder_sheet
        else:
            return None

    class Meta:
        ordering = ('name',)


class Form(CatalogAbstracModel):
    slug = models.SlugField(unique=True)
    approved = models.BooleanField(default=False)
    sheet_approved = models.BooleanField(default=False)
    learn_more = models.URLField(null=True, blank=True)
    comment = HTMLField(null=True, blank=True)

    def save(self):
        self.slug = slugify(self.name)
        super(Form, self).save()

    def get_filter_url(self, musictype):
        return reverse(musictype, args=('form', self.slug))

    def get_absolute_url(self):
        return reverse('musmusic_info_page', args=('form', self.slug))

    def __unicode__(self):
        return self.name


class Instrument(CatalogAbstracModel):
    approved = models.BooleanField(default=False)
    sheet_approved = models.BooleanField(default=False)
    learn_more = models.URLField(null=True, blank=True)
    comment = HTMLField(null=True, blank=True)
    slug = models.SlugField(unique=True)

    def save(self):
        self.slug = slugify(self.name)
        super(Instrument, self).save()

    def get_filter_url(self, musictype):
        return reverse(musictype, args=('instrument', self.slug))

    def get_absolute_url(self):
        return reverse('musmusic_info_page', args=('instrument', self.slug))

    def __unicode__(self):
        return self.name


class Performer(CatalogAbstracModel):
    approved = models.BooleanField(default=False)
    slug = models.SlugField(unique=True)
    learn_more = models.URLField(null=True, blank=True)
    comment = HTMLField(null=True, blank=True)

    def save(self):
        self.slug = slugify(self.name)
        super(Performer, self).save()

    def get_filter_url(self, musictype):
        return reverse(musictype, args=('performer', self.slug))

    def get_absolute_url(self):
        return reverse('musmusic_info_page', args=('performer', self.slug))

    def __unicode__(self):
        return self.name


class Period(CatalogAbstracModel):
    approved = models.BooleanField(default=False)
    sheet_approved = models.BooleanField(default=False)
    more = models.URLField(null=True, blank=True)
    comment = HTMLField(null=True, blank=True)
    slug = models.SlugField(unique=True)

    def save(self):
        self.slug = slugify(self.name)
        super(Period, self).save()

    def get_filter_url(self, musictype):
        return reverse(musictype, args=('period', self.slug))

    def get_absolute_url(self):
        return reverse('musmusic_info_page', args=('period', self.slug))

    def __unicode__(self):
        return self.name


class License(CatalogAbstracModel):
    CC_TYPE = (
        ('NO-CC', 'Is not a "Creative Commons" license'),
        ('CC-BY', 'CC-BY'),
        ('CC-BY-SA', 'CC-BY-SA'),
        ('CC-BY-NC', 'CC-BY-NC'),
        ('CC-BY-NC-SA', 'CC-BY-NC-SA'),
        ('CC-BY-ND', 'CC-BY-ND'),
        ('CC-BY-NC-ND', 'CC-BY-NC-ND'),
        ('CC-PD', 'CC-PD'),
    )

    approved = models.BooleanField(default=True)
    cc_license = models.CharField(max_length=32, choices=CC_TYPE)

    def __unicode__(self):
        return self.name

    def cc_image(self):
        if not self.cc_license:
            return None
        if (
            self.cc_license.startswith("CC-")
            and self.cc_license != 'CC-PD'
        ):
            return "https://i.creativecommons.org/l/{}/3.0/88x31.png".format(
                self.cc_license[3:].lower()
            )
        elif self.cc_license == 'CC-PD':
            return "https://i.creativecommons.org/p/mark/1.0/88x31.png"
        else:
            return None

    def cc_link(self):
        if not self.cc_license:
            return None
        if (
            self.cc_license.startswith("CC-")
            and self.cc_license != 'CC-PD'
        ):
            return "https://creativecommons.org/licenses/{}/3.0/".format(
                self.cc_license[3:].lower()
            )
        elif self.cc_license == 'CC-PD':
            return "https://creativecommons.org/publicdomain/mark/1.0/"
        else:
            return None

    def text(self):
        if not self.cc_license or self.cc_license == 'NO-CC':
            return "This work is licensed under a {}.".format(self.name)
        else:
            if self.cc_license == 'CC-PD':
                return """
    <a
        rel="license"
        href="{}"
    >
        <img src="{}" style="border-style: none;" alt="Public Domain Mark" />
    </a>
        This work is free of known copyright restrictions.""".format(
                    self.cc_link(), self.cc_image()
                )
            else:
                return """
    <a
        rel="license"
        href="{}"
    >
        <img src="{}" style="border-style: none;" alt="{}" />
    </a>
        This work is licensed under a
        <a rel="license" href="{}">{} license</a>.""".format(
                    self.cc_link(), self.cc_image(), self,
                    self.cc_link(), self,
                )


class Genre(CatalogAbstracModel):
    approved = models.BooleanField(default=True)

    def __unicode__(self):
        return self.name

########
#       Downloadable content is here
########


class AbstractPiece(models.Model):

    user = models.ForeignKey('mususer.MusUser', blank=True, null=True)
    title = models.CharField(max_length=255)
    composer = models.ForeignKey('Composer', blank=True, null=True)
    form = models.ForeignKey('Form', blank=True, null=True)
    period = models.ForeignKey('Period', blank=True, null=True)
    instrument = models.ForeignKey('Instrument', blank=True, null=True)
    comment = HTMLField(blank=True, null=True)
    learn_more = models.CharField(max_length=240, blank=True, null=True)
    approved = models.BooleanField(default=False)
    upload_date = models.DateField(auto_now_add=True, blank=True, null=True)
    slug = models.SlugField(max_length=255)
    sort_key = models.CharField(max_length=285, blank=True, null=True)
    imported_from = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        abstract = True

    def __unicode__(self):
        if hasattr(self, 'id') and self.id:
            return self.title + ' #' + unicode(self.id)
        else:
            return self.title

    def get_absolute_url(self):
        if self.composer and self.composer.slug:
            composer_slug = slugify(self.composer.slug) + '/'
        else:
            composer_slug = ''
        if self.content_type == 'sheet':
            musictype = 'sheetmusic'
        else:
            musictype = 'music'
        return reverse(
            'music_view',
            kwargs={'musictype': musictype, 'object_id': self.id},
        ) + composer_slug + self.slug + '/'

    def save(self, *args, **kwargs):
        if self.id:
            old_instance = (
                self.__class__._default_manager
                .filter(pk=self.pk).get()
            )
        else:
            old_instance = None
        self.slug = slugify(self.title)
        self.sort_key = re.sub('\d{1,4}', digitsrepl, self.slug)
        comment = u'Changed'
        for field in self._meta.fields:
            changed, old, new = has_changed(self, field.name)
            if changed:
                if field.name in (
                    'title', 'license', 'learn_more', 'slug',
                ):
                    comment += u' {} from {} to {},'.format(
                        field.name, old, new,
                    )
                elif field.name in METADATA[self.content_type]:
                    comment += u' {} from {} to {},'.format(
                        field.name, old, new,
                    )
                    # (un)approve related metadata in case of changed related
                    if self.content_type == 'sheet':
                        attr = 'sheet_approved'
                    else:
                        attr = 'approved'
                    if getattr(self, field.name + '_id') and self.approved:
                        related = getattr(self, field.name)
                        if not getattr(related, attr):
                            setattr(related, attr, True)
                            related.save()
                    if old_instance and getattr(old_instance, field.name + '_id'):
                        related = getattr(old_instance, field.name)
                        if self.content_type == 'music':
                            related_manager = related.piece_set
                        elif self.content_type == 'sheet':
                            related_manager = related.sheetmusic_set

                        if (
                                getattr(related, attr)
                                and not (
                                    related_manager
                                    .exclude(id=self.id)
                                    .filter(approved=True)
                                    .exists()
                                )
                        ):
                            setattr(related, attr, False)
                            related.save()

                elif field.name == 'approved' and self.approved:
                    # approved changed to True
                    comment += u' {},'.format(field.name)
                    if self.content_type == 'sheet':
                        attr = 'sheet_approved'
                    else:
                        attr = 'approved'
                    for prop in METADATA[self.content_type]:
                        # approve related metadata
                        if getattr(self, prop + '_id'):
                            related = getattr(self, prop)
                            if not getattr(related, attr):
                                setattr(related, attr, True)
                                related.save()
                elif field.name == 'approved':
                    # approved changed to False
                    comment += u' {},'.format(field.name)
                    if self.content_type == 'sheet':
                        attr = 'sheet_approved'
                        manager_attr = 'sheetmusic_set'
                    else:
                        attr = 'approved'
                        manager_attr = 'piece_set'
                    for prop in METADATA[self.content_type]:
                        # unapprove related metadata
                        if getattr(self, prop + '_id'):
                            related = getattr(self, prop)
                            rel_manager = getattr(related, manager_attr)
                            if (
                                getattr(related, attr)
                                and not rel_manager.filter(approved=True).exists()
                            ):
                                setattr(related, attr, False)
                                related.save()
                else:
                    comment += u' {},'.format(field.name)
        if not self.id:
            reversion.set_comment('New object created')
            super(AbstractPiece, self).save(*args, **kwargs)
        elif not comment == 'Changed':
            reversion.set_comment(comment.strip(','))
            super(AbstractPiece, self).save(*args, **kwargs)
        else:
            #print 'Not changed'
            pass

    def delete(self, *args, **kwargs):
        reversion.set_comment('Delete')
        if self.content_type == 'sheet':
            attr = 'sheet_approved'
            manager_attr = 'sheetmusic_set'
        else:
            attr = 'approved'
            manager_attr = 'piece_set'
        for prop in METADATA[self.content_type]:
            # unapprove related metadata
            if getattr(self, prop + '_id'):
                related = getattr(self, prop)
                rel_manager = getattr(related, manager_attr)
                if (
                    getattr(related, attr)
                    and not (
                        rel_manager
                        .filter(approved=True)
                        .exclude(id=self.id)
                        .exists()
                    )
                ):
                    setattr(related, attr, False)
                    related.save()
        super(AbstractPiece, self).delete(*args, **kwargs)


class DownloadableContent(models.Model):
    number = models.IntegerField(default=0)
    title = models.CharField(max_length=255, blank=True, null=True)
    filename = models.CharField(max_length=255, blank=True, null=True)

    created_by = models.ForeignKey(
        'mususer.MusUser', null=True,
        db_column='created_by',
    )
    created_date = models.DateTimeField(
        auto_now_add=True, blank=True, null=True,
    )

    approved = models.BooleanField(default=False)

    class Meta:
        abstract = True

    # I'm not sure why it's here at all
    def get_download_url(self):
        '''This function returns direct url to the file on the server.
        '''
        return "{server}/download/{type}/id/{id}".format(
            server=DOWNLOAD_SERVER_URL, id=self.id, type=self.content_type)


class SheetMusic(AbstractPiece):
    content_type = 'sheet'

    class Meta:
        db_table = 'its_sheets'

    def delete(self, *args, **kwargs):
        for part in self.sheetpart_set.all():
            part.delete()
        super(SheetMusic, self).delete(*args, **kwargs)


class Piece(AbstractPiece):
    content_type = 'music'

    performer = models.ForeignKey('Performer', blank=True, null=True)
    license = models.ForeignKey('License', default=9,  # hardcode PD as default
                                blank=True, null=True)

    class Meta:
        db_table = 'its_pieces'

    def get_avg_rating(self):
        if not hasattr(self, 'rating_obj'):
            if self.rating.exists():
                self.rating_obj = self.rating.get()
            else:
                self.rating_obj = self.rating.create()
        if self.rating_obj.rating_count != 0:
            return float(self.rating_obj.rating_sum) / float(self.rating_obj.rating_count)
        else:
            return 0

    def get_rec_rating(self):
        if not hasattr(self, 'rating_obj'):
            if self.rating.exists():
                self.rating_obj = self.rating.get()
            else:
                self.rating_obj = self.rating.create()
        if self.rating_obj.rating_count != 0:
            return float(self.rating_obj.rating_recording) / float(self.rating_obj.rating_count)
        else:
            return 0

    def get_play_rating(self):
        if not hasattr(self, 'rating_obj'):
            if self.rating.exists():
                self.rating_obj = self.rating.get()
            else:
                self.rating_obj = self.rating.create()
        if self.rating_obj.rating_count != 0:
            return float(self.rating_obj.rating_playing) / float(self.rating_obj.rating_count)
        else:
            return 0

    def get_rating_count(self):
        if not hasattr(self, 'rating_obj'):
            if self.rating.exists():
                self.rating_obj = self.rating.get()
            else:
                self.rating_obj = self.rating.create()
        return self.rating_obj.rating_count

    def delete(self, *args, **kwargs):
        for part in self.part_set.all():
            part.delete()
        super(Piece, self).delete(*args, **kwargs)


class Part(DownloadableContent):
    content_type = 'music'

    piece = models.ForeignKey('Piece')
    hq_filename = models.CharField(max_length=255, null=True)
    box_file = models.ForeignKey(
        'box_client.UploadedFile',
        related_name='lossypart',
        null=True, blank=True,
        on_delete=models.SET_NULL,
    )
    hq_box_file = models.ForeignKey(
        'box_client.UploadedFile',
        related_name='losslesspart',
        null=True, blank=True,
        on_delete=models.SET_NULL,
    )

    class Meta:
        db_table = 'its_parts'

    def __unicode__(self):
        return u'Piece {piece.id} {piece.title} part #{self.number} {self.title}'.format(
            piece=self.piece,
            self=self,
        )

    def get_shared_url(self):
        if self.box_file and not self.box_file.broken:
            return self.box_file.shared_url
        else:
            return ''

    def get_hq_shared_url(self):
        if self.hq_box_file and not self.hq_box_file.broken:
            return self.hq_box_file.shared_url
        else:
            return ''

    def save(self, *args, **kwargs):
        if self.box_file or self.hq_box_file:
            self.approved = True
            if self.piece_id and not self.piece.approved:
                self.piece.approved = True
                with reversion.create_revision():
                    reversion.set_comment("Approve")
                    self.piece.save()
        else:
            self.approved = False
            if self.piece_id and self.piece.approved:
                if not self.piece.part_set.filter(approved=True).exists():
                    self.piece.approved = False
                    with reversion.create_revision():
                        reversion.set_comment("Unapprove")
                        self.piece.save()
        super(Part, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if (
            self.hq_box_file_id
            and not self.hq_box_file.lossypart.exclude(id=self.id)
            and not self.hq_box_file.losslesspart.exclude(id=self.id)
        ):
            self.hq_box_file.delete()
        if (
            self.box_file_id
            and not self.box_file.lossypart.exclude(id=self.id)
            and not self.box_file.losslesspart.exclude(id=self.id)
        ):
            self.box_file.delete()
        super(Part, self).delete(*args, **kwargs)


class SheetPart(DownloadableContent):
    content_type = 'sheet'

    sheet = models.ForeignKey('SheetMusic')
    box_file = models.ForeignKey(
        'box_client.UploadedFile',
        null=True, blank=True,
        on_delete=models.SET_NULL,
    )

    class Meta:
        ordering = ('sheet',)
        db_table = 'its_sparts'

    def __unicode__(self):
        return self.sheet.title

    def get_shared_url(self):
        if self.box_file:
            return self.box_file.shared_url
        else:
            return ''

    def save(self, *args, **kwargs):
        if self.box_file:
            self.approved = True
            if self.sheet_id and not self.sheet.approved:
                self.sheet.approved = True
                with reversion.create_revision():
                    reversion.set_comment("Approve")
                    self.sheet.save()
        else:
            self.approved = False
            if self.sheet_id and self.sheet.approved:
                if not self.sheet.sheetpart_set.filter(approved=True).exists():
                    self.sheet.approved = False
                    with reversion.create_revision():
                        reversion.set_comment("Unapprove")
                        self.sheet.save()
        super(SheetPart, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if (
            self.box_file_id
            and not self.box_file.sheetpart_set.exclude(id=self.id)
        ):
            self.box_file.delete()
        super(SheetPart, self).delete(*args, **kwargs)


class PiecePartBookmark(models.Model):
    part = models.ForeignKey('Part')
    user = models.ForeignKey('mususer.MusUser')


class SheetPartBookmark(models.Model):
    part = models.ForeignKey('SheetPart')
    user = models.ForeignKey('mususer.MusUser')


class PieceRating(models.Model):
    piece = models.ForeignKey('Piece', unique=True, related_name="rating")
    rating_sum = models.IntegerField(default=0)
    rating_recording = models.IntegerField(default=0)
    rating_playing = models.IntegerField(default=0)
    rating_count = models.IntegerField(default=0)
    hits = models.IntegerField(default=0)


class PieceRate(models.Model):

    user = models.ForeignKey('mususer.MusUser')
    piece = models.ForeignKey('Piece', related_name="rates")
    rate = models.IntegerField(default=0)
    rate_recording = models.IntegerField(default=0)
    rate_playing = models.IntegerField(default=0)

    def save(self, *args, **kwargs):
        changed_rec, old_rec, new_rec = has_changed(self, 'rate_recording')
        changed_pl, old_pl, new_pl = has_changed(self, 'rate_playing')
        if changed_rec or old_rec is None or changed_pl or old_pl is None:
            if not self.piece.rating.exists():
                self.piece.rating.create()
            r = self.piece.rating.get()
            if old_rec is None:
                r.rating_count += 1
                old_rec = 0
                new_rec = self.rate_recording
            if old_pl is None:
                old_pl = 0
                new_pl = self.rate_playing
            old = (old_rec + old_pl) / 2
            new = (new_rec + new_pl) / 2
            r.rating_recording += new_rec - old_rec
            r.rating_playing += new_pl - old_pl
            r.rating_sum += new - old
            r.save()
        super(PieceRate, self).save(*args, **kwargs)


class SheetRating(models.Model):
    piece = models.ForeignKey('SheetMusic', unique=True, related_name="rating")
    hits = models.IntegerField(default=0)


class RadioItem(models.Model):
    """
    Model for special benefactor radio
    """
    title = models.CharField(max_length=255)
    composer = models.ForeignKey('Composer', blank=True, null=True)
    performer = models.ForeignKey('Performer', blank=True, null=True)
    box_file = models.ForeignKey('box_client.UploadedFile')

    @property
    def piece(self):
        return self

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return ''
