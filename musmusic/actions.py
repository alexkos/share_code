from django.contrib import admin
from django.shortcuts import render
from django.http import HttpResponseRedirect
import reversion
from musmusic.forms import MergeForm, PieceMassForm


def delete_and_ban(modeladmin, request, queryset):
    for piece in queryset:
        user = piece.user
        user.is_active = False
        user.is_blocked = True
        user.save()
        piece.delete()
delete_and_ban.short_description = (
    "Delete selected pieces and block users authored them"
)


def unapprove(modeladmin, request, queryset):
    for piece in queryset:
        piece.approved = False
        piece.save()
unapprove.short_description = "Un-approve selected pieces"


def merge_metadata(modeladmin, request, queryset):
    form = None
    if 'apply' in request.POST:
        form = MergeForm(request.POST, queryset=queryset)
        if form.is_valid():
            target = form.cleaned_data['target']
            count_pieces = 0
            count_sheets = 0
            model_name = queryset.model._meta.module_name
            for item in (
                queryset
                .exclude(id=target.id)
                .select_related('piece', 'sheetmusic')
            ):
                for piece in item.piece_set.all():
                    setattr(piece, model_name, target)
                    with reversion.create_revision():
                        piece.save()
                        reversion.set_user(request.user)
                        reversion.set_comment("Merge")
                    count_pieces += 1
                if model_name not in ('performer', 'license'):
                    for sheet in item.sheetmusic_set.all():
                        setattr(sheet, model_name, target)
                        with reversion.create_revision():
                            sheet.save()
                            reversion.set_user(request.user)
                            reversion.set_comment("Merge")
                        count_sheets += 1
                with reversion.create_revision():
                    item.delete()
                    reversion.set_user(request.user)
                    reversion.set_comment("Deleted by merge")
            modeladmin.message_user(
                request,
                "{} pieces and {} sheets moved to {}.".format(
                    count_pieces, count_sheets, target,
                ),
            )
            return HttpResponseRedirect(request.get_full_path())
    if not form:
        form = MergeForm(
            initial={'_selected_action': request.POST.getlist(
                admin.ACTION_CHECKBOX_NAME
            )},
            queryset=queryset,
        )
    return render(
        request,
        'musmusic/merge_metadata.html',
        {
            'items': queryset,
            'form': form,
            'title': u'Merge metadata',
            'model_name': queryset.model._meta.module_name,
        },
    )


def mass_edit(modeladmin, request, queryset):
    form = None
    if 'apply' in request.POST:
        form = PieceMassForm(request.POST, queryset=queryset)
        if form.is_valid():
            count = 0
            model_name = queryset.model._meta.module_name
            for item in queryset:
                for field in (
                    'composer', 'performer', 'instrument', 'period', 'form'
                ):
                    if form.cleaned_data.get(field):
                        setattr(item, field, form.cleaned_data.get(field))
                with reversion.create_revision():
                    item.save()
                    reversion.set_user(request.user)
                    reversion.set_comment("Edited with mass edit tool.")
                count += 1
            modeladmin.message_user(
                request,
                "Metadata changed for {} pieces.".format(count),
            )
            return HttpResponseRedirect(request.get_full_path())
    if not form:
        form = PieceMassForm(
            initial={'_selected_action': request.POST.getlist(
                admin.ACTION_CHECKBOX_NAME
            )},
            queryset=queryset,
        )
    return render(
        request,
        'musmusic/mass_edit.html',
        {
            'items': queryset,
            'form': form,
            'title': u'Mass edit metadata',
            'model_name': queryset.model._meta.module_name,
        },
    )
