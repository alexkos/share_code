from datetime import date
from musmusic.models import SheetMusic, Piece


def most_popular(request):
    return {
        'most_popular': {
            'sheetmusic': (
                SheetMusic.objects
                .order_by('-rating__hits', 'id')
            )[:6],
            'music': Piece.objects.order_by('-rating__hits', 'id')[:6],
        }
    }


def today(request):
    return {
        'today': date.today()
    }
