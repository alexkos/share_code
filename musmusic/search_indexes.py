from django.db.models import Count
from haystack import indexes
from musmusic.models import Piece, SheetMusic
#from musmusic.models import Composer, Performer, Form, Instrument, Period


class PieceIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    user = indexes.CharField(model_attr='user', default='')
    title = indexes.CharField(model_attr='title', default='')
    upload_date = indexes.DateTimeField(model_attr='upload_date')
    comment = indexes.CharField(model_attr='comment', default='')
    date = indexes.DateTimeField(model_attr='upload_date')
    musictype = indexes.CharField(default='music')

    def get_model(self):
        return Piece

    def index_queryset(self, using=None):
        return (
            self.get_model().objects
            .filter(approved=True)
        )


class SheetIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    user = indexes.CharField(model_attr='user', default='')
    title = indexes.CharField(model_attr='title', default='')
    upload_date = indexes.DateTimeField(model_attr='upload_date')
    comment = indexes.CharField(model_attr='comment', default='')
    date = indexes.DateTimeField(model_attr='upload_date')
    musictype = indexes.CharField(default='sheetmusic')

    def get_model(self):
        return SheetMusic

    def index_queryset(self, using=None):
        return (
            self.get_model().objects
            .filter(approved=True)
        )
